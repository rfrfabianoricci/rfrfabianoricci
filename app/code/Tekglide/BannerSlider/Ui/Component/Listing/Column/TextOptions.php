<?php
namespace Tekglide\BannerSlider\Ui\Component\Listing\Column;

class TextOptions implements \Magento\Framework\Option\ArrayInterface
{
    //Here you can __construct Model

    public function toOptionArray()
    {
        return [
            ['value' => 'none', 'label' => __('None')],
            ['value' => 'left', 'label' => __('Left')],
            ['value' => 'right', 'label' => __('Right')],
            ['value' => 'center', 'label' => __('Center')],
            ['value' => 'top-right', 'label' => __('Top-Right')],
            ['value' => 'bottom-right', 'label' => __('Bottom-Right')],
            ['value' => 'top-left', 'label' => __('Top-Left')],
            ['value' => 'bottom-left', 'label' => __('Bottom-Left')],
            ['value' => 'top-center', 'label' => __('Top-Center')],
            ['value' => 'bottom-center', 'label' => __('Bottom-Center')]
        ];
    }
}