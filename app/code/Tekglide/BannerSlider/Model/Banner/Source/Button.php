<?php
namespace Tekglide\BannerSlider\Model\Banner\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class Status
 */
class Button implements OptionSourceInterface
{
    /**
     * @var \Tekglide\BannerSlider\Model\Banner
     */
    protected $banner;

    /**
     * Constructor
     *
     * @param \Tekglide\BannerSlider\Model\Banner $banner
     */
    public function __construct(\Tekglide\BannerSlider\Model\Banner $banner)
    {
        $this->banner = $banner;
    }

    /**
     * Get brand title
     *
     * @return array
     */
    public function toOptionArray()
    {
        $brandtitle = $this->banner->getButton();
        $options = [];
        foreach ($brandtitle as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }
        return $options;
    }
}
