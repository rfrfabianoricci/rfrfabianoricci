<?php
namespace Tekglide\BannerSlider\Model;

use Tekglide\BannerSlider\Model\Banner\FileInfo;
use Magento\Framework\App\ObjectManager;
use Magento\Store\Model\StoreManagerInterface;

class Banner extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Banner cache tag
     */
    const CACHE_TAG = 'tekglide_banners_slider';

    /**#@+
     * Banner's statuses
     */
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 2;
    const TEXT = 'text';
    const LEFT = 'left';
    const RIGHT = 'right';
    const CENTER = 'center';
    const TOPCENTER = 'top-center';
    const BOTTOMCENTER = 'bottom-center';
    const TOPLEFT = 'top-left';
    const BOTTOMLEFT = 'bottom-left';
    const TOPRIGHT = 'top-right';
    const BOTTOMRIGHT = 'bottom-right';
    const BRANDTITLE = 'brandtitle';
    const BUTTON_ENABLED = 1;
    const BUTTON_DISABLED = 2;
    const BUTTON_URL = 'button_url';
    const BUTTON_NAME = "button_name";
    const COLOR_PICKER = "text_color_picker";
    const BRAND_COLOR_PICKER = "brand_color_picker";
    const BUTTONCOLOUR = 'button_color_picker';
    const NONE = 'none';
    /**
     * Store manager
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Tekglide\BannerSlider\Model\ResourceModel\Banner::class);
    }

    /**
     * Prepare banner's statuses.
     *
     * @return array
     */
    public function getAvailableStatuses()
    {
        return [self::STATUS_ENABLED => __('Enabled'), self::STATUS_DISABLED => __('Disabled')];
    }

    /**
     * Retrieve the Image URL
     *
     * @param string $imageName
     * @return bool|string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getImageUrl($imageName = null)
    {
        $url = '';
        $image = $imageName;
        if (!$image) {
            $image = $this->getData('image');
        }
        if ($image) {
            if (is_string($image)) {
                $url = $this->_getStoreManager()->getStore()->getBaseUrl(
                    \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
                ).FileInfo::ENTITY_MEDIA_PATH .'/'. $image;
            } else {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __('Something went wrong while getting the image url.')
                );
            }
        }
        return $url;
    }

    // public function setText($text)
    //     {
    //         return $this->setData(self::TITLE, $text);
    //     }

    public function getText()
        {
            return $this->getData(self::TEXT);
        }
    
    public function getTextColour()
        {
            return $this->getData(self::COLOR_PICKER);
        }

    public function getBrandTextColour()
        {
            return $this->getData(self::BRAND_COLOR_PICKER);
        }

    public function getBrandtitle()
        {
            return $this->getData(self::BRANDTITLE);
        }

    public function getButtonColour()
        {
            return $this->getData(self::BUTTONCOLOUR);
        }

    
    
    public function getTextPosition()
    {
        return [self::NONE => __('none'), self::LEFT => __('left'), self::RIGHT => __('right'), 
        self::CENTER => __('center'), self::TOPLEFT => __('top-left'),
        self::BOTTOMLEFT => __('bottom-left'),
        self::TOPRIGHT => __('top-right'), self::BOTTOMRIGHT => __('bottom-right'),
        self::TOPCENTER => __('top-center'),
        self::BOTTOMCENTER => __('bottom-center')];
    }

    public function getButton()
        {
        return [self::BUTTON_ENABLED => __('Enabled'), self::BUTTON_DISABLED => __('Disabled')];

        }
    public function getButtonName()
        {
        return $this->getData(self::BUTTON_NAME);
        }

    public function getButtonUrl()
        {
        return $this->getData(self::BUTTON_URL);
        }

    /**
     * Get StoreManagerInterface instance
     *
     * @return StoreManagerInterface
     */
    private function _getStoreManager()
    {
        if ($this->_storeManager === null) {
            $this->_storeManager = ObjectManager::getInstance()->get(StoreManagerInterface::class);
        }
        return $this->_storeManager;
    }
}
