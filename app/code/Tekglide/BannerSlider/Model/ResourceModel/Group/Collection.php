<?php
namespace Tekglide\BannerSlider\Model\ResourceModel\Group;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Tekglide\BannerSlider\Model\Group::class, \Tekglide\BannerSlider\Model
        \ResourceModel\Group::class);
    }
}
