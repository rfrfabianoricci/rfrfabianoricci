<?php

namespace Knowband\Supercheckout\Controller\Address;

use Magento\Framework\App\Action\Context;
use Magento\Customer\Api\AddressRepositoryInterface;

class ShippingRegions extends \Magento\Framework\App\Action\Action
{

    public $resultJsonFactory;
    public $sc_urlInterface;
    public $sc_countryFactory;
    public $sc_scopeConfig;

    public function __construct(Context $context, \Magento\Checkout\Model\Session $checkoutSession, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory, \Magento\Directory\Model\CountryFactory $countryFactory)
    {
        $this->sc_scopeConfig = $scopeConfig;
        $this->quote = $checkoutSession;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->sc_countryFactory = $countryFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultPage = $this->resultJsonFactory->create();
        $regions = [];
        if ($this->getRequest()->isPost()) {
            $country_id = $this->getRequest()->getPost("country_id");
            $shipping_address = $this->quote->getQuote()->getShippingAddress();
            $default_country = $this->sc_scopeConfig->getValue('general/country/default', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, null);
            $country = $country_id ? $country_id : $default_country;
            $shipping_address->setCountryId($country)->setCollectShippingRates(true)->collectShippingRates();
            $this->quote->getQuote()->save();

            $stateArray = $this->sc_countryFactory->create()->setId($country_id)->getLoadedRegionCollection()->toOptionArray();
            if (count($stateArray) > 0) {
                foreach ($stateArray as $state) {
                    if ($state["title"]) {
                        $regions[] = ["id" => $state["value"], "label" => $state["title"]];
                    }
                }
            }
        }
        $resultPage->setData(['regions' => $regions]);
        return $resultPage;
    }
}
