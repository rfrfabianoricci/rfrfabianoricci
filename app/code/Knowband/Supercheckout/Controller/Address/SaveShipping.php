<?php

namespace Knowband\Supercheckout\Controller\Address;

use Magento\Framework\App\Action\Context;
use Magento\Customer\Api\AddressRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\InputException;

class SaveShipping extends \Magento\Framework\App\Action\Action
{

    public $sc_quotemanagement;
    public $quote;
    public $resultJsonFactory;
    public $sc_urlInterface;
    public $addressRepository;
    public $sc_customerSession;
    public $totalsCollector;

    public function __construct(Context $context, \Magento\Checkout\Model\Session $checkoutSession, \Magento\Quote\Model\QuoteManagement $quoteManagement, \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory, AddressRepositoryInterface $addressRepository, \Magento\Customer\Model\Session $customerSession, \Magento\Quote\Model\Quote\TotalsCollector $totalsCollector)
    {
        parent::__construct($context);
        $this->sc_quotemanagement = $quoteManagement;
        $this->quote = $checkoutSession;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->sc_urlInterface = $context->getUrl();
        $this->addressRepository = $addressRepository;
        $this->sc_customerSession = $customerSession;
        $this->totalsCollector = $totalsCollector;
    }

    public function execute()
    {
        $resultPage = $this->resultJsonFactory->create();
        if ($this->getRequest()->isPost()) {
            try {
                $shipping_data = $this->getRequest()->getPost("shipping");
                $address_id = isset($shipping_data["address_id"]) ? $shipping_data["address_id"] : 0;
                if ($address_id == "new") {
                    unset($shipping_data["address_id"]);
                    $street = [];
                    $street[] = $shipping_data["street1"];
                    if (isset($shipping_data["street2"])) {
                        $street[] = $shipping_data["street2"];
                    }
                    $shipping_data["street"] = $street;
                    $this->quote->setData("shipping_address_id", $address_id);
                    $shipping_address = $this->quote->getQuote()->getShippingAddress();
                    $shipping_address->addData($shipping_data)->setCollectShippingRates(true)->collectShippingRates();
                    $this->quote->getQuote()->save();
                } else {
                    $shipping_address = $this->quote->getQuote()->getShippingAddress();
                    $shipping_address_data = null;
                    try {
                        $shipping_address_data = $this->addressRepository->getById($address_id);
                    } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
                        // do nothing if customer is not found by id
                    }

                    if ($shipping_address_data->getCustomerId() != $this->quote->getQuote()->getCustomerId()) {
                        return ['error' => 1, 'message' => __('The customer address is not valid.')];
                    }
                    $this->quote->setData("shipping_address_id", $address_id);
                    $shipping_address->importCustomerAddressData($shipping_address_data)->setSaveInAddressBook(0);
                    $shipping_address->setCollectShippingRates(true)->collectShippingRates();
                    $this->totalsCollector->collectAddressTotals($this->quote->getQuote(), $shipping_address);
                    $shipping_address->save();
                    $this->quote->getQuote()->save();
                }
                $resultPage->setData(['success' => __("Address saved successfully.")]);
            } catch (\Magento\Framework\Exception\InputException $e) {
                $resultPage->setData(['error' => $e->getMessage()]);
            }
        }
        return $resultPage;
    }
}
