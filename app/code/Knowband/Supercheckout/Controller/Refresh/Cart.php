<?php

namespace Knowband\Supercheckout\Controller\Refresh;

class Cart extends \Magento\Framework\App\Action\Action
{

    public $sc_resultRawFactory;
    public $sc_request;
    public $sc_helper;
    public $sc_scopeConfig;
    public $inlineTranslation;
    public $sc_transportBuilder;
    public $sc_customerSession;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Framework\App\Request\Http $request, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Knowband\Supercheckout\Helper\Data $helper, \Magento\Customer\Model\Session $customerSession, \Magento\Framework\View\Result\PageFactory $resultRawFactory, \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation, \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder)
    {
        parent::__construct($context);
        $this->sc_request = $request;
        $this->sc_scopeConfig = $scopeConfig;
        $this->sc_helper = $helper;
        $this->sc_customerSession = $customerSession;
        $this->sc_resultRawFactory = $resultRawFactory;
        $this->inlineTranslation = $inlineTranslation;
        $this->sc_transportBuilder = $transportBuilder;
    }

    public function execute()
    {
        $resultPage = $this->sc_resultRawFactory->create();
        $resultPage->getLayout()->getBlock('supercheckout_refresh_cart');
        $block_cart = $resultPage->getLayout()
                ->createBlock('Knowband\Supercheckout\Block\Cart')
                ->setTemplate('Knowband_Supercheckout::cart.phtml');
        $block_total = $resultPage->getLayout()
                ->createBlock('Knowband\Supercheckout\Block\Totals')
                ->setTemplate('Knowband_Supercheckout::totals.phtml');
        if ($this->isTotalsEnable()) {
            $block_cart->setChild('cart_block', $block_total);
        }
        $block = $block_cart->toHtml();
        $this->getResponse()->setBody($block);
    }

    public function isTotalsEnable()
    {
        $this->_settings = $this->sc_helper->getSettings();
        if (isset($this->_settings["supercheckout"]["cart"][$this->getUserType()]["totals"]["display"]) && $this->_settings["supercheckout"]["cart"][$this->getUserType()]["totals"]["display"]) {
            return true;
        }
        return false;
    }

    public function getUserType()
    {
        if ($this->sc_customerSession->isLoggedIn()) {
            return "login";
        }
        return "guest";
    }
}
