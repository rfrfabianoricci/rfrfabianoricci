<?php

namespace Knowband\Supercheckout\Controller\Refresh;

class PaymentAddress extends \Magento\Framework\App\Action\Action
{

    public $sc_resultRawFactory;
    public $sc_request;
    public $sc_helper;
    public $sc_scopeConfig;
    public $inlineTranslation;
    public $sc_transportBuilder;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Framework\App\Request\Http $request, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Knowband\Supercheckout\Helper\Data $helper, \Magento\Framework\View\Result\PageFactory $resultRawFactory, \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation, \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder)
    {
        parent::__construct($context);
        $this->sc_request = $request;
        $this->sc_scopeConfig = $scopeConfig;
        $this->sc_helper = $helper;
        $this->sc_resultRawFactory = $resultRawFactory;
        $this->inlineTranslation = $inlineTranslation;
        $this->sc_transportBuilder = $transportBuilder;
    }

    public function execute()
    {
        $resultPage = $this->sc_resultRawFactory->create();
        $resultPage->getLayout()->getBlock('supercheckout_refresh_payment_address');
        $block_payment_address = $resultPage->getLayout()
                ->createBlock('Knowband\Supercheckout\Block\PaymentAddress')
                ->setTemplate('Knowband_Supercheckout::payment_address.phtml')
                ->toHtml();
        $this->getResponse()->setBody($block_payment_address);
    }
}
