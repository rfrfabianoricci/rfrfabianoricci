<?php

namespace Knowband\Supercheckout\Controller\Index;

class Redirect extends \Magento\Checkout\Controller\Index\Index
{

    public $sc_helper;
    public $sc_settings;

    public function execute()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->sc_helper = $objectManager->create('Knowband\Supercheckout\Helper\Data');
        $this->sc_settings = $this->sc_helper->getSettings();
        if (isset($this->sc_settings["supercheckout"]["enable"]) && $this->sc_settings["supercheckout"]["enable"]) {
            if (isset($this->sc_settings["supercheckout"]["test_mode"]) && $this->sc_settings["supercheckout"]["test_mode"]) {
                return parent::execute();
            } else {
                return $this->resultRedirectFactory->create()->setPath('supercheckout');
            }
        } else {
            return parent::execute();
        }
    }
}
