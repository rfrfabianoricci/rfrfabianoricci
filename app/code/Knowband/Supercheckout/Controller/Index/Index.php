<?php

namespace Knowband\Supercheckout\Controller\Index;

class Index extends \Magento\Framework\App\Action\Action
{

    public $sc_resultRawFactory;
    public $sc_request;
    public $sc_helper;
    public $sc_scopeConfig;
    public $inlineTranslation;
    public $sc_transportBuilder;
    public $quote;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Checkout\Model\Session $checkoutSession, \Magento\Framework\App\Request\Http $request, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Knowband\Supercheckout\Helper\Data $helper, \Magento\Framework\View\Result\PageFactory $resultRawFactory, \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation, \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder)
    {
        parent::__construct($context);
        $this->quote = $checkoutSession;
        $this->sc_request = $request;
        $this->sc_scopeConfig = $scopeConfig;
        $this->sc_helper = $helper;
        $this->sc_resultRawFactory = $resultRawFactory;
        $this->inlineTranslation = $inlineTranslation;
        $this->sc_transportBuilder = $transportBuilder;
    }

    public function execute()
    {
        $resultPage = $this->sc_resultRawFactory->create();
        $temp = 0;
        $settings = $this->sc_helper->getSettings();
        if (isset($settings["supercheckout"]["test_mode"]) && $settings["supercheckout"]["test_mode"]) {
            $temp = 1;
        } else {
            if (!isset($settings["supercheckout"]["enable"]) || !$settings["supercheckout"]["enable"]) {
                return $this->resultRedirectFactory->create()->setPath('checkout');
            }
        }

        $quote = $this->quote->getQuote();
        if (!$quote->hasItems() || $quote->getHasError() || !$quote->validateMinimumAmount()) {
            return $this->resultRedirectFactory->create()->setPath('checkout/cart');
        }
        return $resultPage;
    }
}
