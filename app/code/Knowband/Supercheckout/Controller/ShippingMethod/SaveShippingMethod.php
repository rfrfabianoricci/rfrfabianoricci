<?php

namespace Knowband\Supercheckout\Controller\ShippingMethod;

class SaveShippingMethod extends \Magento\Framework\App\Action\Action
{

    public $sc_scopeConfig;
    public $sc_storeManager;
    public $cart;
    public $resultJsonFactory;
    public $quote;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Checkout\Model\Cart $cart, \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory, \Magento\Checkout\Model\Session $checkoutSession)
    {
        $this->sc_scopeConfig = $scopeConfig;
        $this->sc_storeManager = $storeManager;
        $this->cart = $cart;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->quote = $checkoutSession;
        parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        if ($this->getRequest()->isPost()) {
            try {
                $shippingMethod = $this->getRequest()->getPost('shipping_method');
                if (empty($shippingMethod)) {
                    $result->setData(['error' => __("Invalid shipping method.")]);
                    return $result;
                }
                $shippingAddress = $this->quote->getQuote()->getShippingAddress();
                $this->quote->setData("shipping_method_set", $shippingMethod);
                $rate = $shippingAddress->getShippingRateByCode($shippingMethod);
                if (!$rate) {
                    $result->setData(['error' => __("Invalid shipping method.")]);
                    return $result;
                } else {
                    $shippingDescription = $rate->getCarrierTitle() . ' - ' . $rate->getMethodTitle();
                    $shippingAddress->setShippingDescription(trim($shippingDescription, ' -'));
                }
                $shippingAddress->setShippingMethod($shippingMethod)->save();
                $this->quote->getQuote()->collectTotals()->save();
                $result->setData(['success' => __("Shipping method saved successfully.")]);
                return $result;
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $result->setData(['error' => $this->_objectManager->get('Magento\Framework\Escaper')->escapeHtml($e->getMessage())]);
            } catch (\Exception $e) {
                $result->setData(['error' => __('Shipping Method cannot be saved.')]);
            }
        } else {
            $result->setData(['error' => __("Something went wrong. Shipping Method cannot be saved.")]);
        }
        return $result;
    }
}
