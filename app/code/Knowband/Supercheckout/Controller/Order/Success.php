<?php

namespace Knowband\Supercheckout\Controller\Order;

class Success extends \Magento\Framework\App\Action\Action
{

    public $resultPageFactory;
    public $quote;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Checkout\Model\Session $checkoutSession, \Magento\Framework\View\Result\PageFactory $resultPageFactory)
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->quote = $checkoutSession;
        parent::__construct($context);
    }

    public function execute()
    {
        if (!$this->_objectManager->get('Magento\Checkout\Model\Session\SuccessValidator')->isValid()) {
            return $this->resultRedirectFactory->create()->setPath('checkout/cart');
        }
        $this->quote->clearQuote();

        $resultPage = $this->resultPageFactory->create();
        return $resultPage;
    }

}
