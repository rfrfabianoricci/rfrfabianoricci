<?php

namespace Knowband\Supercheckout\Controller\Order;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Customer\Api\AddressRepositoryInterface;
use Magento\Framework\Exception\PaymentException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\StateException;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\NoSuchEntityException;

class Save extends \Magento\Framework\App\Action\Action
{

    const CUSTOMER = 'customer';
    const GUEST = 'guest';
    const REGISTER = 'register';

    public $sc_quotemanagement;
    public $quote;
    public $sc_resultRawFactory;
    public $sc_urlInterface;
    public $sc_quoteFactory;
    public $sc_storeManager;
    public $customerFactory;
    public $sc_customerSession;
    public $sc_scopeConfig;
    public $sc_helper;
    public $addressRepository;
    public $totalsCollector;
    public $sc_objectCopyService;
    public $dataObjectHelper;
    public $sc_customerRepositoryInterface;

    public function __construct(Context $context, \Magento\Checkout\Model\Session $checkoutSession, \Magento\Quote\Model\QuoteManagement $quoteManagement, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Quote\Model\QuoteFactory $quoteFactory, \Magento\Framework\View\Result\PageFactory $resultRawFactory, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Customer\Model\CustomerFactory $customerFactory, \Magento\Customer\Model\Session $customerSession, \Knowband\Supercheckout\Helper\Data $helper, AddressRepositoryInterface $addressRepository, \Magento\Quote\Model\Quote\TotalsCollector $totalsCollector, \Magento\Framework\DataObject\Copy $objectCopyService, \Magento\Framework\Api\DataObjectHelper $dataObjectHelper, \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface, \Magento\Framework\Escaper $escaper)
    {
        parent::__construct($context);
        $this->sc_quotemanagement = $quoteManagement;
        $this->sc_storeManager = $storeManager;
        $this->quote = $checkoutSession;
        $this->sc_resultRawFactory = $resultRawFactory;
        $this->sc_urlInterface = $context->getUrl();
        $this->sc_quoteFactory = $quoteFactory;
        $this->customerFactory = $customerFactory;
        $this->sc_scopeConfig = $scopeConfig;
        $this->sc_helper = $helper;
        $this->sc_customerSession = $customerSession;
        $this->addressRepository = $addressRepository;
        $this->totalsCollector = $totalsCollector;
        $this->sc_objectCopyService = $objectCopyService;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->sc_customerRepositoryInterface = $customerRepositoryInterface;
        $this->escaper = $escaper;
    }

    public function execute()
    {
        $result = $this->sc_resultRawFactory->create();
        if ($this->getRequest()->isPost()) {
            $post_data = $this->getRequest()->getPost();
            $settings = $this->sc_helper->getSettings();
            try {
                if ($errors = $this->validateLogin($post_data)) {
                    $this->messageManager->addError(__(implode(' ', $errors)));
                    $this->_redirect('supercheckout');
                    return $result;
                }
                if (!$this->sc_customerSession->isLoggedIn()) {
                    if ($post_data["login"]["process"] == "register" && isset($settings["supercheckout"]["guest_registration"]) && $settings["supercheckout"]["guest_registration"]) {
//                    if ($post_data["login"]["process"] == "register" || $post_data["login"]["process"] == "guest" && isset($settings["supercheckout"]["guest_registration"]) && $settings["supercheckout"]["guest_registration"]) {
                        $this->saveCustomer($post_data);
                        $post_data->billing["address_id"] = "new";
                        $post_data->shipping["address_id"] = "new";
                    }
                } else {
                    $this->saveLoggedInCustomer($post_data);
                }
                if ($errors = $this->validateAddress()) {
                    $this->messageManager->addError(__(implode(' ', $errors)));
                    $this->_redirect('supercheckout');
                    return $result;
                }
                if ($errors = $this->validateShippingMethods($post_data)) {
                    $this->messageManager->addError(__(implode(' ', $errors)));
                    $this->_redirect('supercheckout');
                    return $result;
                }

                if ($errors = $this->validatePaymentMethods($post_data)) {
                    $this->messageManager->addError(__(implode(' ', $errors)));
                    $this->_redirect('supercheckout');
                    return $result;
                }
                $checkout_method = $this->getCheckoutMethod($post_data["login"]["process"], $post_data["login"]["username"]);
                $this->quote->getQuote()->setCheckoutMethod($checkout_method);
                $this->saveBillingAddress($post_data["billing"], $post_data);
                if ($post_data["same_shipping"]) {
                    $this->saveShippingAddress($post_data["shipping"], $post_data["billing"], $post_data);
                }
                
//                $shippingMethod = $this->getRequest()->getPost('shipping_method');
//                if (empty($shippingMethod)) {
//                    $this->messageManager->addError(_("Invalid shipping method."));
//                    $this->_redirect('supercheckout');
//                }
//                $shippingAddress = $this->quote->getQuote()->getShippingAddress();
//                $this->quote->setData("shipping_method_set", $shippingMethod);
//                $rate = $shippingAddress->getShippingRateByCode($shippingMethod);
//                if (!$rate) {
//                    $this->messageManager->addError(_("Invalid shipping method."));
//                    $this->_redirect('supercheckout');
//                } else {
//                    $shippingDescription = $rate->getCarrierTitle() . ' - ' . $rate->getMethodTitle();
//                    $shippingAddress->setShippingDescription(trim($shippingDescription, ' -'));
//                }
//                $shippingAddress->setShippingMethod($shippingMethod)->save();
                
                $this->quote->getQuote()->collectTotals()->save();
                
                
                $payment_method = $this->quote->getQuote()->getPayment()->getMethod();
                if ($payment_method == 'checkout_finland' || $payment_method == 'paypal_express') {
                    //BOC-- Code to redirect the user to payment redirect url 
                    if ($this->quote->getQuote()->getPayment()->getCheckoutRedirectUrl()) {
                        $resultRedirect = $this->resultRedirectFactory->create();
                        $resultRedirect->setUrl($this->quote->getQuote()->getPayment()->getCheckoutRedirectUrl());
                        return $resultRedirect;
                    }
                }               
                //EOC
                
                $order_id = $this->sc_quotemanagement->placeOrder($this->quote->getQuote()->getId());
                if ($order_id) {
                    //BOC-- Code to redirect the user to checkout finland payment redirect url 
                    if ($payment_method == 'checkout_finland') {
                        $this->_redirect('checkoutfinland/payment/methods');
                }
                    else {
//                         $this->_redirect('supercheckout/order/success');
                         $this->_redirect('checkout/onepage/success');
                    }
                    
                    //EOC
                }
            } catch (PaymentException $e) {
                $this->messageManager->addError(__($e->getMessage()));
                $this->_redirect('supercheckout');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
                $this->_objectManager->get('Magento\Checkout\Helper\Data')
                        ->sendPaymentFailedEmail($this->quote->getQuote(), $e->getMessage());
                $this->messageManager->addError(__($e->getMessage()));
                $this->_redirect('supercheckout');
            } catch (\Exception $e) {
                $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
                $this->_objectManager->get('Magento\Checkout\Helper\Data')
                        ->sendPaymentFailedEmail($this->quote->getQuote(), $e->getMessage());
                $this->messageManager->addError(__($e->getMessage()));
                $this->_redirect('supercheckout');
            } catch (InputException $e) {
                $this->messageManager->addError($this->escaper->escapeHtml($e->getMessage()));
                foreach ($e->getErrors() as $error) {
                    $this->messageManager->addError($this->escaper->escapeHtml($error->getMessage()));
                }
                $this->_redirect('supercheckout');
            }
        } else {
            $this->messageManager->addError("Something went wrong.");
            $this->_redirect('supercheckout');
        }
        return $result;
    }

    public function validateShippingMethods($post_data)
    {

        $settings = $this->sc_helper->getSettings();
        $errors = [];
        if (isset($settings["supercheckout"]["enable_shipping_methods"]) && $settings["supercheckout"]["enable_shipping_methods"]) {
            if (!isset($post_data["shipping_method"])) {
                $errors[] = __("Please select a shipping method.");
            }
        }
        return $errors;
    }

    public function validatePaymentMethods($post_data)
    {
        $settings = $this->sc_helper->getSettings();
        $errors = [];
        if (isset($settings["supercheckout"]["enable_payment_methods"]) && $settings["supercheckout"]["enable_payment_methods"]) {
            if (!isset($post_data["payment"])) {
                $errors[] = __("Please select a payment method.");
            }
        }
        return $errors;
    }

    public function validateAddress()
    {
        $errors = [];
        if ($this->quote->getQuote()->getBillingAddress()->validate() !== true) {
            $this->messageManager->addError(__("Please check the billing address information."));
            $errors = $this->quote->getQuote()->getBillingAddress()->validate();
            if ($errors === true) {
                return [];
            } else {
                return $errors;
            }
        }
        if ($this->quote->getQuote()->getShippingAddress()->validate() !== true) {
            $this->messageManager->addError(__("Please check the shipping address information."));
            $errors = $this->quote->getQuote()->getShippingAddress()->validate();
            if ($errors === true) {
                return [];
            } else {
                return $errors;
            }
        }
        return $errors;
    }

    public function validateLogin($post_data)
    {
        $errors = [];
        if ($post_data["login"]["process"] == "guest") {
            $this->quote->setData("sc-login-type", "guest");
            $this->quote->setData("sc-email", $post_data["login"]["username"]);
            if (!isset($post_data["login"]["username"]) || trim($post_data["login"]["username"]) == "") {
                $errors[] = __("Please enter Email ID.");
            } elseif (!\Zend_Validate::is($post_data["login"]["username"], 'EmailAddress')) {
                $errors[] = __("Please enter a valid Email ID.");
            }
        }
        if ($post_data["login"]["process"] == "register") {
            $this->quote->setData("sc-login-type", "register");
            $this->quote->setData("sc-email", $post_data["login"]["username"]);
            $this->quote->setData("sc-password", $post_data["register"]["password"]);
            $this->quote->setData("sc-confirm-password", $post_data["register"]["confirm_password"]);
            
            $dob = date('m/d/Y', strtotime($post_data["register"]["dob"]));
            $this->quote->setData("sc-dob", isset($post_data["register"]["dob"])?$dob:"");
            $this->quote->setData("sc-gender", isset($post_data["register"]["gender"])?$post_data["register"]["gender"]:"");
            $this->quote->setData("sc-taxvat", isset($post_data["register"]["taxvat"])?$post_data["register"]["taxvat"]:"");
            $customer = $this->customerFactory->create();
            $websiteId = $this->sc_storeManager->getWebsite()->getWebsiteId();

            try {
                $url = $this->sc_urlInterface->getUrl('customer/account/forgotpassword');
                $customer->setWebsiteId($websiteId);
                $customer->loadByEmail($post_data["login"]["username"]);
                if ($customer->getId()) {
                    $errors[] = __('There is already an account with this email address. If you are sure that it is your email address, <a href="%1">click here</a> to get your password and access your account.', $url);
                }
            } catch (Exception $e) {
                /** Catch Statement */
            }
            if (empty($errors)) {
                if (!isset($post_data["login"]["username"]) || trim($post_data["login"]["username"]) == "") {
                    $errors[] = __("Please enter Email ID.");
                } elseif (!\Zend_Validate::is($post_data["login"]["username"], 'EmailAddress')) {
                    $errors[] = __("Please enter a valid Email ID.");
                }
                $password = $post_data["register"]["password"];
                if (!\Zend_Validate::is($password, 'NotEmpty')) {
                    $errors[] = __('The password cannot be empty.');
                }
                if ($password && !\Zend_Validate::is($password, 'StringLength', [6])) {
                    $errors[] = __('The minimum password length is 6');
                }
                $confirmation = $post_data["register"]["confirm_password"];
                if ($password != $confirmation) {
                    $errors[] = __('Please make sure your passwords match.');
                }
                if (($this->sc_scopeConfig->getValue('customer/address/dob_show', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) === 'req') && trim($post_data["register"]["dob"]) == '') {
                    $errors[] = __('The Date of Birth is required.');
                }
                if (($this->sc_scopeConfig->getValue('customer/address/gender_show', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) === 'req') && trim($post_data["register"]["gender"]) == '') {
                    $errors[] = __('Gender is required.');
                }
                if (($this->sc_scopeConfig->getValue('customer/address/taxvat_show', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) === 'req') && trim($post_data["register"]["taxvat"]) == '') {
                    $errors[] = __('Tax/Vat number is required.');
                }
            }
        }
        if ($post_data["login"]["process"] == "login") {
            $this->quote->setData("sc-login-type", "login");
            if (!$this->sc_customerSession->isLoggedIn()) {
                $errors[] = __("Please login to proceed or switch to guest checkout or register yourself.");
            }
        }
        if ($this->sc_customerSession->isLoggedIn()) {
            if ($this->sc_customerSession->getCustomer()->getDob() == "1900-01-01") {
                if (($this->sc_scopeConfig->getValue('customer/address/dob_show', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) === 'req') && trim($post_data["register"]["dob"]) == '') {
                    $errors[] = __('The Date of Birth is required.');
                }
            }
            if ($this->sc_customerSession->getCustomer()->getTaxvat() == "xxxx-xxxx") {
                if (($this->sc_scopeConfig->getValue('customer/address/taxvat_show', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) === 'req') && trim($post_data["register"]["taxvat"]) == '') {
                    $errors[] = __('Tax/Vat number is required.');
                }
            }
            if ($this->sc_customerSession->getCustomer()->getGender() == null) {
                if (($this->sc_scopeConfig->getValue('customer/address/gender_show', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) === 'req') && trim($post_data["register"]["gender"]) == '') {
                    $errors[] = __('Gender is required.');
                }
            }
        }
        return $errors;
    }

    public function getCheckoutMethod($login_type, $email)
    {
        if ($this->sc_customerSession->isLoggedIn()) {
            return self::CUSTOMER;
        }

        if (!$this->quote->getQuote()->getCheckoutMethod()) {
            if ($login_type == "guest") {
                $this->quote->getQuote()->setCheckoutMethod(self::GUEST);
                $this->prepareGuestQuote($email);
            } else {
                $this->quote->getQuote()->setCheckoutMethod(self::REGISTER);
            }
        }
        return $this->quote->getQuote()->getCheckoutMethod();
    }

    public function saveBillingAddress($billing_data, $post_data)
    {
        try {
            $address_id = isset($billing_data["address_id"]) ? $billing_data["address_id"] : "new";
            if ($address_id == "new") {
                unset($billing_data["address_id"]);
                $shipping_address = null;
                $shipping_address = $this->quote->getQuote()->getShippingAddress();
                if (isset($billing_data["same_shipping"]) && $billing_data["same_shipping"]) {
                    $street = [];
                    $street[] = $billing_data["street1"];
                    if (isset($billing_data["street2"])) {
                        $street[] = $billing_data["street2"];
                    }
                    $billing_data["street"] = $street;
                    $shipping_address->setShouldIgnoreValidation(true);
                    $shipping_address->addData($billing_data)->setCollectShippingRates(true)->collectShippingRates();
                    $this->quote->getQuote()->save();
                }
                $street = [];
                $street[] = $billing_data["street1"];
                if (isset($billing_data["street2"])) {
                    $street[] = $billing_data["street2"];
                }
                $billing_data["street"] = $street;
                $billing_address = $this->quote->getQuote()->getBillingAddress();
                $billing_address->addData($billing_data)->setCollectShippingRates(true)->collectShippingRates();
                if ($this->getCheckoutMethod($post_data["login"]["process"], $post_data["login"]["username"]) != "guest") {
                    $customerBillingData = $billing_address->exportCustomerAddress();
                    $dataArray = $this->sc_objectCopyService->getDataFromFieldset('checkout_onepage_quote', 'to_customer', $this->quote->getQuote());
                    $this->dataObjectHelper->populateWithArray(
                        $this->quote->getQuote()->getCustomer(),
                        $dataArray,
                        '\Magento\Customer\Api\Data\CustomerInterface'
                    );
                    $customerBillingData->setIsDefaultBilling(true);
                    if ($shipping_address != null) {
                        $customerShippingData = $shipping_address->exportCustomerAddress();
                        $customerShippingData->setIsDefaultShipping(true);
                    }
                    $billing_address->setCustomerAddressData($customerBillingData);
                    $this->quote->getQuote()->addCustomerAddress($customerBillingData);
                }
                $this->totalsCollector->collectAddressTotals($this->quote->getQuote(), $billing_address);
                $billing_address->setShouldIgnoreValidation(true);
                $billing_address->save();
                $this->quote->getQuote()->save();
            } else {
                $billing_address = $this->quote->getQuote()->getBillingAddress();
                $billing_address_data = null;

                try {
                    $billing_address_data = $this->addressRepository->getById($address_id);
                } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
                    /** Catch Statement */
                }
                if ($billing_address_data->getCustomerId() != $this->quote->getQuote()->getCustomerId()) {
                    return ['error' => 1, 'message' => __('The customer address is not valid.')];
                }
                $billing_address->importCustomerAddressData($billing_address_data)->setSaveInAddressBook(0);
                $billing_address->setCollectShippingRates(true)->collectShippingRates();
                $this->totalsCollector->collectAddressTotals($this->quote->getQuote(), $billing_address);
                $billing_address->setShouldIgnoreValidation(true);
                $billing_address->save();
                $this->quote->getQuote()->save();
                if (isset($billing_data["same_shipping"]) && $billing_data["same_shipping"]) {
                    $shipping_address = $this->quote->getQuote()->getShippingAddress();
                    $shipping_address_data = null;
                    try {
                        $shipping_address_data = $this->addressRepository->getById($address_id);
                    } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
                        // do nothing if customer is not found by id
                    }

                    if ($shipping_address_data->getCustomerId() != $this->quote->getQuote()->getCustomerId()) {
                        return ['error' => 1, 'message' => __('The customer address is not valid.')];
                    }
                    $shipping_address->importCustomerAddressData($shipping_address_data)->setSaveInAddressBook(0);
                    $shipping_address->setCollectShippingRates(true)->collectShippingRates();
                    $this->totalsCollector->collectAddressTotals($this->quote->getQuote(), $shipping_address);
                    $shipping_address->setShouldIgnoreValidation(true);
                    $shipping_address->save();
                    $this->quote->getQuote()->save();
                }
            }
        } catch (\Magento\Framework\Exception\InputException $e) {
            /** Catch Statement */
        }
    }

    public function prepareGuestQuote($email)
    {
        $quote = $this->quote->getQuote();
        $quote->setCustomerId(null)->setCustomerEmail($email);
        return $this;
    }

    public function saveShippingAddress($shipping_data, $billing_data, $post_data)
    {
        try {
            $address_id = isset($shipping_data["address_id"]) ? $shipping_data["address_id"] : "new";
            if ($address_id == "new") {
                unset($shipping_data["address_id"]);
                $street = [];
                $street[] = $shipping_data["street1"];
                if (isset($shipping_data["street2"])) {
                    $street[] = $shipping_data["street2"];
                }
                $shipping_data["street"] = $street;
                $this->quote->setData("shipping_address_id", $address_id);
                $shipping_address = $this->quote->getQuote()->getShippingAddress();
                $shipping_address->addData($shipping_data)->setCollectShippingRates(true)->collectShippingRates();
                if ($billing_data["address_id"] != "new") {
                    if ($this->getCheckoutMethod($post_data["login"]["process"], $post_data["login"]["username"]) != "guest") {
                        $customerShippingData = $shipping_address->exportCustomerAddress();
                        $dataArray = $this->sc_objectCopyService->getDataFromFieldset('checkout_onepage_quote', 'to_customer', $this->quote->getQuote());
                        $this->dataObjectHelper->populateWithArray(
                            $this->quote->getQuote()->getCustomer(),
                            $dataArray,
                            '\Magento\Customer\Api\Data\CustomerInterface'
                        );
                        $customerShippingData->setIsDefaultShipping(true);
                        $shipping_address->setCustomerAddressData($customerShippingData);
                        $this->quote->getQuote()->addCustomerAddress($customerShippingData);
                    }
                }
                $this->totalsCollector->collectAddressTotals($this->quote->getQuote(), $shipping_address);
                $shipping_address->save();
                $this->quote->getQuote()->save();
            } else {
                $shipping_address = $this->quote->getQuote()->getShippingAddress();
                $shipping_address_data = null;
                try {
                    $shipping_address_data = $this->addressRepository->getById($address_id);
                } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
                    // do nothing if customer is not found by id
                }

                if ($shipping_address_data->getCustomerId() != $this->quote->getQuote()->getCustomerId()) {
                    return ['error' => 1, 'message' => __('The customer address is not valid.')];
                }
                $this->quote->setData("shipping_address_id", $address_id);
                $shipping_address->importCustomerAddressData($shipping_address_data)->setSaveInAddressBook(0);
                $shipping_address->setCollectShippingRates(true)->collectShippingRates();
                $this->totalsCollector->collectAddressTotals($this->quote->getQuote(), $shipping_address);
                $shipping_address->save();
                $this->quote->getQuote()->save();
            }
        } catch (Exception $e) {
            /** Catch Statement */
        }
    }

    public function saveLoggedInCustomer($post_data)
    {
        if (isset($post_data["register"])) {
            $customer_id = $this->sc_customerSession->getCustomerId();
            $customer = $this->customerFactory->create()->load($customer_id);
            if (isset($post_data["register"]["dob"])) {
                $dob = date('m/d/Y', strtotime($post_data["register"]["dob"]));
                $customer->setDob($dob);
            }
            if (isset($post_data["register"]["taxvat"])) {
                $customer->setTaxvat($post_data["register"]["taxvat"]);
            }
            if (isset($post_data["register"]["gender"])) {
                $customer->setGender($post_data["register"]["gender"]);
            }
            $customer->save();
        }
    }

    public function saveCustomer($post_data)
    {

        $email = isset($post_data["login"]["username"]) ? $post_data["login"]["username"] : "";
        $firstname = isset($post_data["billing"]["firstname"]) ? $post_data["billing"]["firstname"] : "N/A";
        $lastname = isset($post_data["billing"]["lastname"]) ? $post_data["billing"]["lastname"] : "N/A";
        $gender = isset($post_data["register"]["gender"]) && $post_data["register"]["gender"] ? $post_data["register"]["gender"] : "1";
        $dob = isset($post_data["register"]["dob"]) && $post_data["register"]["dob"] ? $post_data["register"]["dob"] : "01/01/1900";
        $dob = date('m/d/Y', strtotime($dob));
        $taxvat = isset($post_data["register"]["taxvat"]) && $post_data["register"]["taxvat"] ? $post_data["register"]["taxvat"] : "xxxx-xxxx";
        $prefix = isset($post_data["billing"]["prefix"]) && $post_data["billing"]["prefix"] ? $post_data["billing"]["prefix"] : ".";
        $suffix = isset($post_data["billing"]["suffix"]) && $post_data["billing"]["suffix"] ? $post_data["billing"]["suffix"] : ".";
        $password = isset($post_data["register"]["password"]) && $post_data["register"]["password"] ? $post_data["register"]["password"] : mt_rand();
        $customer = $this->customerFactory->create();
        $websiteId = $this->sc_storeManager->getWebsite()->getWebsiteId();
        try {
            $settings = $this->sc_helper->getSettings();
            $customer->setWebsiteId($websiteId);
            $customer->loadByEmail($email);
            if ($email && $firstname) {
                $customer->setEmail($email);
                $customer->setFirstname($firstname);
                $customer->setLastname($lastname);
                $customer->setPassword($password);
                $customer->setGender($gender);
                $customer->setPrefix($prefix);
                $customer->setSuffix($suffix);
                $customer->setDob($dob);
                $customer->setTaxvat($taxvat);
                $customer->save();
                $this->sc_customerSession->setCustomerAsLoggedIn($customer);
                if ($post_data["login"]["process"] == "guest" && isset($settings["supercheckout"]["guest_registration"]) && $settings["supercheckout"]["guest_registration"]) {
                    $this->sc_helper->sendRegistrationEmail($email, $password);
                }
            }
        } catch (StateException $e) {
            $url = $this->sc_urlInterface->getUrl('customer/account/forgotpassword');
            $message = __(
                'There is already an account with this email address. If you are sure that it is your email address, <a href="%1">click here</a> to get your password and access your account.',
                $url
            );
            $this->messageManager->addError($message);
            $this->_redirect('supercheckout');
        } catch (InputException $e) {
            $this->messageManager->addError($this->escaper->escapeHtml($e->getMessage()));
            foreach ($e->getErrors() as $error) {
                $this->messageManager->addError($this->escaper->escapeHtml($error->getMessage()));
            }
            $this->_redirect('supercheckout');
        } catch (LocalizedException $e) {
            $this->messageManager->addError($this->escaper->escapeHtml($e->getMessage()));
            $this->_redirect('supercheckout');
        } catch (\Exception $e) {
            $this->messageManager->addException($e, __('We can\'t save the customer.'));
            $this->_redirect('supercheckout');
        } catch (\Magento\Framework\Validator\Exception $e) {
            $this->messageManager->addException($this->escaper->escapeHtml($e->getMessage()));
            $this->_redirect('supercheckout');
        }
    }
}
