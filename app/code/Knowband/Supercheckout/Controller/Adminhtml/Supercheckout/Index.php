<?php

namespace Knowband\Supercheckout\Controller\Adminhtml\Supercheckout;

use Magento\Framework\Controller\ResultFactory;

class Index extends \Magento\Backend\App\Action
{

    public $resultPageFactory = false;
    public $sc_request;
    public $sc_resource;
    public $sc_storeManager;
    public $sc_helper;
    public $sc_helperAttr;
    public $sc_cacheFrontendPool;
    public $sc_cacheTypeList;

    /*
     *  Function - Modified by RS on 08-Sept-2017 for fixing the issue "able to hit the save button but settings do not successfully remain after save, resetting to being off" - reported by Magento Team
     */
    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\App\Request\Http $request, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Knowband\Supercheckout\Helper\Data $helper, \Knowband\Supercheckout\Helper\Attributes $helperAttr, \Magento\Framework\App\Config\ConfigResource\ConfigInterface $resource, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList, \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool)
    {
        parent::__construct($context);
        $this->sc_request = $request;
        $this->resultPageFactory = $resultPageFactory;
        $this->sc_resource = $resource;
        $this->sc_storeManager = $storeManager;
        $this->sc_helper = $helper;
        $this->sc_helperAttr = $helperAttr;
        $this->sc_cacheFrontendPool = $cacheFrontendPool;
        $this->sc_cacheTypeList = $cacheTypeList;
    }

    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Knowband_Supercheckout::supercheckout_backend');
        $resultPage->getConfig()->getTitle()->prepend(__('Supercheckout'));
        $resultPage->addBreadcrumb(__('Knowband'), __('Knowband'));
        $resultPage->addBreadcrumb(__('Supercheckout'), __('Supercheckout'));

        if ($this->getRequest()->getParam('store')) {
            $scope_id = $this->sc_storeManager->getStore($this->getRequest()->getParam('store'))->getId();
            $scope = "stores";
        } elseif ($this->getRequest()->getParam('website')) {
            $scope_id = $this->sc_storeManager->getWebsite($this->getRequest()->getParam('website'))->getId();
            $scope = "websites";
        } elseif ($this->getRequest()->getParam('group')) {
            $scope_id = $this->sc_storeManager->getGroup($this->getRequest()->getParam('group'))->getWebsite()->getId();
            $scope = "groups";
        } else {
            $scope = "default";
            $scope_id = 0;
        }
        if ($this->sc_request->isPost()) {
            $unset_fb = false;
            $unset_g = false;
            $post_data = $this->sc_request->getPost();
            unset($post_data["form_key"]);
            if (isset($post_data['supercheckout']['enable_facebook_login']) && $post_data['supercheckout']['enable_facebook_login'] == 1) {
                if ((isset($post_data['supercheckout']['facebook_app_secret']) && trim($post_data['supercheckout']['facebook_app_secret']) == '') || (isset($post_data['supercheckout']['facebook_app_id']) && trim($post_data['supercheckout']['facebook_app_id']) == '')) {
                    $unset_fb = true;
                    $this->messageManager->addWarning(__('Data provided for Facebook Login was incomplete. Not able to enable Facebook Login.'));
                }
            }
            if (isset($post_data['supercheckout']['enable_google_login']) && $post_data['supercheckout']['enable_google_login'] == 1) {
                if ((isset($post_data['supercheckout']['google_app_secret']) && trim($post_data['supercheckout']['google_app_secret']) == '') || (isset($post_data['supercheckout']['google_client_id']) && trim($post_data['supercheckout']['google_client_id']) == '') || (isset($post_data['supercheckout']['google_app_id']) && trim($post_data['supercheckout']['google_app_id']) == '')) {
                    $unset_g = true;
                    $this->messageManager->addWarning(__('Data provided for Google Login was incomplete. Not able to enable Google Login.'));
                }
            }
            $value = json_encode($post_data);
            $this->requireAttributeCheck($post_data);
            $this->sc_resource->saveConfig("knowband/supercheckout/settings", $value, $scope, $scope_id);
            if ($unset_fb || $unset_g) {
                $updated_settings = $this->sc_helper->getSavedSettings($scope, $scope_id);
                if ($unset_fb) {
                    unset($updated_settings['supercheckout']['enable_facebook_login']);
                }
                if ($unset_g) {
                    unset($updated_settings['supercheckout']['enable_google_login']);
                }
                $this->sc_resource->saveConfig("knowband/supercheckout/settings", json_encode($updated_settings), $scope, $scope_id);
            }
            $this->messageManager->addSuccess(__('Settings saved successfully.'));
            /* Start - Code Added by RS on 08-Sept-2017 for fixing the issue "able to hit the save button but settings do not successfully remain after save, resetting to being off" - reported by Magento Team */
            $types = array('config');
            foreach ($types as $type) {
                $this->sc_cacheTypeList->cleanType($type);
            }
            foreach ($this->sc_cacheFrontendPool as $cacheFrontend) {
                $cacheFrontend->getBackend()->clean();
            }
            /* End - Code Added by RS on 08-Sept-2017 for fixing the issue "able to hit the save button but settings do not successfully remain after save, resetting to being off" - reported by Magento Team */
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setUrl('*/*/');
            return $resultRedirect;
        }

        return $resultPage;
    }

    public function _isAllowed()
    {
        return $this->_authorization->isAllowed('Knowband_Supercheckout::supercheckout');
    }

    public function requireAttributeCheck()
    {
        $this->sc_helperAttr->unrequireAttribute("firstname");
        $this->sc_helperAttr->unrequireAttribute("lastname");
        /* Start - Code Modified by RS on 08-Sept-2017 for fixing "Attribute with ID: \"tax\" does not exist" issue - reported by Magento Team */
        $this->sc_helperAttr->unrequireAttribute("taxvat");
        /* End - Code Modified by RS on 08-Sept-2017 for fixing "Attribute with ID: \"tax\" does not exist" issue - reported by Magento Team */
        $this->sc_helperAttr->unrequireAttribute("fax");
        $this->sc_helperAttr->unrequireAttribute("street");
        $this->sc_helperAttr->unrequireAttribute("prefix");
        $this->sc_helperAttr->unrequireAttribute("suffix");
        $this->sc_helperAttr->unrequireAttribute("middlename");
        $this->sc_helperAttr->unrequireAttribute("city");
        $this->sc_helperAttr->unrequireAttribute("country_id");
        $this->sc_helperAttr->unrequireAttribute("region");
        $this->sc_helperAttr->unrequireAttribute("region_id");
        $this->sc_helperAttr->unrequireAttribute("postcode");
        $this->sc_helperAttr->unrequireAttribute("telephone");
    }
}
