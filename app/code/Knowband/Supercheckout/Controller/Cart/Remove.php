<?php

namespace Knowband\Supercheckout\Controller\Cart;

class Remove extends \Magento\Framework\App\Action\Action
{

    public $sc_scopeConfig;
    public $sc_storeManager;
    public $cart;
    public $resultJsonFactory;
    public $sc_formKeyValidator;
    public $sc_urlInterface;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Checkout\Model\Cart $cart, \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory, \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator)
    {
        $this->sc_scopeConfig = $scopeConfig;
        $this->sc_storeManager = $storeManager;
        $this->cart = $cart;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->sc_formKeyValidator = $formKeyValidator;
        $this->sc_urlInterface = $context->getUrl();
        parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        if (!$this->sc_formKeyValidator->validate($this->getRequest())) {
            $result->setData(['redirect' => $this->sc_urlInterface->getUrl('supercheckout')]);
        }
        if ($this->getRequest()->isPost()) {
            $id = (int) $this->getRequest()->getPost('id');
            if ($id) {
                try {
                    $this->cart->removeItem($id)->save();
                    $result->setData(['success' => __('Item removed successfully.')]);
                } catch (\Exception $e) {
                    $result->setData(['error' => __('We can\'t remove the item.')]);
                }
            }
        } else {
            $result->setData(['error' => __("Something went wrong. Item cannot be removed.")]);
        }
        return $result;
    }
}
