<?php

namespace Knowband\Supercheckout\Controller\Cart;

class Voucher extends \Magento\Framework\App\Action\Action
{

    public $sc_scopeConfig;
    public $sc_storeManager;
    public $cart;
    public $resultJsonFactory;
    public $sc_checkoutSession;
    public $couponFactory;
    public $quoteRepository;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Checkout\Model\Session $checkoutSession, \Magento\Checkout\Model\Cart $cart, \Magento\SalesRule\Model\CouponFactory $couponFactory, \Magento\Quote\Api\CartRepositoryInterface $quoteRepository, \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory)
    {
        $this->sc_scopeConfig = $scopeConfig;
        $this->sc_storeManager = $storeManager;
        $this->cart = $cart;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->sc_checkoutSession = $checkoutSession;
        $this->couponFactory = $couponFactory;
        $this->quoteRepository = $quoteRepository;
        parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        if ($this->getRequest()->isPost()) {
            $couponCode = $this->getRequest()->getPost('remove') == 1 ? '' : trim($this->getRequest()->getPost('voucher'));

            $cartQuote = $this->cart->getQuote();
            $oldCouponCode = $cartQuote->getCouponCode();

            $codeLength = strlen($couponCode);
            if (!$codeLength && !strlen($oldCouponCode)) {
                $result->setData(['error' => __('Please enter a voucher code.')]);
                return $result;
            }
            try {
                $isCodeLengthValid = $codeLength && $codeLength <= \Magento\Checkout\Helper\Cart::COUPON_CODE_MAX_LENGTH;
                $itemsCount = $cartQuote->getItemsCount();
                if ($itemsCount) {
                    $cartQuote->getShippingAddress()->setCollectShippingRates(true);
                    $cartQuote->setCouponCode($isCodeLengthValid ? $couponCode : '')->collectTotals();
                    $this->quoteRepository->save($cartQuote);
                }

                if ($codeLength) {
                    $escaper = $this->_objectManager->get('Magento\Framework\Escaper');
                    if (!$itemsCount) {
                        if ($isCodeLengthValid) {
                            $coupon = $this->couponFactory->create();
                            $coupon->load($couponCode, 'code');
                            if ($coupon->getId()) {
                                $this->sc_checkoutSession->getQuote()->setCouponCode($couponCode)->save();
                                $result->setData(['success' => __('You used coupon code "%1".', $escaper->escapeHtml($couponCode))]);
                            } else {
                                $result->setData(['error' => __('The coupon code "%1" is not valid.', $escaper->escapeHtml($couponCode))]);
                            }
                        } else {
                            $result->setData(['error' => __('The coupon code "%1" is not valid.', $escaper->escapeHtml($couponCode))]);
                        }
                    } else {
                        if ($isCodeLengthValid && $couponCode == $cartQuote->getCouponCode()) {
                            $result->setData(['success' => __('You used coupon code "%1".', $escaper->escapeHtml($couponCode))]);
                        } else {
                            $result->setData(['error' => __('The coupon code "%1" is not valid.', $escaper->escapeHtml($couponCode))]);
                            $this->cart->save();
                        }
                    }
                } else {
                    $result->setData(['success' => __('You canceled the coupon code.')]);
                }
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $result->setData(['error' => $e->getMessage()]);
            } catch (\Exception $e) {
                $result->setData(['error' => __('We cannot apply the coupon code.')]);
                $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
            }
        } else {
            $result->setData(['error' => __("Something went wrong. Coupon cannot be applied.")]);
        }
        return $result;
    }
}
