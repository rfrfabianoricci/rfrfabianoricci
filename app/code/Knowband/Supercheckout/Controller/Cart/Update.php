<?php

namespace Knowband\Supercheckout\Controller\Cart;

class Update extends \Magento\Framework\App\Action\Action
{

    public $sc_scopeConfig;
    public $sc_storeManager;
    public $cart;
    public $resultJsonFactory;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Checkout\Model\Cart $cart, \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory)
    {
        $this->sc_scopeConfig = $scopeConfig;
        $this->sc_storeManager = $storeManager;
        $this->cart = $cart;
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        if ($this->getRequest()->isPost()) {
            try {
                $cart_post = $this->getRequest()->getPost('cart');
                if (is_array($cart_post)) {
                    $filter = new \Zend_Filter_LocalizedToNormalized(
                        ['locale' => $this->_objectManager->get('Magento\Framework\Locale\ResolverInterface')->getLocale()]
                    );
                    $cartData[$cart_post["id"]]['qty'] = $cart_post["qty"];
                    if (!$this->cart->getCustomerSession()->getCustomerId() && $this->cart->getQuote()->getCustomerId()) {
                        $this->cart->getQuote()->setCustomerId(null);
                    }
                    $cartData = $this->cart->suggestItemsQty($cartData);
                    $this->cart->updateItems($cartData)->save();
                    $result->setData(['success' => __('Item updated successfully.')]);
                }
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $result->setData(['error' => $this->_objectManager->get('Magento\Framework\Escaper')->escapeHtml($e->getMessage())]);
            } catch (\Exception $e) {
                $result->setData(['error' => __('We can\'t update the shopping cart.')]);
            }
        } else {
            $result->setData(['error' => __("Something went wrong. Cart cannot be updated.")]);
        }
        return $result;
    }
}
