<?php

namespace Knowband\Supercheckout\Controller\Login;

use Magento\Framework\App\Action\Context;

class SocialLoginCheck extends \Magento\Framework\App\Action\Action
{

    private $resultJsonFactory;
    public $sc_urlInterface;
    public $storeManager;
    public $customerFactory;
    public $sc_session;

    public function __construct(Context $context, \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Customer\Model\CustomerFactory $customerFactory, \Magento\Customer\Model\Session $session)
    {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->sc_urlInterface = $context->getUrl();
        $this->storeManager = $storeManager;
        $this->customerFactory = $customerFactory;
        $this->sc_session = $session;
    }

    public function execute()
    {
        $result = $this->resultJsonFactory->create();

        $result->setData(['registered' => true]);
        return $result;
    }
}
