<?php

namespace Knowband\Supercheckout\Controller\Login;

use Magento\Customer\Model\Account\Redirect as AccountRedirect;
use Magento\Framework\App\Action\Context;
use Magento\Customer\Model\Session;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Model\Url as CustomerUrl;
use Magento\Framework\Exception\EmailNotConfirmedException;
use Magento\Framework\Exception\AuthenticationException;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\State\UserLockedException;
use Magento\Framework\App\Config\ScopeConfigInterface;

class Login extends \Magento\Customer\Controller\AbstractAccount
{

    public $customerAccountManagement;
    public $formKeyValidator;
    public $accountRedirect;
    public $session;
    private $scopeConfig;
    private $cookieMetadataFactory;
    private $cookieMetadataManager;
    private $resultJsonFactory;
    public $sc_urlInterface;

    public function __construct(Context $context, Session $customerSession, AccountManagementInterface $customerAccountManagement, CustomerUrl $customerHelperData, Validator $formKeyValidator, AccountRedirect $accountRedirect, \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory)
    {
        parent::__construct($context);
        $this->session = $customerSession;
        $this->customerAccountManagement = $customerAccountManagement;
        $this->customerUrl = $customerHelperData;
        $this->formKeyValidator = $formKeyValidator;
        $this->accountRedirect = $accountRedirect;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->sc_urlInterface = $context->getUrl();
    }

    public function execute()
    {
        if ($this->session->isLoggedIn() || !$this->formKeyValidator->validate($this->getRequest())) {
            $result = $this->resultJsonFactory->create();
            $result->setData(['redirect' => $this->sc_urlInterface->getUrl('supercheckout')]);
            return $result;
        }

        if ($this->getRequest()->isPost()) {
            $login = $this->getRequest()->getPost('login');
            if (!empty($login['username']) && !empty($login['password'])) {
                try {
                    $customer = $this->customerAccountManagement->authenticate($login['username'], $login['password']);
                    $this->session->setCustomerDataAsLoggedIn($customer);
                    $this->session->regenerateId();
                    if ($this->getCookieManager()->getCookie('mage-cache-sessid')) {
                        $metadata = $this->getCookieMetadataFactory()->createCookieMetadata();
                        $metadata->setPath('/');
                        $this->getCookieManager()->deleteCookie('mage-cache-sessid', $metadata);
                    }
                    $this->accountRedirect->clearRedirectCookie();
                    $this->session->setUsername($login['username']);
                    $result = $this->resultJsonFactory->create();
                    $result->setData(['success' => __("Login Successful.")]);
                    return $result;
                } catch (EmailNotConfirmedException $e) {
                    $value = $this->customerUrl->getEmailConfirmationUrl($login['username']);
                    $this->session->setUsername($login['username']);
                    $result = $this->resultJsonFactory->create();
                    $result->setData(['error' => __(
                        'This account is not confirmed. <a href="%1">Click here</a> to resend confirmation email.',
                        $value
                    )]);
                    return $result;
                } catch (UserLockedException $e) {
                    $this->session->setUsername($login['username']);
                    $result = $this->resultJsonFactory->create();
                    $result->setData(['error' => __(
                        'The account is locked. Please wait and try again or contact %1.',
                        $this->getScopeConfig()->getValue('contact/email/recipient_email')
                    )]);
                    return $result;
                } catch (AuthenticationException $e) {
                    $this->session->setUsername($login['username']);
                    $result = $this->resultJsonFactory->create();
                    $result->setData(['error' => __('Invalid login or password.')]);
                    return $result;
                } catch (LocalizedException $e) {
                    $message = $e->getMessage();
                    $this->session->setUsername($login['username']);
                    $result = $this->resultJsonFactory->create();
                    $result->setData(['error' => $message]);
                    return $result;
                } catch (\Exception $e) {
                    // PA DSS violation: throwing or logging an exception here can disclose customer password
                    $result = $this->resultJsonFactory->create();
                    $result->setData(['error' => __('An unspecified error occurred. Please contact us for assistance.')]);
                    return $result;
                }
            } else {
                $result = $this->resultJsonFactory->create();
                $result->setData(['error' => __('A login and a password are required.')]);
                return $result;
            }
        }
        $result = $this->resultJsonFactory->create();
        $result->setData(['redirect' => $this->sc_urlInterface->getUrl('supercheckout')]);
        return $result;
    }

    private function getCookieManager()
    {
        if (!$this->cookieMetadataManager) {
            $this->cookieMetadataManager = \Magento\Framework\App\ObjectManager::getInstance()->get(
                \Magento\Framework\Stdlib\Cookie\PhpCookieManager::class
            );
        }
        return $this->cookieMetadataManager;
    }

    private function getCookieMetadataFactory()
    {
        if (!$this->cookieMetadataFactory) {
            $this->cookieMetadataFactory = \Magento\Framework\App\ObjectManager::getInstance()->get(
                \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory::class
            );
        }
        return $this->cookieMetadataFactory;
    }
}
