<?php

namespace Knowband\Supercheckout\Controller\Login;

use Magento\Framework\App\Action\Context;

class SocialRegister extends \Magento\Framework\App\Action\Action
{
    private $resultJsonFactory;
    public $sc_urlInterface;
    public $storeManager;
    public $customerFactory;
    public $sc_session;

    public function __construct(Context $context, \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Customer\Model\CustomerFactory $customerFactory, \Magento\Customer\Model\Session $session)
    {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->sc_urlInterface = $context->getUrl();
        $this->storeManager = $storeManager;
        $this->customerFactory = $customerFactory;
        $this->sc_session = $session;
    }

    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        if ($post_data = $this->getRequest()->getPost("login")) {
            $email = isset($post_data["email"]) ? $post_data["email"] : "";
            $firstname = isset($post_data["firstname"]) ? $post_data["firstname"] : "";
            $lastname = isset($post_data["lastname"]) ? $post_data["lastname"] : "";
            $gender = isset($post_data["gender"]) && $post_data["gender"] == "female" ? "2" : "1";
            $customer = $this->customerFactory->create();
            $websiteId = $this->storeManager->getWebsite()->getWebsiteId();
            try {
                $customer->setWebsiteId($websiteId);
                $customer->loadByEmail($email);
                if ($customer->getId()) {
                    $this->sc_session->setCustomerAsLoggedIn($customer);
                    $result->setData(['success' => __("Login Successful.")]);
                    return $result;
                } else {
                    if ($email && $firstname) {
                        $customer->setEmail($email);
                        $customer->setFirstname($firstname);
                        $customer->setLastname($lastname);
                        $customer->setPassword(mt_rand());
                        $customer->setGender($gender);
                        $customer->setPrefix(".");
                        $customer->setSuffix(".");
                        $customer->setDob("01/01/1900");
                        $customer->setTaxvat("xxxx-xxxx");
                        $customer->save();
                        $this->sc_session->setCustomerAsLoggedIn($customer);
                        $result->setData(['success' => __("Account Created Successfully.")]);
                        return $result;
                    } else {
                        $result->setData(['error' => __("Email is private. Please make it public to enable this feature.")]);
                        return $result;
                    }
                }
            } catch (\Exception $e) {
                $result->setData(['error' => $e->getMessage()]);
                return $result;
            }
        } else {
            $result->setData(['error' => __("Something went wrong. Please try again later.")]);
            return $result;
        }
    }
}
