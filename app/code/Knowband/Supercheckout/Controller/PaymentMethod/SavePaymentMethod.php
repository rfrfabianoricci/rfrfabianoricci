<?php

namespace Knowband\Supercheckout\Controller\PaymentMethod;

class SavePaymentMethod extends \Magento\Framework\App\Action\Action
{

    public $sc_scopeConfig;
    public $sc_storeManager;
    public $cart;
    public $resultJsonFactory;
    public $quote;
    public $paymentSpecification;
    public $quoteRepository;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Checkout\Model\Cart $cart, \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory, \Magento\Quote\Api\CartRepositoryInterface $quoteRepository, \Magento\Checkout\Model\Session $checkoutSession)
    {
        $this->sc_scopeConfig = $scopeConfig;
        $this->sc_storeManager = $storeManager;
        $this->cart = $cart;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->quote = $checkoutSession;
        $this->quoteRepository = $quoteRepository;
        parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        if ($this->getRequest()->isPost()) {
            try {
                $payment = [];
                $payment['method'] = $this->getRequest()->getPost('payment_method');
                if (empty($payment['method']) || !isset($payment['method'])) {
                    $result->setData(['error' => __("Invalid payment method.")]);
                    return $result;
                }
                $quote = $this->quote->getQuote();
                $this->quote->setData("payment_method_set", $payment['method']);
                $quote->getPayment()->importData($payment);
                if (!$quote->isVirtual() && $quote->getShippingAddress()) {
                    $quote->getShippingAddress()->setCollectShippingRates(true);
                    $quote->setTotalsCollectedFlag(false)->collectTotals();
                }
                $this->quoteRepository->save($quote);
                $result->setData(['success' => __("Payment method saved successfully.")]);
                return $result;
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $result->setData(['error' => $this->_objectManager->get('Magento\Framework\Escaper')->escapeHtml($e->getMessage())]);
            } catch (\Exception $e) {
                $result->setData(['error' => __('Payment Method cannot be saved.')]);
            }
        } else {
            $result->setData(['error' => __("Something went wrong. Payment Method cannot be saved.")]);
        }
        return $result;
    }
}
