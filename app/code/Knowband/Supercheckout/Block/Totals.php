<?php

namespace Knowband\Supercheckout\Block;

use Magento\Framework\View\Element\BlockInterface;
use Magento\Checkout\Block\Checkout\LayoutProcessorInterface;

class Totals extends \Magento\Checkout\Block\Cart\AbstractCart
{

    public $sc_totalRenderers;
    public $sc_defaultRenderer = 'Magento\Checkout\Block\Total\DefaultTotal';
    public $sc_totals = null;
    public $sc_salesConfig;
    public $layoutProcessors;

    public function __construct(\Magento\Framework\View\Element\Template\Context $context, \Magento\Customer\Model\Session $customerSession, \Magento\Checkout\Model\Session $checkoutSession, \Magento\Sales\Model\Config $salesConfig, array $layoutProcessors = [], array $data = [])
    {
        $this->sc_salesConfig = $salesConfig;
        parent::__construct($context, $customerSession, $checkoutSession, $data);
        $this->_isScopePrivate = true;
        $this->layoutProcessors = $layoutProcessors;
    }

    public function getJsLayout()
    {
        foreach ($this->layoutProcessors as $processor) {
            $this->jsLayout = $processor->process($this->jsLayout);
        }
        return parent::getJsLayout();
    }

    public function getTotals()
    {
        if ($this->sc_totals === null) {
            return parent::getTotals();
        }
        return $this->sc_totals;
    }

    public function getFormattedPrice($price)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $priceHelper = $objectManager->create('Magento\Framework\Pricing\Helper\Data');
        $formattedPrice = $priceHelper->currency($price, true, false);
        return $formattedPrice;
    }

    public function setTotals($value)
    {
        $this->sc_totals = $value;
        return $this;
    }

    public function _getTotalRenderer($code)
    {
        $blockName = $code . '_total_renderer';
        $block = $this->getLayout()->getBlock($blockName);
        if (!$block) {
            $renderer = $this->sc_salesConfig->getTotalsRenderer('quote', 'totals', $code);
            if (!empty($renderer)) {
                $block = $renderer;
            } else {
                $block = $this->sc_defaultRenderer;
            }

            $block = $this->getLayout()->createBlock($block, $blockName);
        }
        $block->setTotals($this->getTotals());
        return $block;
    }

    public function renderTotal($total, $area = null, $colspan = 1)
    {
        $code = $total->getCode();
        if ($total->getAs()) {
            $code = $total->getAs();
        }
        return $this->_getTotalRenderer($code)->setTotal($total)->setColspan($colspan)->setRenderingArea($area === null ? -1 : $area)->toHtml();
    }

    public function renderTotals($area = null, $colspan = 1)
    {
        $html = '';
        foreach ($this->getTotals() as $total) {
            if ($total->getArea() != $area && $area != -1) {
                continue;
            }
            $html .= $this->renderTotal($total, $area, $colspan);
        }
        return $html;
    }

    public function needDisplayBaseGrandtotal()
    {
        $quote = $this->getQuote();
        if ($quote->getBaseCurrencyCode() != $quote->getQuoteCurrencyCode()) {
            return true;
        }
        return false;
    }

    public function displayBaseGrandtotal()
    {
        $firstTotal = reset($this->sc_totals);
        if ($firstTotal) {
            $total = $firstTotal->getAddress()->getBaseGrandTotal();
            return $this->_storeManager->getStore()->getBaseCurrency()->format($total, [], true);
        }
        return '-';
    }

    public function getQuote()
    {
        if ($this->getCustomQuote()) {
            return $this->getCustomQuote();
        }

        if (null === $this->_quote) {
            $this->_quote = $this->_checkoutSession->getQuote();
        }
        return $this->_quote;
    }
}
