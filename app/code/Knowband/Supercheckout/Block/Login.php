<?php

namespace Knowband\Supercheckout\Block;

class Login extends \Knowband\Supercheckout\Block\SupercheckoutFront
{

    public function getDefaultRadioLogin()
    {
        if (null == $this->quote->getData("sc-login-type")) {
            if (!empty($this->sc_settings)) {
                if ($this->sc_settings["supercheckout"]["default_option"] == "guest" && isset($this->sc_settings["supercheckout"]["guest_checkout"]) && $this->sc_settings["supercheckout"]["guest_checkout"]) {
                    return $this->sc_settings["supercheckout"]["default_option"];
                }
                if ($this->sc_settings["supercheckout"]["default_option"] == "register" && isset($this->sc_settings["supercheckout"]["register_guest"]) && $this->sc_settings["supercheckout"]["register_guest"]) {
                    return $this->sc_settings["supercheckout"]["default_option"];
                }
                return 'login';
            } else {
                $this->sc_settings = $this->sc_helper->getSettings();
                $this->getDefaultRadioLogin();
            }
        } else {
            return $this->quote->getData("sc-login-type");
        }
    }

    public function isFBEnabled()
    {
        if (!empty($this->sc_settings)) {
            if (isset($this->sc_settings["supercheckout"]["enable_facebook_login"]) && isset($this->sc_settings["supercheckout"]["facebook_app_id"])) {
                if ($this->sc_settings["supercheckout"]["enable_facebook_login"] && $this->sc_settings["supercheckout"]["facebook_app_id"]) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
        return false;
    }

    public function isGoogleEnabled()
    {
        if (!empty($this->sc_settings)) {
            if (isset($this->sc_settings["supercheckout"]["enable_google_login"]) && isset($this->sc_settings["supercheckout"]["google_app_id"]) && isset($this->sc_settings["supercheckout"]["google_client_id"])) {
                if ($this->sc_settings["supercheckout"]["enable_google_login"] && $this->sc_settings["supercheckout"]["google_app_id"] && $this->sc_settings["supercheckout"]["google_client_id"]) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
        return false;
    }

    public function getCustomerForgotPasswordUrl()
    {
        return $this->getUrl('customer/account/forgotpassword');
    }

    public function getLoginUrl()
    {
        return $this->getUrl('supercheckout/login/login');
    }

    public function getLogoutUrl()
    {
        return $this->getUrl('customer/account/logout');
    }

    public function getMyAccountUrl()
    {
        return $this->getUrl('customer/account');
    }

    public function getDateFormat()
    {
        return $this->_localeDate->getDateFormat(\IntlDateFormatter::SHORT);
    }

    public function isLoggedIn()
    {
        if ($this->sc_customerSession->isLoggedIn()) {
            return true;
        }
        return false;
    }

    public function getGreeting()
    {
        if ($this->sc_customerSession->isLoggedIn()) {
            if (null != $this->sc_customerSession->getCustomer()->getName()) {
                return __("Welcome ") . $this->sc_customerSession->getCustomer()->getName();
            } else {
                return __("Welcome ");
            }
        } else {
            return false;
        }
    }

    public function ifDobExist()
    {
        if ($this->sc_customerSession->isLoggedIn()) {
            if (null != $this->sc_customerSession->getCustomer()->getDob()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function ifGenderExist()
    {
        if ($this->sc_customerSession->isLoggedIn()) {
            if (null != $this->sc_customerSession->getCustomer()->getGender()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
