<?php
namespace Knowband\Supercheckout\Block;

use Magento\Payment\Model\Method\AbstractMethod;
use Magento\Framework\Exception\LocalizedException;

class PaymentMethods extends \Magento\Payment\Block\Form\Container
{
    public $sc_multishipping;
    public $sc_checkoutSession;
    public $quoteRepository;
    public $paymentSpecification;
    public $sc_helper;
    public $quote;	

    public function __construct(\Magento\Framework\View\Element\Template\Context $context, \Magento\Payment\Helper\Data $paymentHelper, \Knowband\Supercheckout\Helper\Data $helper, \Magento\Payment\Model\Checks\SpecificationFactory $methodSpecificationFactory, \Magento\Multishipping\Model\Checkout\Type\Multishipping $multishipping, \Magento\Checkout\Model\Session $checkoutSession, \Magento\Quote\Api\CartRepositoryInterface $quoteRepository, array $data = [])
    {
        $this->sc_multishipping = $multishipping;
        $this->sc_checkoutSession = $checkoutSession;
        parent::__construct($context, $paymentHelper, $methodSpecificationFactory, $data);
        $this->_isScopePrivate = true;
        $this->sc_helper = $helper;
        $this->quote = $checkoutSession;
        $this->_settings = $this->sc_helper->getSettings();
        $this->quoteRepository = $quoteRepository;
    }

    public function _prepareLayout()
    {

        try {
            $quote = $this->quote->getQuote();
			$existing_payment_method = $this->sc_checkoutSession->getData("payment_method_set");
            $existing_shipping_method = $this->sc_checkoutSession->getData("shipping_method_set");
            $_payment_method = $existing_payment_method ? $existing_payment_method : $this->_settings["supercheckout"]["default_payment_method"];
            $_shipping_method = $existing_shipping_method ? $existing_shipping_method : $this->_settings["supercheckout"]["default_shipping_method"];
            $payment['method'] = $_payment_method;
            $quote->getPayment()->importData($payment);
            $quote->setShippingMethod($_shipping_method)->save();
            if (!$quote->isVirtual() && $quote->getShippingAddress()) {
                $quote->getShippingAddress()->setCollectShippingRates(true);
                $quote->setTotalsCollectedFlag(false)->collectTotals();
            }
            $this->quoteRepository->save($quote);            
        } catch (LocalizedException $e) {
            /** Catch Statement */
        }
        return parent::_prepareLayout();
    }

    public function _canUseMethod($method)
    {
        return $this->methodSpecificationFactory->create(
            [
                AbstractMethod::CHECK_USE_FOR_COUNTRY,
                AbstractMethod::CHECK_USE_FOR_CURRENCY,
                AbstractMethod::CHECK_ORDER_TOTAL_MIN_MAX,
            ]
        )->isApplicable($method, $this->getQuote());
    }

    public function getSelectedMethodCode()
    {
        $method = $this->getQuote()->getPayment()->getMethod();
        if ($method) {
            return $method;
        }
        return false;
    }

    public function getQuote()
    {
        return $this->sc_checkoutSession->getQuote();
    }

    public function getQuoteBaseGrandTotal()
    {
        return (double) $this->getQuote()->getBaseGrandTotal();
    }
}
