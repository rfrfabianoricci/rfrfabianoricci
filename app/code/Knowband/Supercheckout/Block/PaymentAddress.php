<?php

namespace Knowband\Supercheckout\Block;

use Magento\Framework\Exception\NoSuchEntityException;

class PaymentAddress extends \Knowband\Supercheckout\Block\SupercheckoutFront
{

    public function getAddressesHtmlSelect()
    {
        if (null != $this->quote->getData("billing_address_id")) {
            $address_id = $this->quote->getData("billing_address_id");
        } else {
            $address_id = $this->sc_customerSession->getCustomer()->getDefaultBilling();
        }

        if ($address_id != "new" && $address_id) {
            $this->saveBillingAddressDefault($address_id);
        }
        $select = $this->getLayout()->createBlock('Magento\Framework\View\Element\Html\Select')
                ->setName('billing[address_id]')
                ->setId('payment_address_select')
                ->setValue($address_id)
                ->setOptions($this->getAddressOptions());
        return $select->getHtml();
    }

    public function saveBillingAddressDefault($address_id)
    {
        $billing_address = $this->quote->getQuote()->getBillingAddress();
        $billing_address_data = null;
        try {
            $billing_address_data = $this->addressRepository->getById($address_id);
            if ($billing_address_data->getCustomerId() != $this->quote->getQuote()->getCustomerId()) {
                return ['error' => 1, 'message' => __('The customer address is not valid.')];
            }
            $this->quote->setData("billing_address_id", $address_id);
            $billing_address->importCustomerAddressData($billing_address_data)->setSaveInAddressBook(0);
            $billing_address->setCollectShippingRates(true)->collectShippingRates();
            $this->totalsCollector->collectAddressTotals($this->quote->getQuote(), $billing_address);
            $billing_address->save();
            $this->quote->getQuote()->save();
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            /** Catch Statement */
        }
    }

    public function getAddressOptions()
    {
        $options = [];
        $addresses = [];
        try {
            $addresses = $this->customerRepository->getById($this->sc_customerSession->getCustomerId())->getAddresses();
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            /** Customer does not exist */
        }
        /** @var \Magento\Customer\Api\Data\AddressInterface $address */
        foreach ($addresses as $address) {
            $label = $this->sc_addressConfig
                    ->getFormatByCode("oneline")
                    ->getRenderer()
                    ->renderArray($this->addressMapper->toFlatArray($address));

            $options[] = [
                'value' => $address->getId(),
                'label' => $label,
            ];
        }
        $options[] = [
            'value' => "new",
            'label' => __("New Address"),
        ];
        return $options;
    }

    public function showAddressDropdown()
    {
        if (!$this->sc_customerSession->isLoggedIn()) {
            return false;
        } else {
            if (count($this->getAddressOptions()) > 1) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public function isSameShippingSet()
    {
        if (null != $this->quote->getData("same_shipping")) {
            if ($this->quote->getData("same_shipping") == "yes") {
                return true;
            } else {
                return false;
            }
        }
        return true;
    }

    public function isNewPaymentSelected()
    {
        if (null != $this->quote->getData("billing_address_id")) {
            if ($this->quote->getData("billing_address_id") == "new") {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public function isSocialFirstTimeUser()
    {
        if ($this->sc_customerSession->isLoggedIn()) {
            if (count($this->getAddressOptions()) > 1) {
                return false;
            } else {
                return true;
            }
        }
        return false;
    }
}
