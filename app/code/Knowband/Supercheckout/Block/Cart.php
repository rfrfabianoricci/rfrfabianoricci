<?php

namespace Knowband\Supercheckout\Block;

class Cart extends \Knowband\Supercheckout\Block\SupercheckoutFront
{

    public function getCartItems()
    {
        $all_items = $this->quote->getQuote()->getAllVisibleItems();
        $items = [];
        $image_width = isset($this->sc_settings["supercheckout"]["cart"]["popup_image_width"]) ? $this->sc_settings["supercheckout"]["cart"]["popup_image_width"] : 230;
        $image_height = isset($this->sc_settings["supercheckout"]["cart"]["popup_image_height"]) ? $this->sc_settings["supercheckout"]["cart"]["popup_image_height"] : 230;
        foreach ($all_items as $_item) {
            $options = $_item->getProduct()->getTypeInstance(true)->getOrderOptions($_item->getProduct());
            $cart_option = [];
            if ($_item->getProduct()->getTypeId() == "bundle") {
                if (isset($options['bundle_options'])) {
                    foreach ($options['bundle_options'] as $option) {
                        $option_values = [];
                        foreach ($option['value'] as $values) {
                            $option_values[] = $values['qty'] . ' x ' . $values["title"] . ' x ' . $this->getPriceFormat($values["price"] / $values['qty']);
                        }
                        $cart_option[] = [
                            'label' => $option['label'],
                            'value' => implode(', ', $option_values)
                        ];
                    }
                }
            } else {
                if (isset($options['attributes_info'])) {
                    $customOptions = $options['attributes_info'];
                    if (!empty($customOptions)) {
                        foreach ($customOptions as $option) {
                            $cart_option[] = [
                                'label' => $option['label'],
                                'value' => $option['value']
                            ];
                        }
                    }
                }
            }
            $items[] = [
                'item_id' => $_item->getId(),
                'name' => $_item->getProduct()->getName(),
                'qty' => $_item->getQty(),
                'price' => $this->getPriceFormat($_item->getProduct()->getFinalPrice()),
                'options' => $cart_option,
                'image' => $this->sc_imageHelper->init($_item->getProduct(), 'product_small_image')->getUrl(),
                'popover_image' => $this->sc_imageHelper->init($_item->getProduct(), 'product_small_image')->resize($image_width, $image_height)->getUrl(),
                'product_url' => $_item->getProduct()->getProductUrl(),
                'item_object' => $_item
            ];
        }
        return $items;
    }

    public function couponCode()
    {
        return $this->quote->getQuote()->getCouponCode();
    }

    public function getPriceFormat($price)
    {
        return $this->priceCurrency->convertAndFormat($price, true, 2);
    }

    public function getSubtotalInclTax($item)
    {
        if ($item->getRowTotalInclTax()) {
            return $this->getPriceFormat($item->getRowTotalInclTax());
        }
        $tax = $item->getTaxAmount() + $item->getDiscountTaxCompensation();
        return $this->getPriceFormat($item->getRowTotal() + $tax);
    }

    public function getSubtotalExclTax($_item)
    {
        return $this->getPriceFormat($_item->getRowTotal());
    }

    public function getPriceInclTax($item)
    {
        if ($item->getPriceInclTax()) {
            return $this->getPriceFormat($item->getPriceInclTax());
        }
        $qty = $item->getQty() ? $item->getQty() : ($item->getQtyOrdered() ? $item->getQtyOrdered() : 1);
        $taxAmount = $item->getTaxAmount() + $item->getDiscountTaxCompensation();
        $price = floatval($qty) ? ($item->getRowTotal() + $taxAmount) / $qty : 0;
        return $this->getPriceFormat($this->priceCurrency->round($price));
    }

    public function getPriceExclTax($item)
    {
        return $this->getPriceFormat($item->getCalculationPrice());
    }

    protected function getUserType()
    {
        if ($this->sc_customerSession->isLoggedIn()) {
            return "login";
        }
        return "guest";
    }

    public function isCartEnable()
    {
        if (isset($this->sc_settings["supercheckout"]["cart"][$this->getUserType()]["cart"]["display"]) && $this->sc_settings["supercheckout"]["cart"][$this->getUserType()]["cart"]["display"]) {
            return true;
        }
        return false;
    }

    public function isPopUpEnable()
    {
        if (isset($this->sc_settings["supercheckout"]["cart"][$this->getUserType()]["image"]["display"]) && $this->sc_settings["supercheckout"]["cart"][$this->getUserType()]["image"]["display"]) {
            return true;
        }
        return false;
    }

    public function isProductNameEnable()
    {
        if (isset($this->sc_settings["supercheckout"]["cart"][$this->getUserType()]["name"]["display"]) && $this->sc_settings["supercheckout"]["cart"][$this->getUserType()]["name"]["display"]) {
            return true;
        }
        return false;
    }

    public function isProductQtyEnable()
    {
        if (isset($this->sc_settings["supercheckout"]["cart"][$this->getUserType()]["qty"]["display"]) && $this->sc_settings["supercheckout"]["cart"][$this->getUserType()]["qty"]["display"]) {
            return true;
        }
        return false;
    }

    public function isProductPriceEnable()
    {
        if (isset($this->sc_settings["supercheckout"]["cart"][$this->getUserType()]["price"]["display"]) && $this->sc_settings["supercheckout"]["cart"][$this->getUserType()]["price"]["display"]) {
            return true;
        }
        return false;
    }

    public function isTotalsEnable()
    {
        if (isset($this->sc_settings["supercheckout"]["cart"][$this->getUserType()]["totals"]["display"]) && $this->sc_settings["supercheckout"]["cart"][$this->getUserType()]["totals"]["display"]) {
            return true;
        }
        return false;
    }

    public function isVoucherEnable()
    {
        if (isset($this->sc_settings["supercheckout"]["cart"][$this->getUserType()]["voucher"]["display"]) && $this->sc_settings["supercheckout"]["cart"][$this->getUserType()]["voucher"]["display"]) {
            return true;
        }
        return false;
    }
}
