<?php

namespace Knowband\Supercheckout\Block\Adminhtml\Tab;

class ShippingAddress extends \Magento\Backend\Block\Widget implements \Magento\Backend\Block\Widget\Tab\TabInterface
{

    private $helper;
    private $scopeConfig;

    public function __construct(\Magento\Backend\Block\Template\Context $context, \Knowband\Supercheckout\Helper\Data $helper, array $data = [])
    {
        parent::__construct($context, $data);
        $this->helper = $helper;
        $this->scopeConfig = $context->getScopeConfig();
    }

    public function getTabLabel()
    {
        return __('Shipping Address');
    }

    public function getTabTitle()
    {
        return __('Shipping Address');
    }

    public function canShowTab()
    {
        return true;
    }

    public function isHidden()
    {
        return false;
    }

    public function getSettings()
    {
        return $this->helper->getSavedSettings();
    }

    public function getAddressFields()
    {
        $customer_form = $this->scopeConfig->getValue('customer/address', "default", 0);
        $settings = $this->getSettings();
        $form_fields = [];
        $form_fields["shipping_address_items"][] = [
            "label" => __("Prefix"),
            "name" => "prefix",
            "sort_order" => isset($settings["supercheckout"]["shipping_address"]["fields"]["prefix"]["sort_order"]) ? $settings["supercheckout"]["shipping_address"]["fields"]["prefix"]["sort_order"] : "1",
            "enabled" => (isset($customer_form["prefix_show"]) && $customer_form["prefix_show"]) ? "1" : "0",
            "display_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["prefix"]["display"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["prefix"]["display"] : (isset($customer_form["prefix_show"]) && $customer_form["prefix_show"]) ? "1" : "0",
            "required_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["prefix"]["require"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["prefix"]["require"] : (isset($customer_form["prefix_show"]) && $customer_form["prefix_show"] == "req") ? "1" : "0",
            "display_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["prefix"]["display"]) ? $settings["supercheckout"]["shipping_address"]["login"]["prefix"]["display"] : (isset($customer_form["prefix_show"]) && $customer_form["prefix_show"]) ? "1" : "0",
            "required_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["prefix"]["require"]) ? $settings["supercheckout"]["shipping_address"]["login"]["prefix"]["require"] : (isset($customer_form["prefix_show"]) && $customer_form["prefix_show"] == "req") ? "1" : "0",
        ];
        $form_fields["shipping_address_items"][] = [
            "label" => __("First Name"),
            "name" => "firstname",
            "sort_order" => isset($settings["supercheckout"]["shipping_address"]["fields"]["firstname"]["sort_order"]) ? $settings["supercheckout"]["shipping_address"]["fields"]["firstname"]["sort_order"] : "2",
            "enabled" => "1",
            "display_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["firstname"]["display"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["firstname"]["display"] : "1",
            "required_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["firstname"]["require"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["firstname"]["require"] : "1",
            "display_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["firstname"]["display"]) ? $settings["supercheckout"]["shipping_address"]["login"]["firstname"]["display"] : "1",
            "required_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["firstname"]["require"]) ? $settings["supercheckout"]["shipping_address"]["login"]["firstname"]["require"] : "1",
        ];
        $form_fields["shipping_address_items"][] = [
            "label" => __("Middle Name/Initial"),
            "name" => "middlename",
            "sort_order" => isset($settings["supercheckout"]["shipping_address"]["fields"]["middlename"]["sort_order"]) ? $settings["supercheckout"]["shipping_address"]["fields"]["middlename"]["sort_order"] : "3",
            "enabled" => (isset($customer_form["middlename_show"]) && $customer_form["middlename_show"]) ? "1" : "0",
            "display_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["middlename"]["display"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["middlename"]["display"] : (isset($customer_form["middlename_show"]) && $customer_form["middlename_show"]) ? "1" : "0",
            "required_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["middlename"]["require"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["middlename"]["require"] : "0",
            "display_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["middlename"]["display"]) ? $settings["supercheckout"]["shipping_address"]["login"]["middlename"]["display"] : (isset($customer_form["middlename_show"]) && $customer_form["middlename_show"]) ? "1" : "0",
            "required_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["middlename"]["require"]) ? $settings["supercheckout"]["shipping_address"]["login"]["middlename"]["require"] : "0",
        ];
        $form_fields["shipping_address_items"][] = [
            "label" => __("Last Name"),
            "name" => "lastname",
            "sort_order" => isset($settings["supercheckout"]["shipping_address"]["fields"]["lastname"]["sort_order"]) ? $settings["supercheckout"]["shipping_address"]["fields"]["lastname"]["sort_order"] : "4",
            "enabled" => "1",
            "display_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["lastname"]["display"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["lastname"]["display"] : "1",
            "required_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["lastname"]["require"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["lastname"]["require"] : "1",
            "display_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["lastname"]["display"]) ? $settings["supercheckout"]["shipping_address"]["login"]["lastname"]["display"] : "1",
            "required_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["lastname"]["require"]) ? $settings["supercheckout"]["shipping_address"]["login"]["lastname"]["require"] : "1",
        ];
        $form_fields["shipping_address_items"][] = [
            "label" => __("Suffix"),
            "name" => "suffix",
            "sort_order" => isset($settings["supercheckout"]["shipping_address"]["fields"]["suffix"]["sort_order"]) ? $settings["supercheckout"]["shipping_address"]["fields"]["suffix"]["sort_order"] : "5",
            "enabled" => (isset($customer_form["suffix_show"]) && $customer_form["suffix_show"]) ? "1" : "0",
            "display_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["suffix"]["display"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["suffix"]["display"] : (isset($customer_form["suffix_show"]) && $customer_form["suffix_show"]) ? "1" : "0",
            "required_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["suffix"]["require"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["suffix"]["require"] : (isset($customer_form["suffix_show"]) && $customer_form["suffix_show"] == "req") ? "1" : "0",
            "display_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["suffix"]["display"]) ? $settings["supercheckout"]["shipping_address"]["login"]["suffix"]["display"] : (isset($customer_form["suffix_show"]) && $customer_form["suffix_show"]) ? "1" : "0",
            "required_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["suffix"]["require"]) ? $settings["supercheckout"]["shipping_address"]["login"]["suffix"]["require"] : (isset($customer_form["suffix_show"]) && $customer_form["suffix_show"] == "req") ? "1" : "0",
        ];
        $form_fields["shipping_address_items"][] = [
            "label" => __("Company"),
            "name" => "company",
            "sort_order" => isset($settings["supercheckout"]["shipping_address"]["fields"]["company"]["sort_order"]) ? $settings["supercheckout"]["shipping_address"]["fields"]["company"]["sort_order"] : "6",
            "enabled" => "1",
            "display_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["company"]["display"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["company"]["display"] : "1",
            "required_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["company"]["require"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["company"]["require"] : "0",
            "display_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["company"]["display"]) ? $settings["supercheckout"]["shipping_address"]["login"]["company"]["display"] : "1",
            "required_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["company"]["require"]) ? $settings["supercheckout"]["shipping_address"]["login"]["company"]["require"] : "0",
        ];
        $form_fields["shipping_address_items"][] = [
            "label" => __("Address 1"),
            "name" => "street1",
            "sort_order" => isset($settings["supercheckout"]["shipping_address"]["fields"]["street1"]["sort_order"]) ? $settings["supercheckout"]["shipping_address"]["fields"]["street1"]["sort_order"] : "7",
            "enabled" => "1",
            "display_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["street1"]["display"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["street1"]["display"] : "1",
            "required_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["street1"]["require"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["street1"]["require"] : "1",
            "display_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["street1"]["display"]) ? $settings["supercheckout"]["shipping_address"]["login"]["street1"]["display"] : "1",
            "required_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["street1"]["require"]) ? $settings["supercheckout"]["shipping_address"]["login"]["street1"]["require"] : "1",
        ];
        $form_fields["shipping_address_items"][] = [
            "label" => __("Address 2"),
            "name" => "street2",
            "sort_order" => isset($settings["supercheckout"]["shipping_address"]["fields"]["street2"]["sort_order"]) ? $settings["supercheckout"]["shipping_address"]["fields"]["street2"]["sort_order"] : "8",
            "enabled" => "1",
            "display_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["street2"]["display"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["street2"]["display"] : "1",
            "required_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["street2"]["require"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["street2"]["require"] : "0",
            "display_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["street2"]["display"]) ? $settings["supercheckout"]["shipping_address"]["login"]["street2"]["display"] : "1",
            "required_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["street2"]["require"]) ? $settings["supercheckout"]["shipping_address"]["login"]["street2"]["require"] : "0",
        ];
        $form_fields["shipping_address_items"][] = [
            "label" => __("City"),
            "name" => "city",
            "sort_order" => isset($settings["supercheckout"]["shipping_address"]["fields"]["city"]["sort_order"]) ? $settings["supercheckout"]["shipping_address"]["fields"]["city"]["sort_order"] : "9",
            "enabled" => "1",
            "display_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["city"]["display"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["city"]["display"] : "1",
            "required_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["city"]["require"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["city"]["require"] : "1",
            "display_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["city"]["display"]) ? $settings["supercheckout"]["shipping_address"]["login"]["city"]["display"] : "1",
            "required_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["city"]["require"]) ? $settings["supercheckout"]["shipping_address"]["login"]["city"]["require"] : "1",
        ];
        $form_fields["shipping_address_items"][] = [
            "label" => __("Country"),
            "name" => "country",
            "sort_order" => isset($settings["supercheckout"]["shipping_address"]["fields"]["country"]["sort_order"]) ? $settings["supercheckout"]["shipping_address"]["fields"]["country"]["sort_order"] : "10",
            "enabled" => "1",
            "display_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["country"]["display"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["country"]["display"] : "1",
            "required_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["country"]["require"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["country"]["require"] : "1",
            "display_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["country"]["display"]) ? $settings["supercheckout"]["shipping_address"]["login"]["country"]["display"] : "1",
            "required_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["country"]["require"]) ? $settings["supercheckout"]["shipping_address"]["login"]["country"]["require"] : "1",
        ];
        $form_fields["shipping_address_items"][] = [
            "label" => __("State/Province"),
            "name" => "region",
            "sort_order" => isset($settings["supercheckout"]["shipping_address"]["fields"]["region"]["sort_order"]) ? $settings["supercheckout"]["shipping_address"]["fields"]["region"]["sort_order"] : "11",
            "enabled" => "1",
            "display_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["region"]["display"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["region"]["display"] : "1",
            "required_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["region"]["require"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["region"]["require"] : "1",
            "display_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["region"]["display"]) ? $settings["supercheckout"]["shipping_address"]["login"]["region"]["display"] : "1",
            "required_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["region"]["require"]) ? $settings["supercheckout"]["shipping_address"]["login"]["region"]["require"] : "1",
        ];
        $form_fields["shipping_address_items"][] = [
            "label" => __("Zip/Postal Code"),
            "name" => "postcode",
            "sort_order" => isset($settings["supercheckout"]["shipping_address"]["fields"]["postcode"]["sort_order"]) ? $settings["supercheckout"]["shipping_address"]["fields"]["postcode"]["sort_order"] : "12",
            "enabled" => "1",
            "display_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["postcode"]["display"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["postcode"]["display"] : "1",
            "required_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["postcode"]["require"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["postcode"]["require"] : "0",
            "display_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["postcode"]["display"]) ? $settings["supercheckout"]["shipping_address"]["login"]["postcode"]["display"] : "1",
            "required_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["postcode"]["require"]) ? $settings["supercheckout"]["shipping_address"]["login"]["postcode"]["require"] : "0",
        ];
        $form_fields["shipping_address_items"][] = [
            "label" => __("Telephone"),
            "name" => "telephone",
            "sort_order" => isset($settings["supercheckout"]["shipping_address"]["fields"]["telephone"]["sort_order"]) ? $settings["supercheckout"]["shipping_address"]["fields"]["telephone"]["sort_order"] : "13",
            "enabled" => "1",
            "display_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["telephone"]["display"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["telephone"]["display"] : "1",
            "required_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["telephone"]["require"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["telephone"]["require"] : "1",
            "display_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["telephone"]["display"]) ? $settings["supercheckout"]["shipping_address"]["login"]["telephone"]["display"] : "1",
            "required_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["telephone"]["require"]) ? $settings["supercheckout"]["shipping_address"]["login"]["telephone"]["require"] : "1",
        ];
        $form_fields["shipping_address_items"][] = [
            "label" => __("Fax"),
            "name" => "fax",
            "sort_order" => isset($settings["supercheckout"]["shipping_address"]["fields"]["fax"]["sort_order"]) ? $settings["supercheckout"]["shipping_address"]["fields"]["fax"]["sort_order"] : "14",
            "enabled" => "1",
            "display_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["fax"]["display"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["fax"]["display"] : "1",
            "required_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["fax"]["require"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["fax"]["require"] : "0",
            "display_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["fax"]["display"]) ? $settings["supercheckout"]["shipping_address"]["login"]["fax"]["display"] : "1",
            "required_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["fax"]["require"]) ? $settings["supercheckout"]["shipping_address"]["login"]["fax"]["require"] : "0",
        ];
        return $form_fields;
    }
}
