<?php

namespace Knowband\Supercheckout\Block\Adminhtml\Tab;

class PaymentMethod extends \Magento\Backend\Block\Widget implements \Magento\Backend\Block\Widget\Tab\TabInterface
{

    private $helper;
    private $scopeConfig;
    private $paymentModelConfig;

    public function __construct(\Magento\Backend\Block\Template\Context $context, \Knowband\Supercheckout\Helper\Data $helper, \Magento\Payment\Model\Config $paymentModelConfig, array $data = [])
    {
        parent::__construct($context, $data);
        $this->helper = $helper;
        $this->scopeConfig = $context->getScopeConfig();
        $this->paymentModelConfig = $paymentModelConfig;
    }

    public function getTabLabel()
    {
        return __('Payment Method');
    }

    public function getTabTitle()
    {
        return __('Payment Method');
    }

    public function canShowTab()
    {
        return true;
    }

    public function isHidden()
    {
        return false;
    }

    public function getSettings()
    {
        return $this->helper->getSavedSettings();
    }

    public function getActiveMethods()
    {
        $payments = $this->paymentModelConfig->getActiveMethods();
        $methods = [];
        foreach ($payments as $paymentCode => $paymentModel) {
            $paymentTitle = $this->scopeConfig
                    ->getValue('payment/' . $paymentCode . '/title');
            $methods[$paymentCode] = $paymentTitle;
        }
        return $methods;
    }
}
