<?php

namespace Knowband\Supercheckout\Block\Adminhtml\Tab;

class General extends \Magento\Backend\Block\Widget implements \Magento\Backend\Block\Widget\Tab\TabInterface
{

    private $helper;

    public function __construct(\Magento\Backend\Block\Template\Context $context, \Knowband\Supercheckout\Helper\Data $helper, array $data = [])
    {
        $this->helper = $helper;
        parent::__construct($context, $data);
    }

    public function getTabLabel()
    {
        return __('General Settings');
    }

    public function getTabTitle()
    {
        return __('General Settings');
    }

    public function canShowTab()
    {
        return true;
    }

    public function isHidden()
    {
        return false;
    }

    public function getSettings()
    {
        return $this->helper->getSavedSettings();
    }
}
