<?php

namespace Knowband\Supercheckout\Block\Adminhtml\Tab;

class Design extends \Magento\Backend\Block\Widget implements \Magento\Backend\Block\Widget\Tab\TabInterface
{

    private $helper;
    private $scopeConfig;

    public function __construct(\Magento\Backend\Block\Template\Context $context, \Knowband\Supercheckout\Helper\Data $helper, array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->helper = $helper;
        $this->scopeConfig = $context->getScopeConfig();
    }

    public function getTabLabel()
    {
        return __('Design');
    }

    public function getTabTitle()
    {
        return __('Design');
    }

    public function canShowTab()
    {
        return true;
    }

    public function isHidden()
    {
        return false;
    }

    public function getSettings()
    {
        return $this->helper->getSavedSettings();
    }

    public function getPortlets()
    {
        $portlets = [];
        $settings = $this->getSettings();
        $portlets["portlets_items"][] = [
            "name" => "login",
            "title" => __("Login"),
            "info" => __("Customer's login details & registration center."),
            "col" => isset($settings["supercheckout"]["design"]["threecolumn"]["portlet"]["login"]["column"]) ? $settings["supercheckout"]["design"]["threecolumn"]["portlet"]["login"]["column"] : "1",
            "row" => isset($settings["supercheckout"]["design"]["threecolumn"]["portlet"]["login"]["row"]) ? $settings["supercheckout"]["design"]["threecolumn"]["portlet"]["login"]["row"] : "0",
            "col-2" => isset($settings["supercheckout"]["design"]["twocolumn"]["portlet"]["login"]["col-2"]) ? $settings["supercheckout"]["design"]["twocolumn"]["portlet"]["login"]["col-2"] : "1",
            "row-2" => isset($settings["supercheckout"]["design"]["twocolumn"]["portlet"]["login"]["row-2"]) ? $settings["supercheckout"]["design"]["twocolumn"]["portlet"]["login"]["row-2"] : "0",
            "inside-2" => isset($settings["supercheckout"]["design"]["twocolumn"]["portlet"]["login"]["inside-2"]) ? $settings["supercheckout"]["design"]["twocolumn"]["portlet"]["login"]["inside-2"] : "1",
            "action" => false
        ];
        $portlets["portlets_items"][] = [
            "name" => "payment_address",
            "title" => __("Payment Address"),
            "info" => __("Customer information and payment address."),
            "col" => isset($settings["supercheckout"]["design"]["threecolumn"]["portlet"]["payment_address"]["column"]) ? $settings["supercheckout"]["design"]["threecolumn"]["portlet"]["payment_address"]["column"] : "1",
            "row" => isset($settings["supercheckout"]["design"]["threecolumn"]["portlet"]["payment_address"]["row"]) ? $settings["supercheckout"]["design"]["threecolumn"]["portlet"]["payment_address"]["row"] : "1",
            "col-2" => isset($settings["supercheckout"]["design"]["twocolumn"]["portlet"]["payment_address"]["col-2"]) ? $settings["supercheckout"]["design"]["twocolumn"]["portlet"]["payment_address"]["col-2"] : "1",
            "row-2" => isset($settings["supercheckout"]["design"]["twocolumn"]["portlet"]["payment_address"]["row-2"]) ? $settings["supercheckout"]["design"]["twocolumn"]["portlet"]["payment_address"]["row-2"] : "1",
            "inside-2" => isset($settings["supercheckout"]["design"]["twocolumn"]["portlet"]["payment_address"]["inside-2"]) ? $settings["supercheckout"]["design"]["twocolumn"]["portlet"]["payment_address"]["inside-2"] : "1",
            "action" => false
        ];
        $portlets["portlets_items"][] = [
            "name" => "shipping_address",
            "title" => __("Shipping Address"),
            "info" => __("Extra address for shipping purposes."),
            "col" => isset($settings["supercheckout"]["design"]["threecolumn"]["portlet"]["shipping_address"]["column"]) ? $settings["supercheckout"]["design"]["threecolumn"]["portlet"]["shipping_address"]["column"] : "1",
            "row" => isset($settings["supercheckout"]["design"]["threecolumn"]["portlet"]["shipping_address"]["row"]) ? $settings["supercheckout"]["design"]["threecolumn"]["portlet"]["shipping_address"]["row"] : "2",
            "col-2" => isset($settings["supercheckout"]["design"]["twocolumn"]["portlet"]["shipping_address"]["col-2"]) ? $settings["supercheckout"]["design"]["twocolumn"]["portlet"]["shipping_address"]["col-2"] : "1",
            "row-2" => isset($settings["supercheckout"]["design"]["twocolumn"]["portlet"]["shipping_address"]["row-2"]) ? $settings["supercheckout"]["design"]["twocolumn"]["portlet"]["shipping_address"]["row-2"] : "2",
            "inside-2" => isset($settings["supercheckout"]["design"]["twocolumn"]["portlet"]["shipping_address"]["inside-2"]) ? $settings["supercheckout"]["design"]["twocolumn"]["portlet"]["shipping_address"]["inside-2"] : "1",
            "action" => false
        ];
        $portlets["portlets_items"][] = [
            "name" => "shipping_method",
            "title" => __("Shipping Method"),
            "info" => __("Third step. You can set a default method and hide this step."),
            "col" => isset($settings["supercheckout"]["design"]["threecolumn"]["portlet"]["shipping_method"]["column"]) ? $settings["supercheckout"]["design"]["threecolumn"]["portlet"]["shipping_method"]["column"] : "2",
            "row" => isset($settings["supercheckout"]["design"]["threecolumn"]["portlet"]["shipping_method"]["row"]) ? $settings["supercheckout"]["design"]["threecolumn"]["portlet"]["shipping_method"]["row"] : "0",
            "col-2" => isset($settings["supercheckout"]["design"]["twocolumn"]["portlet"]["shipping_method"]["col-2"]) ? $settings["supercheckout"]["design"]["twocolumn"]["portlet"]["shipping_method"]["col-2"] : "2",
            "row-2" => isset($settings["supercheckout"]["design"]["twocolumn"]["portlet"]["shipping_method"]["row-2"]) ? $settings["supercheckout"]["design"]["twocolumn"]["portlet"]["shipping_method"]["row-2"] : "0",
            "inside-2" => isset($settings["supercheckout"]["design"]["twocolumn"]["portlet"]["shipping_method"]["inside-2"]) ? $settings["supercheckout"]["design"]["twocolumn"]["portlet"]["shipping_method"]["inside-2"] : "3",
            "action" => false
        ];
        $portlets["portlets_items"][] = [
            "name" => "payment_method",
            "title" => __("Payment Method"),
            "info" => __("Fourth step. You can set a default method and hide this step."),
            "col" => isset($settings["supercheckout"]["design"]["threecolumn"]["portlet"]["payment_method"]["column"]) ? $settings["supercheckout"]["design"]["threecolumn"]["portlet"]["payment_method"]["column"] : "2",
            "row" => isset($settings["supercheckout"]["design"]["threecolumn"]["portlet"]["payment_method"]["row"]) ? $settings["supercheckout"]["design"]["threecolumn"]["portlet"]["payment_method"]["row"] : "1",
            "col-2" => isset($settings["supercheckout"]["design"]["twocolumn"]["portlet"]["payment_method"]["col-2"]) ? $settings["supercheckout"]["design"]["twocolumn"]["portlet"]["payment_method"]["col-2"] : "2",
            "row-2" => isset($settings["supercheckout"]["design"]["twocolumn"]["portlet"]["payment_method"]["row-2"]) ? $settings["supercheckout"]["design"]["twocolumn"]["portlet"]["payment_method"]["row-2"] : "0",
            "inside-2" => isset($settings["supercheckout"]["design"]["twocolumn"]["portlet"]["payment_method"]["inside-2"]) ? $settings["supercheckout"]["design"]["twocolumn"]["portlet"]["payment_method"]["inside-2"] : "4",
            "action" => false
        ];
        $portlets["portlets_items"][] = [
            "name" => "cart",
            "title" => __("Cart"),
            "info" => __("The step shows the products in cart. You can hide the complete module or any particular row."),
            "col" => isset($settings["supercheckout"]["design"]["threecolumn"]["portlet"]["cart"]["column"]) ? $settings["supercheckout"]["design"]["threecolumn"]["portlet"]["cart"]["column"] : "3",
            "row" => isset($settings["supercheckout"]["design"]["threecolumn"]["portlet"]["cart"]["row"]) ? $settings["supercheckout"]["design"]["threecolumn"]["portlet"]["cart"]["row"] : "0",
            "col-2" => isset($settings["supercheckout"]["design"]["twocolumn"]["portlet"]["cart"]["col-2"]) ? $settings["supercheckout"]["design"]["twocolumn"]["portlet"]["cart"]["col-2"] : "2",
            "row-2" => isset($settings["supercheckout"]["design"]["twocolumn"]["portlet"]["cart"]["row-2"]) ? $settings["supercheckout"]["design"]["twocolumn"]["portlet"]["cart"]["row-2"] : "0",
            "inside-2" => isset($settings["supercheckout"]["design"]["twocolumn"]["portlet"]["cart"]["inside-2"]) ? $settings["supercheckout"]["design"]["twocolumn"]["portlet"]["cart"]["inside-2"] : "2",
            "action" => false
        ];
        $portlets["portlets_items"][] = [
            "name" => "confirm",
            "title" => __("Confirm"),
            "info" => __("The last step is the confirm. Edit fields."),
            "col" => isset($settings["supercheckout"]["design"]["threecolumn"]["portlet"]["confirm"]["column"]) ? $settings["supercheckout"]["design"]["threecolumn"]["portlet"]["confirm"]["column"] : "3",
            "row" => isset($settings["supercheckout"]["design"]["threecolumn"]["portlet"]["confirm"]["row"]) ? $settings["supercheckout"]["design"]["threecolumn"]["portlet"]["confirm"]["row"] : "1",
            "col-2" => isset($settings["supercheckout"]["design"]["twocolumn"]["portlet"]["confirm"]["col-2"]) ? $settings["supercheckout"]["design"]["twocolumn"]["portlet"]["confirm"]["col-2"] : "2",
            "row-2" => isset($settings["supercheckout"]["design"]["twocolumn"]["portlet"]["confirm"]["row-2"]) ? $settings["supercheckout"]["design"]["twocolumn"]["portlet"]["confirm"]["row-2"] : "0",
            "inside-2" => isset($settings["supercheckout"]["design"]["twocolumn"]["portlet"]["confirm"]["inside-2"]) ? $settings["supercheckout"]["design"]["twocolumn"]["portlet"]["confirm"]["inside-2"] : "5",
            "action" => false
        ];
        $portlets["portlets_items"][] = [
            "name" => "html",
            "title" => __("HTML Content"),
            "info" => __("You can add html or message on checkout page"),
            "col" => isset($settings["supercheckout"]["design"]["threecolumn"]["portlet"]["html"]["column"]) ? $settings["supercheckout"]["design"]["threecolumn"]["portlet"]["html"]["column"] : "3",
            "row" => isset($settings["supercheckout"]["design"]["threecolumn"]["portlet"]["html"]["row"]) ? $settings["supercheckout"]["design"]["threecolumn"]["portlet"]["html"]["row"] : "2",
            "col-2" => isset($settings["supercheckout"]["design"]["twocolumn"]["portlet"]["html"]["col-2"]) ? $settings["supercheckout"]["design"]["twocolumn"]["portlet"]["html"]["col-2"] : "2",
            "row-2" => isset($settings["supercheckout"]["design"]["twocolumn"]["portlet"]["html"]["row-2"]) ? $settings["supercheckout"]["design"]["twocolumn"]["portlet"]["html"]["row-2"] : "0",
            "inside-2" => isset($settings["supercheckout"]["design"]["twocolumn"]["portlet"]["html"]["inside-2"]) ? $settings["supercheckout"]["design"]["twocolumn"]["portlet"]["html"]["inside-2"] : "5",
            "action" => true
        ];
        return $portlets;
    }
}
