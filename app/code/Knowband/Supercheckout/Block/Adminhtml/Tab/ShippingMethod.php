<?php

namespace Knowband\Supercheckout\Block\Adminhtml\Tab;

use Magento\Framework\Exception\InputException;

class ShippingMethod extends \Magento\Backend\Block\Widget implements \Magento\Backend\Block\Widget\Tab\TabInterface
{

    private $helper;
    private $scopeConfig;
    private $carrierFactory;

    public function __construct(\Magento\Backend\Block\Template\Context $context, \Knowband\Supercheckout\Helper\Data $helper, \Magento\Shipping\Model\CarrierFactory $carrierFactory, array $data = [])
    {
        parent::__construct($context, $data);
        $this->helper = $helper;
        $this->scopeConfig = $context->getScopeConfig();
        $this->carrierFactory = $carrierFactory;
    }

    public function getTabLabel()
    {
        return __('Shipping Method');
    }

    public function getTabTitle()
    {
        return __('Shipping Method');
    }

    public function canShowTab()
    {
        return true;
    }

    public function isHidden()
    {
        return false;
    }

    public function getSettings()
    {
        return $this->helper->getSavedSettings();
    }

    public function getAllActiveCarriers($store = null)
    {
        $carriers = [];
        $shipping_methods = [];
        try {
            $config = $this->scopeConfig->getValue('carriers', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
            foreach (array_keys($config) as $carrierCode) {
                if ($this->scopeConfig->isSetFlag('carriers/' . $carrierCode . '/active', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store)) {
                    $carrierModel = $this->carrierFactory->create($carrierCode, $store);
                    if ($carrierModel) {
                        $carriers[$carrierCode] = $carrierModel;
                    }
                }
            }
            foreach ($carriers as $_ccode => $_carrier) {
                if ($_methods = $_carrier->getAllowedMethods()) {
                    foreach ($_methods as $_mcode => $_method) {
                        $_code = $_ccode . '_' . $_mcode;
                    }
                    $_title = $this->scopeConfig->getValue("carriers/$_ccode/title");
                    if (!$_title) {
                        $_title = $_ccode;
                    }
                    $shipping_methods[$_code] = $_title;
                }
            }
        } catch (\Magento\Framework\Exception\InputException $ex) {
        }
        return $shipping_methods;
    }
}
