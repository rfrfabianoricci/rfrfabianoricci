<?php

namespace Knowband\Supercheckout\Block\Adminhtml\Tab;

class CartConfirm extends \Magento\Backend\Block\Widget implements \Magento\Backend\Block\Widget\Tab\TabInterface
{

    private $helper;
    private $scopeConfig;

    public function __construct(\Magento\Backend\Block\Template\Context $context, \Knowband\Supercheckout\Helper\Data $helper, array $data = [])
    {
        parent::__construct($context, $data);
        $this->helper = $helper;
        $this->scopeConfig = $context->getScopeConfig();
    }

    public function getTabLabel()
    {
        return __('Cart & Confirm');
    }

    public function getTabTitle()
    {
        return __('Cart & Confirm');
    }

    public function canShowTab()
    {
        return true;
    }

    public function isHidden()
    {
        return false;
    }

    public function getSettings()
    {
        return $this->helper->getSavedSettings();
    }

    public function getCartFields()
    {
        $settings = $this->getSettings();
        $form_fields = [];
        $form_fields["cart_items"][] = [
            "label" => __("Display Cart"),
            "name" => "cart",
            "display_guest" => isset($settings["supercheckout"]["cart"]["guest"]["cart"]["display"]) ? $settings["supercheckout"]["cart"]["guest"]["cart"]["display"] : "1",
            "display_login" => isset($settings["supercheckout"]["cart"]["login"]["cart"]["display"]) ? $settings["supercheckout"]["cart"]["login"]["cart"]["display"] : "1",
            "hint" => __("By hiding the cart, you are hiding the cart and the options in it. The confirm button still stays to complete the order.")
        ];
        $form_fields["cart_items"][] = [
            "label" => __("Display Popup Image"),
            "name" => "image",
            "display_guest" => isset($settings["supercheckout"]["cart"]["guest"]["image"]["display"]) ? $settings["supercheckout"]["cart"]["guest"]["image"]["display"] : "1",
            "display_login" => isset($settings["supercheckout"]["cart"]["login"]["image"]["display"]) ? $settings["supercheckout"]["cart"]["login"]["image"]["display"] : "1",
            "hint" => __("")
        ];
        $form_fields["cart_items"][] = [
            "label" => __("Display Product Name"),
            "name" => "name",
            "display_guest" => isset($settings["supercheckout"]["cart"]["guest"]["name"]["display"]) ? $settings["supercheckout"]["cart"]["guest"]["name"]["display"] : "1",
            "display_login" => isset($settings["supercheckout"]["cart"]["login"]["name"]["display"]) ? $settings["supercheckout"]["cart"]["login"]["name"]["display"] : "1",
            "hint" => __("")
        ];
        $form_fields["cart_items"][] = [
            "label" => __("Display Product Quantity"),
            "name" => "qty",
            "display_guest" => isset($settings["supercheckout"]["cart"]["guest"]["qty"]["display"]) ? $settings["supercheckout"]["cart"]["guest"]["qty"]["display"] : "1",
            "display_login" => isset($settings["supercheckout"]["cart"]["login"]["qty"]["display"]) ? $settings["supercheckout"]["cart"]["login"]["qty"]["display"] : "1",
            "hint" => __("")
        ];
        $form_fields["cart_items"][] = [
            "label" => __("Display Product Price"),
            "name" => "price",
            "display_guest" => isset($settings["supercheckout"]["cart"]["guest"]["price"]["display"]) ? $settings["supercheckout"]["cart"]["guest"]["price"]["display"] : "1",
            "display_login" => isset($settings["supercheckout"]["cart"]["login"]["price"]["display"]) ? $settings["supercheckout"]["cart"]["login"]["price"]["display"] : "1",
            "hint" => __("")
        ];
        $form_fields["cart_items"][] = [
            "label" => __("Display Totals"),
            "name" => "totals",
            "display_guest" => isset($settings["supercheckout"]["cart"]["guest"]["totals"]["display"]) ? $settings["supercheckout"]["cart"]["guest"]["totals"]["display"] : "1",
            "display_login" => isset($settings["supercheckout"]["cart"]["login"]["totals"]["display"]) ? $settings["supercheckout"]["cart"]["login"]["totals"]["display"] : "1",
            "hint" => __("")
        ];
        $form_fields["cart_items"][] = [
            "label" => __("Display Voucher Input"),
            "name" => "voucher",
            "display_guest" => isset($settings["supercheckout"]["cart"]["guest"]["voucher"]["display"]) ? $settings["supercheckout"]["cart"]["guest"]["voucher"]["display"] : "1",
            "display_login" => isset($settings["supercheckout"]["cart"]["login"]["voucher"]["display"]) ? $settings["supercheckout"]["cart"]["login"]["voucher"]["display"] : "1",
            "hint" => __("")
        ];
        return $form_fields;
    }

    public function getConfirmFields()
    {
        $settings = $this->getSettings();
        $form_fields = [];
        $form_fields["confirm_items"][] = [
            "label" => __("I agree to the conditions"),
            "name" => "agree",
            "display_guest" => isset($settings["supercheckout"]["confirm"]["guest"]["agree"]["display"]) ? $settings["supercheckout"]["confirm"]["guest"]["agree"]["display"] : "1",
            "display_login" => isset($settings["supercheckout"]["confirm"]["login"]["agree"]["display"]) ? $settings["supercheckout"]["confirm"]["login"]["agree"]["display"] : "1",
            "hint" => __("")
        ];
        $form_fields["confirm_items"][] = [
            "label" => __("Leave a comment for the order."),
            "name" => "comment",
            "display_guest" => isset($settings["supercheckout"]["confirm"]["guest"]["comment"]["display"]) ? $settings["supercheckout"]["confirm"]["guest"]["comment"]["display"] : "1",
            "display_login" => isset($settings["supercheckout"]["confirm"]["login"]["comment"]["display"]) ? $settings["supercheckout"]["confirm"]["login"]["comment"]["display"] : "1",
            "hint" => __("")
        ];
        return $form_fields;
    }

    public function getPopupDimensions()
    {
        $dimensions = [];
        $settings = $this->getSettings();
        if (isset($settings["supercheckout"]["cart"]["popup_image_width"]) && isset($settings["supercheckout"]["cart"]["popup_image_height"]) && $settings["supercheckout"]["cart"]["popup_image_width"] && $settings["supercheckout"]["cart"]["popup_image_width"]) {
            $dimensions = [
                "width" => $settings["supercheckout"]["cart"]["popup_image_width"],
                "height" => $settings["supercheckout"]["cart"]["popup_image_height"]
            ];
        } else {
            $dimensions = [
                "width" => "230",
                "height" => "230"
            ];
        }
        return $dimensions;
    }
}
