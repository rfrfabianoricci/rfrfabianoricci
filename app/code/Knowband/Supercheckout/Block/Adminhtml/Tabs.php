<?php

namespace Knowband\Supercheckout\Block\Adminhtml;

class Tabs extends \Magento\Backend\Block\Widget\Tabs
{

    private $coreRegistry = null;
    private $storeManager;
    private $helper;

    public function __construct(\Magento\Backend\Block\Template\Context $context, \Magento\Framework\Json\EncoderInterface $jsonEncoder, \Magento\Backend\Model\Auth\Session $authSession, \Knowband\Supercheckout\Helper\Data $helper, \Magento\Framework\Registry $registry, array $data = [])
    {
        parent::__construct($context, $jsonEncoder, $authSession, $data);
        $this->coreRegistry = $registry;
        $this->storeManager = $context->getStoreManager();
        $this->helper = $helper;
    }

    protected function _construct()
    {
        parent::_construct();
        $this->setId('supercheckout_view_tabs');
        $this->setDestElementId('supercheckout_view');
    }
}
