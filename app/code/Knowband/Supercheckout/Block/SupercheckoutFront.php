<?php

namespace Knowband\Supercheckout\Block;

use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Payment\Model\Method\AbstractMethod;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Api\AddressRepositoryInterface;
use Magento\Customer\Model\Address\Config as AddressConfig;
use Magento\Framework\Exception\NoSuchEntityException;

class SupercheckoutFront extends \Magento\Framework\View\Element\Template
{

    public $sc_storeManager;
    public $sc_urlInterface;
    public $sc_request;
    public $sc_scopeConfig;
    public $sc_helper;
    public $sc_settings = [];
    public $sc_carrierFactory;
    public $quoteRepository;
    public $quote;
    public $totalsCollector;
    public $converter;
    public $sc_taxHelper;
    public $sc_imageHelper;
    public $priceCurrency;
    public $methodSpecificationFactory;
    public $sc_paymentHelper;
    public $sc_customerSession;
    public $currentCustomer;
    public $customerRepository;
    public $sc_addressConfig;
    public $addressMapper;
    public $addressRepository;
    public $sc_countryCollectionFactory;
    public $sc_countryFactory;

    public function __construct(\Magento\Backend\Block\Template\Context $context, \Magento\Framework\App\Request\Http $request, \Knowband\Supercheckout\Helper\Data $helper, \Magento\Catalog\Helper\Image $imageHelper, \Magento\Shipping\Model\CarrierFactory $carrierFactory, \Magento\Quote\Api\CartRepositoryInterface $quoteRepository, \Magento\Checkout\Model\Session $checkoutSession, \Magento\Customer\Model\Session $customerSession, \Magento\Quote\Model\Cart\ShippingMethodConverter $converter, \Magento\Quote\Model\Quote\TotalsCollector $totalsCollector, \Magento\Tax\Helper\Data $taxHelper, PriceCurrencyInterface $priceCurrency, \Magento\Payment\Helper\Data $paymentHelper, \Magento\Payment\Model\Checks\SpecificationFactory $methodSpecificationFactory, CustomerRepositoryInterface $customerRepository, AddressConfig $addressConfig, AddressRepositoryInterface $addressRepository, \Magento\Customer\Model\Address\Mapper $addressMapper, \Magento\Directory\Model\ResourceModel\Country\CollectionFactory $countryCollectionFactory, \Magento\Directory\Model\CountryFactory $countryFactory, array $data = [])
    {
        parent::__construct($context, $data);
        $this->sc_storeManager = $context->getStoreManager();
        $this->sc_urlInterface = $context->getUrlBuilder();
        $this->sc_request = $request;
        $this->sc_scopeConfig = $context->getScopeConfig();
        $this->sc_helper = $helper;
        $this->sc_settings = $this->sc_helper->getSettings();
        $this->sc_carrierFactory = $carrierFactory;
        $this->quoteRepository = $quoteRepository;
        $this->quote = $checkoutSession;
        $this->totalsCollector = $totalsCollector;
        $this->converter = $converter;
        $this->priceCurrency = $priceCurrency;
        $this->sc_taxHelper = $taxHelper;
        $this->sc_paymentHelper = $paymentHelper;
        $this->sc_customerSession = $customerSession;
        $this->methodSpecificationFactory = $methodSpecificationFactory;
        $this->sc_imageHelper = $imageHelper;
        $this->customerRepository = $customerRepository;
        $this->sc_addressConfig = $addressConfig;
        $this->addressMapper = $addressMapper;
        $this->sc_countryCollectionFactory = $countryCollectionFactory;
        $this->sc_countryFactory = $countryFactory;
        $this->addressRepository = $addressRepository;
    }

    public function getSortOrder($type = "")
    {
        if ($type) {
            if (!empty($this->sc_settings)) {
                if ($this->sc_settings["supercheckout"]["layout-type"] == "two") {
                    return [
                        'col' => $this->sc_settings["supercheckout"]["design"]["twocolumn"]["portlet"][$type]["col-2"],
                        'row' => $this->sc_settings["supercheckout"]["design"]["twocolumn"]["portlet"][$type]["row-2"],
                        'inside' => $this->sc_settings["supercheckout"]["design"]["twocolumn"]["portlet"][$type]["inside-2"]
                    ];
                } else {
                    return [
                        'col' => $this->sc_settings["supercheckout"]["design"]["threecolumn"]["portlet"][$type]["column"],
                        'row' => $this->sc_settings["supercheckout"]["design"]["threecolumn"]["portlet"][$type]["row"],
                        'inside' => 0,
                    ];
                }
            } else {
                $this->sc_settings = $this->sc_helper->getSettings();
                $this->getSortOrder();
            }
        } else {
            return [
                'col' => 0,
                'row' => 0,
                'inside' => 0,
            ];
        }
    }

    public function getColumnWidth($layout = "threecolumn")
    {
        if (!empty($this->sc_settings)) {
            if ($layout == "twocolumn") {
                return [
                    'one' => $this->sc_settings["supercheckout"]["design"][$layout]["column_width"][1],
                    'two' => $this->sc_settings["supercheckout"]["design"][$layout]["column_width"][2],
                    'inside_one' => $this->sc_settings["supercheckout"]["design"][$layout]["inside"]["column_width"][1],
                    'inside_two' => $this->sc_settings["supercheckout"]["design"][$layout]["inside"]["column_width"][2]
                ];
            } else {
                return [
                    'one' => $this->sc_settings["supercheckout"]["design"][$layout]["column_width"][1],
                    'two' => $this->sc_settings["supercheckout"]["design"][$layout]["column_width"][2],
                    'three' => $this->sc_settings["supercheckout"]["design"][$layout]["column_width"][3]
                ];
            }
        } else {
            $this->sc_settings = $this->sc_helper->getSettings();
            $this->getLoginSortOrder();
        }
    }

    public function showLoginRadios()
    {
        if (!empty($this->sc_settings)) {
            if ((isset($this->sc_settings["supercheckout"]["register_guest"]) && $this->sc_settings["supercheckout"]["register_guest"]) || (isset($this->sc_settings["supercheckout"]["guest_checkout"]) && $this->sc_settings["supercheckout"]["guest_checkout"])) {
                return true;
            } else {
                return false;
            }
        } else {
            $this->sc_settings = $this->sc_helper->getSettings();
            $this->showLoginRadios();
        }
    }

    public function showDob()
    {
        if ($this->sc_customerSession->isLoggedIn()) {
            if (null != $this->sc_customerSession->getCustomer()->getDob() && $this->sc_customerSession->getCustomer()->getDob() != '1900-01-01') {
                return false;
            }
        }
        if ($this->sc_scopeConfig->getValue('customer/address/dob_show', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) === 'opt' || $this->sc_scopeConfig->getValue('customer/address/dob_show', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) === 'req') {
            return true;
        }
        return false;
    }

    public function showGender()
    {
        if ($this->sc_customerSession->isLoggedIn()) {
            if (null != $this->sc_customerSession->getCustomer()->getGender()) {
                return false;
            }
        }
        if ($this->sc_scopeConfig->getValue('customer/address/gender_show', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) === 'opt' || $this->sc_scopeConfig->getValue('customer/address/gender_show', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) === 'req') {
            return true;
        }
        return false;
    }

    public function showTaxvat()
    {
        if ($this->sc_customerSession->isLoggedIn()) {
            if (null != $this->sc_customerSession->getCustomer()->getTaxvat() && $this->sc_customerSession->getCustomer()->getTaxvat() != 'xxxx-xxxx') {
                return false;
            }
        }
        if ($this->sc_scopeConfig->getValue('customer/address/taxvat_show', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) === 'opt' || $this->sc_scopeConfig->getValue('customer/address/taxvat_show', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) === 'req') {
            return true;
        }
        return false;
    }

    public function isGenderRequired()
    {
        if ($this->sc_scopeConfig->getValue('customer/address/gender_show', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) === 'req') {
            return true;
        }
        return false;
    }

    public function isTaxvatRequired()
    {
        if ($this->sc_scopeConfig->getValue('customer/address/taxvat_show', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) === 'req') {
            return true;
        }
        return false;
    }

    public function isDobRequired()
    {
        if ($this->sc_scopeConfig->getValue('customer/address/dob_show', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) === 'req') {
            return true;
        }
        return false;
    }

    public function getCartRemoveUrl()
    {
        return $this->sc_urlInterface->getUrl('supercheckout/cart/remove');
    }

    public function getCartUpdateUrl()
    {
        return $this->sc_urlInterface->getUrl('supercheckout/cart/update');
    }

    public function getRefreshUrl()
    {
        return $this->sc_urlInterface->getUrl('supercheckout/refresh/');
    }

    public function getSaveShippingMethodUrl()
    {
        return $this->sc_urlInterface->getUrl('supercheckout/shippingmethod/saveshippingmethod');
    }

    public function getSavePaymentMethodUrl()
    {
        return $this->sc_urlInterface->getUrl('supercheckout/paymentmethod/savepaymentmethod');
    }

    public function getSavePaymentAddressUrl()
    {
        return $this->sc_urlInterface->getUrl('supercheckout/address/savepayment');
    }

    public function getSaveShippingAddressUrl()
    {
        return $this->sc_urlInterface->getUrl('supercheckout/address/saveshipping');
    }

    public function getSocialRegisterUrl()
    {
        return $this->sc_urlInterface->getUrl('supercheckout/login/socialregister');
    }

    public function getSocialLoginCheckUrl()
    {
        return $this->sc_urlInterface->getUrl('supercheckout/login/sociallogincheck');
    }

    public function getSaveOrderUrl()
    {
        return $this->sc_urlInterface->getUrl('supercheckout/order/save');
    }

    public function getPaymentRegionsUrl()
    {
        return $this->sc_urlInterface->getUrl('supercheckout/address/paymentregions');
    }

    public function getShippingRegionsUrl()
    {
        return $this->sc_urlInterface->getUrl('supercheckout/address/shippingregions');
    }

    public function getVoucherUrl()
    {
        return $this->sc_urlInterface->getUrl('supercheckout/cart/voucher');
    }

    public function getPortletHtml()
    {
        if (!empty($this->sc_settings)) {
            return $this->sc_settings["supercheckout"]["design"]["portlet"]["html_portlet"];
        } else {
            $this->sc_settings = $this->sc_helper->getSettings();
            $this->getPortletHtml();
        }
    }

    public function getHeaderHtml()
    {
        if (!empty($this->sc_settings)) {
            return $this->sc_settings["supercheckout"]["design"]["portlet"]["html_header"];
        } else {
            $this->sc_settings = $this->sc_helper->getSettings();
            $this->getPortletHtml();
        }
    }

    public function getFooterHtml()
    {
        if (!empty($this->sc_settings)) {
            return $this->sc_settings["supercheckout"]["design"]["portlet"]["html_footer"];
        } else {
            $this->sc_settings = $this->sc_helper->getSettings();
            $this->getPortletHtml();
        }
    }

    public function isLoggedIn()
    {
        if ($this->sc_customerSession->isLoggedIn()) {
            return true;
        }
        return false;
    }

    public function getBillingAddressFields()
    {

        $customer_form = $this->sc_scopeConfig->getValue('customer/address', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, null);
        $settings = $this->sc_helper->getSettings();
        $form_fields = [];
        $prefix_group_items = [];
        if (isset($customer_form["prefix_options"]) && $customer_form["prefix_options"] != "") {
            $options_dropdown = array_combine(explode(";", $customer_form["prefix_options"]), explode(";", $customer_form["prefix_options"]));
            foreach ($options_dropdown as $id => $label) {
                if (null != $this->quote->getQuote()->getBillingAddress()->getPrefix()) {
                    if ($id == $this->quote->getQuote()->getBillingAddress()->getPrefix()) {
                        $prefix_group_items[] = ["id" => $id, "label" => $label, 'selected' => true];
                    } else {
                        $prefix_group_items[] = ["id" => $id, "label" => $label, 'selected' => false];
                    }
                } else {
                    $prefix_group_items[] = ["id" => $id, "label" => $label, 'selected' => false];
                }
            }
        }
        $form_fields[] = [
            "label" => __("Prefix"),
            "name" => "prefix",
            "type" => "select",
            "value" => (null != $this->quote->getQuote()->getBillingAddress()->getPrefix()) ? $this->quote->getQuote()->getBillingAddress()->getPrefix() : "",
            "options" => [],
            "sort_order" => isset($settings["supercheckout"]["payment_address"]["fields"]["prefix"]["sort_order"]) ? $settings["supercheckout"]["payment_address"]["fields"]["prefix"]["sort_order"] : "1",
            "enabled" => (isset($customer_form["prefix_show"]) && $customer_form["prefix_show"]) ? "1" : "0",
            "display_guest" => isset($settings["supercheckout"]["payment_address"]["guest"]["prefix"]["display"]) ? $settings["supercheckout"]["payment_address"]["guest"]["prefix"]["display"] : (isset($customer_form["prefix_show"]) && $customer_form["prefix_show"]) ? "1" : "0",
            "required_guest" => isset($settings["supercheckout"]["payment_address"]["guest"]["prefix"]["require"]) ? $settings["supercheckout"]["payment_address"]["guest"]["prefix"]["require"] : (isset($customer_form["prefix_show"]) && $customer_form["prefix_show"] == "req") ? "1" : "0",
            "display_login" => isset($settings["supercheckout"]["payment_address"]["login"]["prefix"]["display"]) ? $settings["supercheckout"]["payment_address"]["login"]["prefix"]["display"] : (isset($customer_form["prefix_show"]) && $customer_form["prefix_show"]) ? "1" : "0",
            "required_login" => isset($settings["supercheckout"]["payment_address"]["login"]["prefix"]["require"]) ? $settings["supercheckout"]["payment_address"]["login"]["prefix"]["require"] : (isset($customer_form["prefix_show"]) && $customer_form["prefix_show"] == "req") ? "1" : "0",
            "options" => $prefix_group_items,
            "onchange" => "onPaymentChange([])"
        ];
        $form_fields[] = [
            "label" => __("First Name"),
            "name" => "firstname",
            "type" => "text",
            "value" => (null != $this->quote->getQuote()->getBillingAddress()->getFirstname()) ? $this->quote->getQuote()->getBillingAddress()->getFirstname() : "",
            "sort_order" => isset($settings["supercheckout"]["payment_address"]["fields"]["firstname"]["sort_order"]) ? $settings["supercheckout"]["payment_address"]["fields"]["firstname"]["sort_order"] : "2",
            "enabled" => "1",
            "display_guest" => isset($settings["supercheckout"]["payment_address"]["guest"]["firstname"]["display"]) ? $settings["supercheckout"]["payment_address"]["guest"]["firstname"]["display"] : "1",
            "required_guest" => isset($settings["supercheckout"]["payment_address"]["guest"]["firstname"]["require"]) ? $settings["supercheckout"]["payment_address"]["guest"]["firstname"]["require"] : "1",
            "display_login" => isset($settings["supercheckout"]["payment_address"]["login"]["firstname"]["display"]) ? $settings["supercheckout"]["payment_address"]["login"]["firstname"]["display"] : "1",
            "required_login" => isset($settings["supercheckout"]["payment_address"]["login"]["firstname"]["require"]) ? $settings["supercheckout"]["payment_address"]["login"]["firstname"]["require"] : "1",
            "options" => [],
            "onblur" => "onPaymentChange([])"
        ];
        $form_fields[] = [
            "label" => __("Middle Name/Initial"),
            "name" => "middlename",
            "type" => "text",
            "value" => (null != $this->quote->getQuote()->getBillingAddress()->getMiddlename()) ? $this->quote->getQuote()->getBillingAddress()->getMiddlename() : "",
            "sort_order" => isset($settings["supercheckout"]["payment_address"]["fields"]["middlename"]["sort_order"]) ? $settings["supercheckout"]["payment_address"]["fields"]["middlename"]["sort_order"] : "3",
            "enabled" => (isset($customer_form["middlename_show"]) && $customer_form["middlename_show"]) ? "1" : "0",
            "display_guest" => isset($settings["supercheckout"]["payment_address"]["guest"]["middlename"]["display"]) ? $settings["supercheckout"]["payment_address"]["guest"]["middlename"]["display"] : (isset($customer_form["middlename_show"]) && $customer_form["middlename_show"]) ? "1" : "0",
            "required_guest" => isset($settings["supercheckout"]["payment_address"]["guest"]["middlename"]["require"]) ? $settings["supercheckout"]["payment_address"]["guest"]["middlename"]["require"] : "0",
            "display_login" => isset($settings["supercheckout"]["payment_address"]["login"]["middlename"]["display"]) ? $settings["supercheckout"]["payment_address"]["login"]["middlename"]["display"] : (isset($customer_form["middlename_show"]) && $customer_form["middlename_show"]) ? "1" : "0",
            "required_login" => isset($settings["supercheckout"]["payment_address"]["login"]["middlename"]["require"]) ? $settings["supercheckout"]["payment_address"]["login"]["middlename"]["require"] : "0",
            "options" => [],
            "onblur" => "onPaymentChange([])"
        ];
        $form_fields[] = [
            "label" => __("Last Name"),
            "name" => "lastname",
            "type" => "text",
            "value" => (null != $this->quote->getQuote()->getBillingAddress()->getLastname()) ? $this->quote->getQuote()->getBillingAddress()->getLastname() : "",
            "sort_order" => isset($settings["supercheckout"]["payment_address"]["fields"]["lastname"]["sort_order"]) ? $settings["supercheckout"]["payment_address"]["fields"]["lastname"]["sort_order"] : "4",
            "enabled" => "1",
            "display_guest" => isset($settings["supercheckout"]["payment_address"]["guest"]["lastname"]["display"]) ? $settings["supercheckout"]["payment_address"]["guest"]["lastname"]["display"] : "1",
            "required_guest" => isset($settings["supercheckout"]["payment_address"]["guest"]["lastname"]["require"]) ? $settings["supercheckout"]["payment_address"]["guest"]["lastname"]["require"] : "1",
            "display_login" => isset($settings["supercheckout"]["payment_address"]["login"]["lastname"]["display"]) ? $settings["supercheckout"]["payment_address"]["login"]["lastname"]["display"] : "1",
            "required_login" => isset($settings["supercheckout"]["payment_address"]["login"]["lastname"]["require"]) ? $settings["supercheckout"]["payment_address"]["login"]["lastname"]["require"] : "1",
            "options" => [],
            "onblur" => "onPaymentChange([])"
        ];
        $suffix_group_items = [];
        if (isset($customer_form["suffix_options"]) && $customer_form["suffix_options"] != "") {
            $options_dropdown = array_combine(explode(";", $customer_form["suffix_options"]), explode(";", $customer_form["suffix_options"]));
            foreach ($options_dropdown as $id => $label) {
                if (null != $this->quote->getQuote()->getBillingAddress()->getSuffix()) {
                    if ($id == $this->quote->getQuote()->getBillingAddress()->getSuffix()) {
                        $suffix_group_items[] = ["id" => $id, "label" => $label, 'selected' => true];
                    } else {
                        $suffix_group_items[] = ["id" => $id, "label" => $label, 'selected' => false];
                    }
                } else {
                    $suffix_group_items[] = ["id" => $id, "label" => $label, 'selected' => false];
                }
            }
        }
        $form_fields[] = [
            "label" => __("Suffix"),
            "name" => "suffix",
            "type" => "select",
            "value" => "",
            "options" => [],
            "sort_order" => isset($settings["supercheckout"]["payment_address"]["fields"]["suffix"]["sort_order"]) ? $settings["supercheckout"]["payment_address"]["fields"]["suffix"]["sort_order"] : "5",
            "enabled" => (isset($customer_form["suffix_show"]) && $customer_form["suffix_show"]) ? "1" : "0",
            "display_guest" => isset($settings["supercheckout"]["payment_address"]["guest"]["suffix"]["display"]) ? $settings["supercheckout"]["payment_address"]["guest"]["suffix"]["display"] : (isset($customer_form["suffix_show"]) && $customer_form["suffix_show"]) ? "1" : "0",
            "required_guest" => isset($settings["supercheckout"]["payment_address"]["guest"]["suffix"]["require"]) ? $settings["supercheckout"]["payment_address"]["guest"]["suffix"]["require"] : (isset($customer_form["suffix_show"]) && $customer_form["suffix_show"] == "req") ? "1" : "0",
            "display_login" => isset($settings["supercheckout"]["payment_address"]["login"]["suffix"]["display"]) ? $settings["supercheckout"]["payment_address"]["login"]["suffix"]["display"] : (isset($customer_form["suffix_show"]) && $customer_form["suffix_show"]) ? "1" : "0",
            "required_login" => isset($settings["supercheckout"]["payment_address"]["login"]["suffix"]["require"]) ? $settings["supercheckout"]["payment_address"]["login"]["suffix"]["require"] : (isset($customer_form["suffix_show"]) && $customer_form["suffix_show"] == "req") ? "1" : "0",
            "options" => $suffix_group_items,
            "onchange" => "onPaymentChange([])"
        ];
        $form_fields[] = [
            "label" => __("Company"),
            "name" => "company",
            "type" => "text",
            "value" => (null != $this->quote->getQuote()->getBillingAddress()->getCompany()) ? $this->quote->getQuote()->getBillingAddress()->getCompany() : "",
            "sort_order" => isset($settings["supercheckout"]["payment_address"]["fields"]["company"]["sort_order"]) ? $settings["supercheckout"]["payment_address"]["fields"]["company"]["sort_order"] : "6",
            "enabled" => "1",
            "display_guest" => isset($settings["supercheckout"]["payment_address"]["guest"]["company"]["display"]) ? $settings["supercheckout"]["payment_address"]["guest"]["company"]["display"] : "1",
            "required_guest" => isset($settings["supercheckout"]["payment_address"]["guest"]["company"]["require"]) ? $settings["supercheckout"]["payment_address"]["guest"]["company"]["require"] : "0",
            "display_login" => isset($settings["supercheckout"]["payment_address"]["login"]["company"]["display"]) ? $settings["supercheckout"]["payment_address"]["login"]["company"]["display"] : "1",
            "required_login" => isset($settings["supercheckout"]["payment_address"]["login"]["company"]["require"]) ? $settings["supercheckout"]["payment_address"]["login"]["company"]["require"] : "0",
            "options" => [],
            "onblur" => "onPaymentChange([])"
        ];
        $street = [];
        if (null != $this->quote->getQuote()->getBillingAddress()->getStreet()) {
            $street = $this->quote->getQuote()->getBillingAddress()->getStreet();
        }
        $form_fields[] = [
            "label" => __("Address 1"),
            "name" => "street1",
            "type" => "text",
            "value" => isset($street[0]) ? $street[0] : "",
            "sort_order" => isset($settings["supercheckout"]["payment_address"]["fields"]["street1"]["sort_order"]) ? $settings["supercheckout"]["payment_address"]["fields"]["street1"]["sort_order"] : "7",
            "enabled" => "1",
            "display_guest" => isset($settings["supercheckout"]["payment_address"]["guest"]["street1"]["display"]) ? $settings["supercheckout"]["payment_address"]["guest"]["street1"]["display"] : "1",
            "required_guest" => isset($settings["supercheckout"]["payment_address"]["guest"]["street1"]["require"]) ? $settings["supercheckout"]["payment_address"]["guest"]["street1"]["require"] : "1",
            "display_login" => isset($settings["supercheckout"]["payment_address"]["login"]["street1"]["display"]) ? $settings["supercheckout"]["payment_address"]["login"]["street1"]["display"] : "1",
            "required_login" => isset($settings["supercheckout"]["payment_address"]["login"]["street1"]["require"]) ? $settings["supercheckout"]["payment_address"]["login"]["street1"]["require"] : "1",
            "options" => [],
            "onblur" => "onPaymentChange([])"
        ];
        $form_fields[] = [
            "label" => __("Address 2"),
            "name" => "street2",
            "type" => "text",
            "value" => isset($street[1]) ? $street[1] : "",
            "sort_order" => isset($settings["supercheckout"]["payment_address"]["fields"]["street2"]["sort_order"]) ? $settings["supercheckout"]["payment_address"]["fields"]["street2"]["sort_order"] : "8",
            "enabled" => "1",
            "display_guest" => isset($settings["supercheckout"]["payment_address"]["guest"]["street2"]["display"]) ? $settings["supercheckout"]["payment_address"]["guest"]["street2"]["display"] : "1",
            "required_guest" => isset($settings["supercheckout"]["payment_address"]["guest"]["street2"]["require"]) ? $settings["supercheckout"]["payment_address"]["guest"]["street2"]["require"] : "0",
            "display_login" => isset($settings["supercheckout"]["payment_address"]["login"]["street2"]["display"]) ? $settings["supercheckout"]["payment_address"]["login"]["street2"]["display"] : "1",
            "required_login" => isset($settings["supercheckout"]["payment_address"]["login"]["street2"]["require"]) ? $settings["supercheckout"]["payment_address"]["login"]["street2"]["require"] : "0",
            "options" => [],
            "onblur" => "onPaymentChange([])"
        ];
        $form_fields[] = [
            "label" => __("City"),
            "name" => "city",
            "type" => "text",
            "value" => (null != $this->quote->getQuote()->getBillingAddress()->getCity()) ? $this->quote->getQuote()->getBillingAddress()->getCity() : "",
            "sort_order" => isset($settings["supercheckout"]["payment_address"]["fields"]["city"]["sort_order"]) ? $settings["supercheckout"]["payment_address"]["fields"]["city"]["sort_order"] : "9",
            "enabled" => "1",
            "display_guest" => isset($settings["supercheckout"]["payment_address"]["guest"]["city"]["display"]) ? $settings["supercheckout"]["payment_address"]["guest"]["city"]["display"] : "1",
            "required_guest" => isset($settings["supercheckout"]["payment_address"]["guest"]["city"]["require"]) ? $settings["supercheckout"]["payment_address"]["guest"]["city"]["require"] : "1",
            "display_login" => isset($settings["supercheckout"]["payment_address"]["login"]["city"]["display"]) ? $settings["supercheckout"]["payment_address"]["login"]["city"]["display"] : "1",
            "required_login" => isset($settings["supercheckout"]["payment_address"]["login"]["city"]["require"]) ? $settings["supercheckout"]["payment_address"]["login"]["city"]["require"] : "1",
            "options" => [],
            "onblur" => "onPaymentChange([])"
        ];
        $form_fields[] = [
            "label" => __("Country"),
            "name" => "country_id",
            "type" => "select",
            "value" => "",
            "options" => [],
            "sort_order" => isset($settings["supercheckout"]["payment_address"]["fields"]["country"]["sort_order"]) ? $settings["supercheckout"]["payment_address"]["fields"]["country"]["sort_order"] : "10",
            "enabled" => "1",
            "display_guest" => isset($settings["supercheckout"]["payment_address"]["guest"]["country"]["display"]) ? $settings["supercheckout"]["payment_address"]["guest"]["country"]["display"] : "1",
            "required_guest" => isset($settings["supercheckout"]["payment_address"]["guest"]["country"]["require"]) ? $settings["supercheckout"]["payment_address"]["guest"]["country"]["require"] : "1",
            "display_login" => isset($settings["supercheckout"]["payment_address"]["login"]["country"]["display"]) ? $settings["supercheckout"]["payment_address"]["login"]["country"]["display"] : "1",
            "required_login" => isset($settings["supercheckout"]["payment_address"]["login"]["country"]["require"]) ? $settings["supercheckout"]["payment_address"]["login"]["country"]["require"] : "1",
            "options" => $this->getAvailablePaymentCountries(),
            "onchange" => "onPaymentCountryChange(this)"
        ];
        $form_fields[] = [
            "label" => __("State/Province"),
            "name" => "region_id",
            "type" => "select",
            "value" => "",
            "options" => [],
            "sort_order" => isset($settings["supercheckout"]["payment_address"]["fields"]["region"]["sort_order"]) ? $settings["supercheckout"]["payment_address"]["fields"]["region"]["sort_order"] : "11",
            "enabled" => "1",
            "display_guest" => isset($settings["supercheckout"]["payment_address"]["guest"]["region"]["display"]) ? $settings["supercheckout"]["payment_address"]["guest"]["region"]["display"] : "1",
            "required_guest" => isset($settings["supercheckout"]["payment_address"]["guest"]["region"]["require"]) ? $settings["supercheckout"]["payment_address"]["guest"]["region"]["require"] : "1",
            "display_login" => isset($settings["supercheckout"]["payment_address"]["login"]["region"]["display"]) ? $settings["supercheckout"]["payment_address"]["login"]["region"]["display"] : "1",
            "required_login" => isset($settings["supercheckout"]["payment_address"]["login"]["region"]["require"]) ? $settings["supercheckout"]["payment_address"]["login"]["region"]["require"] : "1",
            "options" => $this->getPaymentRegions(),
            "onchange" => "onPaymentChange(['shipping-address','shipping-method','payment-method','cart'])"
        ];
        $form_fields[] = [
            "label" => __("State/Province"),
            "name" => "region",
            "type" => "text",
            "value" => (null != $this->quote->getQuote()->getBillingAddress()->getRegion()) ? $this->quote->getQuote()->getBillingAddress()->getRegion() : "",
            "options" => [],
            "sort_order" => isset($settings["supercheckout"]["payment_address"]["fields"]["region"]["sort_order"]) ? $settings["supercheckout"]["payment_address"]["fields"]["region"]["sort_order"] : "11",
            "enabled" => "1",
            "display_guest" => isset($settings["supercheckout"]["payment_address"]["guest"]["region"]["display"]) ? $settings["supercheckout"]["payment_address"]["guest"]["region"]["display"] : "1",
            "required_guest" => isset($settings["supercheckout"]["payment_address"]["guest"]["region"]["require"]) ? $settings["supercheckout"]["payment_address"]["guest"]["region"]["require"] : "1",
            "display_login" => isset($settings["supercheckout"]["payment_address"]["login"]["region"]["display"]) ? $settings["supercheckout"]["payment_address"]["login"]["region"]["display"] : "1",
            "required_login" => isset($settings["supercheckout"]["payment_address"]["login"]["region"]["require"]) ? $settings["supercheckout"]["payment_address"]["login"]["region"]["require"] : "1",
            "options" => [],
            "onblur" => "onPaymentChange(['shipping-address','shipping-method','payment-method','cart'])"
        ];
        $form_fields[] = [
            "label" => __("Zip/Postal Code"),
            "name" => "postcode",
            "type" => "text",
            "value" => (null != $this->quote->getQuote()->getBillingAddress()->getPostcode()) ? $this->quote->getQuote()->getBillingAddress()->getPostcode() : "",
            "sort_order" => isset($settings["supercheckout"]["payment_address"]["fields"]["postcode"]["sort_order"]) ? $settings["supercheckout"]["payment_address"]["fields"]["postcode"]["sort_order"] : "12",
            "enabled" => "1",
            "display_guest" => isset($settings["supercheckout"]["payment_address"]["guest"]["postcode"]["display"]) ? $settings["supercheckout"]["payment_address"]["guest"]["postcode"]["display"] : "1",
            "required_guest" => isset($settings["supercheckout"]["payment_address"]["guest"]["postcode"]["require"]) ? $settings["supercheckout"]["payment_address"]["guest"]["postcode"]["require"] : "0",
            "display_login" => isset($settings["supercheckout"]["payment_address"]["login"]["postcode"]["display"]) ? $settings["supercheckout"]["payment_address"]["login"]["postcode"]["display"] : "1",
            "required_login" => isset($settings["supercheckout"]["payment_address"]["login"]["postcode"]["require"]) ? $settings["supercheckout"]["payment_address"]["login"]["postcode"]["require"] : "0",
            "options" => [],
            "onblur" => "onPaymentChange(['shipping-address','shipping-method','payment-method','cart'])"
        ];
        $form_fields[] = [
            "label" => __("Telephone"),
            "name" => "telephone",
            "type" => "text",
            "value" => (null != $this->quote->getQuote()->getBillingAddress()->getTelephone()) ? $this->quote->getQuote()->getBillingAddress()->getTelephone() : "",
            "sort_order" => isset($settings["supercheckout"]["payment_address"]["fields"]["telephone"]["sort_order"]) ? $settings["supercheckout"]["payment_address"]["fields"]["telephone"]["sort_order"] : "13",
            "enabled" => "1",
            "display_guest" => isset($settings["supercheckout"]["payment_address"]["guest"]["telephone"]["display"]) ? $settings["supercheckout"]["payment_address"]["guest"]["telephone"]["display"] : "1",
            "required_guest" => isset($settings["supercheckout"]["payment_address"]["guest"]["telephone"]["require"]) ? $settings["supercheckout"]["payment_address"]["guest"]["telephone"]["require"] : "1",
            "display_login" => isset($settings["supercheckout"]["payment_address"]["login"]["telephone"]["display"]) ? $settings["supercheckout"]["payment_address"]["login"]["telephone"]["display"] : "1",
            "required_login" => isset($settings["supercheckout"]["payment_address"]["login"]["telephone"]["require"]) ? $settings["supercheckout"]["payment_address"]["login"]["telephone"]["require"] : "1",
            "options" => [],
            "onblur" => "onPaymentChange([])"
        ];
        $form_fields[] = [
            "label" => __("Fax"),
            "name" => "fax",
            "type" => "text",
            "value" => (null != $this->quote->getQuote()->getBillingAddress()->getFax()) ? $this->quote->getQuote()->getBillingAddress()->getFax() : "",
            "sort_order" => isset($settings["supercheckout"]["payment_address"]["fields"]["fax"]["sort_order"]) ? $settings["supercheckout"]["payment_address"]["fields"]["fax"]["sort_order"] : "14",
            "enabled" => "1",
            "display_guest" => isset($settings["supercheckout"]["payment_address"]["guest"]["fax"]["display"]) ? $settings["supercheckout"]["payment_address"]["guest"]["fax"]["display"] : "1",
            "required_guest" => isset($settings["supercheckout"]["payment_address"]["guest"]["fax"]["require"]) ? $settings["supercheckout"]["payment_address"]["guest"]["fax"]["require"] : "0",
            "display_login" => isset($settings["supercheckout"]["payment_address"]["login"]["fax"]["display"]) ? $settings["supercheckout"]["payment_address"]["login"]["fax"]["display"] : "1",
            "required_login" => isset($settings["supercheckout"]["payment_address"]["login"]["fax"]["require"]) ? $settings["supercheckout"]["payment_address"]["login"]["fax"]["require"] : "0",
            "options" => [],
            "onblur" => "onPaymentChange([])"
        ];
        $form_fields[] = [
            "label" => __("VAT number"),
            "name" => "vat_id",
            "type" => "text",
            "value" => (null != $this->quote->getQuote()->getBillingAddress()->getVatId()) ? $this->quote->getQuote()->getBillingAddress()->getVatId() : "",
            "sort_order" => isset($settings["supercheckout"]["payment_address"]["fields"]["taxvat"]["sort_order"]) ? $settings["supercheckout"]["payment_address"]["fields"]["taxvat"]["sort_order"] : "15",
            "enabled" => (isset($customer_form["taxvat_show"]) && $customer_form["taxvat_show"]) ? "1" : "0",
            "display_guest" => isset($settings["supercheckout"]["payment_address"]["guest"]["taxvat"]["display"]) ? $settings["supercheckout"]["payment_address"]["guest"]["taxvat"]["display"] : (isset($customer_form["taxvat_show"]) && $customer_form["taxvat_show"]) ? "1" : "0",
            "required_guest" => isset($settings["supercheckout"]["payment_address"]["guest"]["taxvat"]["require"]) ? $settings["supercheckout"]["payment_address"]["guest"]["taxvat"]["require"] : (isset($customer_form["taxvat_show"]) && $customer_form["taxvat_show"] == "req") ? "1" : "0",
            "display_login" => isset($settings["supercheckout"]["payment_address"]["login"]["taxvat"]["display"]) ? $settings["supercheckout"]["payment_address"]["login"]["taxvat"]["display"] : (isset($customer_form["taxvat_show"]) && $customer_form["taxvat_show"]) ? "1" : "0",
            "required_login" => isset($settings["supercheckout"]["payment_address"]["login"]["taxvat"]["require"]) ? $settings["supercheckout"]["payment_address"]["login"]["taxvat"]["require"] : (isset($customer_form["taxvat_show"]) && $customer_form["taxvat_show"] == "req") ? "1" : "0",
            "options" => [],
            "onblur" => "onPaymentChange([])"
        ];
        return $form_fields;
    }

    public function getShippingAddressFields()
    {

        $customer_form = $this->sc_scopeConfig->getValue('customer/address', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, null);
        $settings = $this->sc_helper->getSettings();
        $form_fields = [];
        $prefix_group_items = [];
        if (isset($customer_form["prefix_options"]) && $customer_form["prefix_options"] != "") {
            $options_dropdown = array_combine(explode(";", $customer_form["prefix_options"]), explode(";", $customer_form["prefix_options"]));
            foreach ($options_dropdown as $id => $label) {
                if (null != $this->quote->getQuote()->getShippingAddress()->getPrefix()) {
                    if ($id == $this->quote->getQuote()->getShippingAddress()->getPrefix()) {
                        $prefix_group_items[] = ["id" => $id, "label" => $label, 'selected' => true];
                    } else {
                        $prefix_group_items[] = ["id" => $id, "label" => $label, 'selected' => false];
                    }
                } else {
                    $prefix_group_items[] = ["id" => $id, "label" => $label, 'selected' => false];
                }
            }
        }
        $form_fields[] = [
            "label" => __("Prefix"),
            "name" => "prefix",
            "type" => "select",
            "value" => (null != $this->quote->getQuote()->getShippingAddress()->getPrefix()) ? $this->quote->getQuote()->getShippingAddress()->getPrefix() : "",
            "options" => [],
            "sort_order" => isset($settings["supercheckout"]["shipping_address"]["fields"]["prefix"]["sort_order"]) ? $settings["supercheckout"]["shipping_address"]["fields"]["prefix"]["sort_order"] : "1",
            "enabled" => (isset($customer_form["prefix_show"]) && $customer_form["prefix_show"]) ? "1" : "0",
            "display_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["prefix"]["display"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["prefix"]["display"] : (isset($customer_form["prefix_show"]) && $customer_form["prefix_show"]) ? "1" : "0",
            "required_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["prefix"]["require"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["prefix"]["require"] : (isset($customer_form["prefix_show"]) && $customer_form["prefix_show"] == "req") ? "1" : "0",
            "display_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["prefix"]["display"]) ? $settings["supercheckout"]["shipping_address"]["login"]["prefix"]["display"] : (isset($customer_form["prefix_show"]) && $customer_form["prefix_show"]) ? "1" : "0",
            "required_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["prefix"]["require"]) ? $settings["supercheckout"]["shipping_address"]["login"]["prefix"]["require"] : (isset($customer_form["prefix_show"]) && $customer_form["prefix_show"] == "req") ? "1" : "0",
            "options" => $prefix_group_items,
            "onchange" => "onShippingChange(false)"
        ];
        $form_fields[] = [
            "label" => __("First Name"),
            "name" => "firstname",
            "type" => "text",
            "value" => (null != $this->quote->getQuote()->getShippingAddress()->getFirstname()) ? $this->quote->getQuote()->getShippingAddress()->getFirstname() : "",
            "sort_order" => isset($settings["supercheckout"]["shipping_address"]["fields"]["firstname"]["sort_order"]) ? $settings["supercheckout"]["shipping_address"]["fields"]["firstname"]["sort_order"] : "2",
            "enabled" => "1",
            "display_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["firstname"]["display"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["firstname"]["display"] : "1",
            "required_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["firstname"]["require"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["firstname"]["require"] : "1",
            "display_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["firstname"]["display"]) ? $settings["supercheckout"]["shipping_address"]["login"]["firstname"]["display"] : "1",
            "required_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["firstname"]["require"]) ? $settings["supercheckout"]["shipping_address"]["login"]["firstname"]["require"] : "1",
            "options" => [],
            "onblur" => "onShippingChange(false)"
        ];
        $form_fields[] = [
            "label" => __("Middle Name/Initial"),
            "name" => "middlename",
            "type" => "text",
            "value" => (null != $this->quote->getQuote()->getShippingAddress()->getMiddlename()) ? $this->quote->getQuote()->getShippingAddress()->getMiddlename() : "",
            "sort_order" => isset($settings["supercheckout"]["shipping_address"]["fields"]["middlename"]["sort_order"]) ? $settings["supercheckout"]["shipping_address"]["fields"]["middlename"]["sort_order"] : "3",
            "enabled" => (isset($customer_form["middlename_show"]) && $customer_form["middlename_show"]) ? "1" : "0",
            "display_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["middlename"]["display"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["middlename"]["display"] : (isset($customer_form["middlename_show"]) && $customer_form["middlename_show"]) ? "1" : "0",
            "required_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["middlename"]["require"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["middlename"]["require"] : "0",
            "display_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["middlename"]["display"]) ? $settings["supercheckout"]["shipping_address"]["login"]["middlename"]["display"] : (isset($customer_form["middlename_show"]) && $customer_form["middlename_show"]) ? "1" : "0",
            "required_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["middlename"]["require"]) ? $settings["supercheckout"]["shipping_address"]["login"]["middlename"]["require"] : "0",
            "options" => [],
            "onblur" => "onShippingChange(false)"
        ];
        $form_fields[] = [
            "label" => __("Last Name"),
            "name" => "lastname",
            "type" => "text",
            "value" => (null != $this->quote->getQuote()->getShippingAddress()->getLastname()) ? $this->quote->getQuote()->getShippingAddress()->getLastname() : "",
            "sort_order" => isset($settings["supercheckout"]["shipping_address"]["fields"]["lastname"]["sort_order"]) ? $settings["supercheckout"]["shipping_address"]["fields"]["lastname"]["sort_order"] : "4",
            "enabled" => "1",
            "display_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["lastname"]["display"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["lastname"]["display"] : "1",
            "required_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["lastname"]["require"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["lastname"]["require"] : "1",
            "display_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["lastname"]["display"]) ? $settings["supercheckout"]["shipping_address"]["login"]["lastname"]["display"] : "1",
            "required_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["lastname"]["require"]) ? $settings["supercheckout"]["shipping_address"]["login"]["lastname"]["require"] : "1",
            "options" => [],
            "onblur" => "onShippingChange(false)"
        ];
        $suffix_group_items = [];
        if (isset($customer_form["suffix_options"]) && $customer_form["suffix_options"] != "") {
            $options_dropdown = array_combine(explode(";", $customer_form["suffix_options"]), explode(";", $customer_form["suffix_options"]));
            foreach ($options_dropdown as $id => $label) {
                if (null != $this->quote->getQuote()->getShippingAddress()->getSuffix()) {
                    if ($id == $this->quote->getQuote()->getShippingAddress()->getSuffix()) {
                        $suffix_group_items[] = ["id" => $id, "label" => $label, 'selected' => true];
                    } else {
                        $suffix_group_items[] = ["id" => $id, "label" => $label, 'selected' => false];
                    }
                } else {
                    $suffix_group_items[] = ["id" => $id, "label" => $label, 'selected' => false];
                }
            }
        }
        $form_fields[] = [
            "label" => __("Suffix"),
            "name" => "suffix",
            "type" => "select",
            "value" => "",
            "options" => [],
            "sort_order" => isset($settings["supercheckout"]["shipping_address"]["fields"]["suffix"]["sort_order"]) ? $settings["supercheckout"]["shipping_address"]["fields"]["suffix"]["sort_order"] : "5",
            "enabled" => (isset($customer_form["suffix_show"]) && $customer_form["suffix_show"]) ? "1" : "0",
            "display_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["suffix"]["display"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["suffix"]["display"] : (isset($customer_form["suffix_show"]) && $customer_form["suffix_show"]) ? "1" : "0",
            "required_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["suffix"]["require"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["suffix"]["require"] : (isset($customer_form["suffix_show"]) && $customer_form["suffix_show"] == "req") ? "1" : "0",
            "display_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["suffix"]["display"]) ? $settings["supercheckout"]["shipping_address"]["login"]["suffix"]["display"] : (isset($customer_form["suffix_show"]) && $customer_form["suffix_show"]) ? "1" : "0",
            "required_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["suffix"]["require"]) ? $settings["supercheckout"]["shipping_address"]["login"]["suffix"]["require"] : (isset($customer_form["suffix_show"]) && $customer_form["suffix_show"] == "req") ? "1" : "0",
            "options" => $suffix_group_items,
            "onchange" => "onShippingChange(false)"
        ];
        $form_fields[] = [
            "label" => __("Company"),
            "name" => "company",
            "type" => "text",
            "value" => (null != $this->quote->getQuote()->getShippingAddress()->getCompany()) ? $this->quote->getQuote()->getShippingAddress()->getCompany() : "",
            "sort_order" => isset($settings["supercheckout"]["shipping_address"]["fields"]["company"]["sort_order"]) ? $settings["supercheckout"]["shipping_address"]["fields"]["company"]["sort_order"] : "6",
            "enabled" => "1",
            "display_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["company"]["display"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["company"]["display"] : "1",
            "required_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["company"]["require"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["company"]["require"] : "0",
            "display_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["company"]["display"]) ? $settings["supercheckout"]["shipping_address"]["login"]["company"]["display"] : "1",
            "required_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["company"]["require"]) ? $settings["supercheckout"]["shipping_address"]["login"]["company"]["require"] : "0",
            "options" => [],
            "onblur" => "onShippingChange(false)"
        ];
        $street = [];
        if (null != $this->quote->getQuote()->getShippingAddress()->getStreet()) {
            $street = $this->quote->getQuote()->getShippingAddress()->getStreet();
        }
        $form_fields[] = [
            "label" => __("Address 1"),
            "name" => "street1",
            "type" => "text",
            "value" => isset($street[0]) ? $street[0] : "",
            "sort_order" => isset($settings["supercheckout"]["shipping_address"]["fields"]["street1"]["sort_order"]) ? $settings["supercheckout"]["shipping_address"]["fields"]["street1"]["sort_order"] : "7",
            "enabled" => "1",
            "display_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["street1"]["display"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["street1"]["display"] : "1",
            "required_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["street1"]["require"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["street1"]["require"] : "1",
            "display_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["street1"]["display"]) ? $settings["supercheckout"]["shipping_address"]["login"]["street1"]["display"] : "1",
            "required_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["street1"]["require"]) ? $settings["supercheckout"]["shipping_address"]["login"]["street1"]["require"] : "1",
            "options" => [],
            "onblur" => "onShippingChange(false)"
        ];
        $form_fields[] = [
            "label" => __("Address 2"),
            "name" => "street2",
            "type" => "text",
            "value" => isset($street[1]) ? $street[1] : "",
            "sort_order" => isset($settings["supercheckout"]["shipping_address"]["fields"]["street2"]["sort_order"]) ? $settings["supercheckout"]["shipping_address"]["fields"]["street2"]["sort_order"] : "8",
            "enabled" => "1",
            "display_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["street2"]["display"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["street2"]["display"] : "1",
            "required_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["street2"]["require"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["street2"]["require"] : "0",
            "display_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["street2"]["display"]) ? $settings["supercheckout"]["shipping_address"]["login"]["street2"]["display"] : "1",
            "required_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["street2"]["require"]) ? $settings["supercheckout"]["shipping_address"]["login"]["street2"]["require"] : "0",
            "options" => [],
            "onblur" => "onShippingChange(false)"
        ];
        $form_fields[] = [
            "label" => __("City"),
            "name" => "city",
            "type" => "text",
            "value" => (null != $this->quote->getQuote()->getShippingAddress()->getCity()) ? $this->quote->getQuote()->getShippingAddress()->getCity() : "",
            "sort_order" => isset($settings["supercheckout"]["shipping_address"]["fields"]["city"]["sort_order"]) ? $settings["supercheckout"]["shipping_address"]["fields"]["city"]["sort_order"] : "9",
            "enabled" => "1",
            "display_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["city"]["display"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["city"]["display"] : "1",
            "required_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["city"]["require"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["city"]["require"] : "1",
            "display_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["city"]["display"]) ? $settings["supercheckout"]["shipping_address"]["login"]["city"]["display"] : "1",
            "required_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["city"]["require"]) ? $settings["supercheckout"]["shipping_address"]["login"]["city"]["require"] : "1",
            "options" => [],
            "onblur" => "onShippingChange(false)"
        ];
        $form_fields[] = [
            "label" => __("Country"),
            "name" => "country_id",
            "type" => "select",
            "value" => "",
            "options" => [],
            "sort_order" => isset($settings["supercheckout"]["shipping_address"]["fields"]["country"]["sort_order"]) ? $settings["supercheckout"]["shipping_address"]["fields"]["country"]["sort_order"] : "10",
            "enabled" => "1",
            "display_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["country"]["display"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["country"]["display"] : "1",
            "required_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["country"]["require"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["country"]["require"] : "1",
            "display_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["country"]["display"]) ? $settings["supercheckout"]["shipping_address"]["login"]["country"]["display"] : "1",
            "required_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["country"]["require"]) ? $settings["supercheckout"]["shipping_address"]["login"]["country"]["require"] : "1",
            "options" => $this->getAvailableShippingCountries(),
            "onchange" => "onShippingCountryChange(this)"
        ];
        $form_fields[] = [
            "label" => __("State/Province"),
            "name" => "region_id",
            "type" => "select",
            "value" => "",
            "options" => [],
            "sort_order" => isset($settings["supercheckout"]["shipping_address"]["fields"]["region"]["sort_order"]) ? $settings["supercheckout"]["shipping_address"]["fields"]["region"]["sort_order"] : "11",
            "enabled" => "1",
            "display_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["region"]["display"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["region"]["display"] : "1",
            "required_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["region"]["require"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["region"]["require"] : "1",
            "display_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["region"]["display"]) ? $settings["supercheckout"]["shipping_address"]["login"]["region"]["display"] : "1",
            "required_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["region"]["require"]) ? $settings["supercheckout"]["shipping_address"]["login"]["region"]["require"] : "1",
            "options" => $this->getShippingRegions(),
            "onchange" => "onShippingChange(true)"
        ];
        $form_fields[] = [
            "label" => __("State/Province"),
            "name" => "region",
            "type" => "text",
            "value" => (null != $this->quote->getQuote()->getShippingAddress()->getRegion()) ? $this->quote->getQuote()->getShippingAddress()->getRegion() : "",
            "options" => [],
            "sort_order" => isset($settings["supercheckout"]["shipping_address"]["fields"]["region"]["sort_order"]) ? $settings["supercheckout"]["shipping_address"]["fields"]["region"]["sort_order"] : "11",
            "enabled" => "1",
            "display_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["region"]["display"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["region"]["display"] : "1",
            "required_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["region"]["require"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["region"]["require"] : "1",
            "display_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["region"]["display"]) ? $settings["supercheckout"]["shipping_address"]["login"]["region"]["display"] : "1",
            "required_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["region"]["require"]) ? $settings["supercheckout"]["shipping_address"]["login"]["region"]["require"] : "1",
            "options" => [],
            "onblur" => "onShippingChange(true)"
        ];
        $form_fields[] = [
            "label" => __("Zip/Postal Code"),
            "name" => "postcode",
            "type" => "text",
            "value" => (null != $this->quote->getQuote()->getShippingAddress()->getPostcode()) ? $this->quote->getQuote()->getShippingAddress()->getPostcode() : "",
            "sort_order" => isset($settings["supercheckout"]["shipping_address"]["fields"]["postcode"]["sort_order"]) ? $settings["supercheckout"]["shipping_address"]["fields"]["postcode"]["sort_order"] : "12",
            "enabled" => "1",
            "display_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["postcode"]["display"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["postcode"]["display"] : "1",
            "required_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["postcode"]["require"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["postcode"]["require"] : "0",
            "display_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["postcode"]["display"]) ? $settings["supercheckout"]["shipping_address"]["login"]["postcode"]["display"] : "1",
            "required_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["postcode"]["require"]) ? $settings["supercheckout"]["shipping_address"]["login"]["postcode"]["require"] : "0",
            "options" => [],
            "onblur" => "onShippingChange(true)"
        ];
        $form_fields[] = [
            "label" => __("Telephone"),
            "name" => "telephone",
            "type" => "text",
            "value" => (null != $this->quote->getQuote()->getShippingAddress()->getTelephone()) ? $this->quote->getQuote()->getShippingAddress()->getTelephone() : "",
            "sort_order" => isset($settings["supercheckout"]["shipping_address"]["fields"]["telephone"]["sort_order"]) ? $settings["supercheckout"]["shipping_address"]["fields"]["telephone"]["sort_order"] : "13",
            "enabled" => "1",
            "display_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["telephone"]["display"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["telephone"]["display"] : "1",
            "required_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["telephone"]["require"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["telephone"]["require"] : "1",
            "display_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["telephone"]["display"]) ? $settings["supercheckout"]["shipping_address"]["login"]["telephone"]["display"] : "1",
            "required_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["telephone"]["require"]) ? $settings["supercheckout"]["shipping_address"]["login"]["telephone"]["require"] : "1",
            "options" => [],
            "onblur" => "onShippingChange(false)"
        ];
        $form_fields[] = [
            "label" => __("Fax"),
            "name" => "fax",
            "type" => "text",
            "value" => (null != $this->quote->getQuote()->getShippingAddress()->getFax()) ? $this->quote->getQuote()->getShippingAddress()->getFax() : "",
            "sort_order" => isset($settings["supercheckout"]["shipping_address"]["fields"]["fax"]["sort_order"]) ? $settings["supercheckout"]["shipping_address"]["fields"]["fax"]["sort_order"] : "14",
            "enabled" => "1",
            "display_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["fax"]["display"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["fax"]["display"] : "1",
            "required_guest" => isset($settings["supercheckout"]["shipping_address"]["guest"]["fax"]["require"]) ? $settings["supercheckout"]["shipping_address"]["guest"]["fax"]["require"] : "0",
            "display_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["fax"]["display"]) ? $settings["supercheckout"]["shipping_address"]["login"]["fax"]["display"] : "1",
            "required_login" => isset($settings["supercheckout"]["shipping_address"]["login"]["fax"]["require"]) ? $settings["supercheckout"]["shipping_address"]["login"]["fax"]["require"] : "0",
            "options" => [],
            "onblur" => "onShippingChange(false)"
        ];
        return $form_fields;
    }

    public function getAvailablePaymentCountries()
    {
        $default_country = $this->sc_scopeConfig->getValue('general/country/default', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, null);
        $collection = $this->sc_countryCollectionFactory->create();
        $collection->addFieldToSelect('*');
        $countries = [];
        foreach ($collection as $country) {
            $selected = false;
            if (null != $this->quote->getQuote()->getBillingAddress()->getCountryId()) {
                if ($this->quote->getQuote()->getBillingAddress()->getCountryId() == $country->getCountryId()) {
                    $selected = true;
                }
            } else {
                if ($default_country == $country->getCountryId()) {
                    $selected = true;
                }
            }
            $countries[] = ["id" => $country->getCountryId(), "label" => $country->getName(), "selected" => $selected];
        }
        return $countries;
    }

    public function getPaymentRegions()
    {

        if (null != $this->quote->getQuote()->getBillingAddress()->getCountryId()) {
            $default_country = $this->quote->getQuote()->getBillingAddress()->getCountryId();
        } else {
            $default_country = $this->sc_scopeConfig->getValue('general/country/default', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, null);
        }

        $stateArray = $this->sc_countryFactory->create()->setId($default_country)->getLoadedRegionCollection()->toOptionArray();
        $regions = [];
        if (count($stateArray) > 0) {
            foreach ($stateArray as $state) {
                /* Start - Code Modified by RS on 08-Sept-2017 for undefined Index issue on page load */
                if (isset($state["title"]) && $state["title"] != "") {
                    if (null != $this->quote->getQuote()->getBillingAddress()->getRegionId()) {
                        if ($this->quote->getQuote()->getBillingAddress()->getRegionId() == $state["value"]) {
                            $regions[] = ["id" => $state["value"], "label" => $state["title"], "selected" => true];
                        } else {
                            $regions[] = ["id" => $state["value"], "label" => $state["title"], "selected" => false];
                        }
                    } else {
                        $regions[] = ["id" => $state["value"], "label" => $state["title"], "selected" => false];
                    }
                }
                /* Start - Code Modified by RS on 08-Sept-2017 for undefined Index issue on page load */
            }
        }
        return $regions;
    }

    public function getAvailableShippingCountries()
    {
        $default_country = $this->sc_scopeConfig->getValue('general/country/default', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, null);
        $collection = $this->sc_countryCollectionFactory->create();
        $collection->addFieldToSelect('*');
        $countries = [];
        foreach ($collection as $country) {
            $selected = false;
            if (null != $this->quote->getQuote()->getShippingAddress()->getCountryId()) {
                if ($this->quote->getQuote()->getShippingAddress()->getCountryId() == $country->getCountryId()) {
                    $selected = true;
                }
            } else {
                if ($default_country == $country->getCountryId()) {
                    $selected = true;
                }
            }
            $countries[] = ["id" => $country->getCountryId(), "label" => $country->getName(), "selected" => $selected];
        }
        return $countries;
    }

    public function getShippingRegions()
    {

        if (null != $this->quote->getQuote()->getShippingAddress()->getCountryId()) {
            $default_country = $this->quote->getQuote()->getShippingAddress()->getCountryId();
        } else {
            $default_country = $this->sc_scopeConfig->getValue('general/country/default', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, null);
        }

        $stateArray = $this->sc_countryFactory->create()->setId($default_country)->getLoadedRegionCollection()->toOptionArray();
        $regions = [];
        if (count($stateArray) > 0) {
            foreach ($stateArray as $state) {
                /* Start - Code Modified by RS on 08-Sept-2017 for undefined Index issue on page load */
                if (isset($state["title"]) && $state["title"] != "") {
                    if (null != $this->quote->getQuote()->getShippingAddress()->getRegionId()) {
                        if ($this->quote->getQuote()->getShippingAddress()->getRegionId() == $state["value"]) {
                            $regions[] = ["id" => $state["value"], "label" => $state["title"], "selected" => true];
                        } else {
                            $regions[] = ["id" => $state["value"], "label" => $state["title"], "selected" => false];
                        }
                    } else {
                        $regions[] = ["id" => $state["value"], "label" => $state["title"], "selected" => false];
                    }
                }
                /* End - Code Modified by RS on 08-Sept-2017 for undefined Index issue on page load */
            }
        }
        return $regions;
    }
}
