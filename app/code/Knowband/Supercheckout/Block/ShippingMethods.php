<?php

namespace Knowband\Supercheckout\Block;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

class ShippingMethods extends \Knowband\Supercheckout\Block\SupercheckoutFront
{

    public function getAddresses()
    {
        return $this->quote->getQuote()->getShippingAddress();
    }

    public function getCheckout()
    {
        return $this->quote;
    }

    public function getShippingRates($address)
    {
        try {
            $quote = $this->quote->getQuote();
            $address = $quote->getShippingAddress();
            $existing_shipping_method = $this->quote->getQuote()->getShippingAddress()->getShippingMethod();
            if (null == $existing_shipping_method) {
                $default_shipping_method = $this->sc_settings["supercheckout"]["default_shipping_method"];
                $address->setShippingMethod($default_shipping_method)->save();
                $this->quote->getQuote()->collectTotals()->save();
            }
            if ($this->sc_customerSession->isLoggedIn()) {
                if (null != $this->quote->getData("billing_address_id")) {
                    $address_id = $this->quote->getData("billing_address_id");
                } else {
                    $address_id = $this->sc_customerSession->getCustomer()->getDefaultShipping();
                }
                $shipping_address = $this->quote->getQuote()->getShippingAddress();
                try {
                    if ($address_id != "new" && $address_id) {
                        $shipping_address_data = $this->addressRepository->getById($address_id);
                        $shipping_address->importCustomerAddressData($shipping_address_data)->setSaveInAddressBook(0);
                    }
                } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
                    /** Catch Statement */
                }

                $shipping_address->setCollectShippingRates(true)->collectShippingRates();
                $this->totalsCollector->collectAddressTotals($this->quote->getQuote(), $shipping_address);
                $shipping_address->save();
            }
            $default_country = $this->sc_scopeConfig->getValue('general/country/default', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, null);
            $country = $address->getCountryId() ? $address->getCountryId() : $default_country;
            $address->setCountryId($country)->setCollectShippingRates(true)->collectShippingRates();
            $quote->save();
            $groups = $address->getGroupedAllShippingRates();
            return $groups;
        } catch (LocalizedException $e) {
            /** Catch Statement */
        }
    }

    public function getCarrierName($carrierCode)
    {
        if ($name = $this->sc_scopeConfig->getValue('carriers/' . $carrierCode . '/title', \Magento\Store\Model\ScopeInterface::SCOPE_STORE)) {
            return $name;
        }
        return $carrierCode;
    }

    public function getShippingPrice($address, $price, $flag)
    {
        return $this->priceCurrency->convertAndFormat(
            $this->sc_taxHelper->getShippingPrice($price, $flag, $address),
            true,
            2,
            $address->getQuote()->getStore()
        );
    }

    public function getAddressShippingMethod($address)
    {
        return $address->getShippingMethod();
    }
}
