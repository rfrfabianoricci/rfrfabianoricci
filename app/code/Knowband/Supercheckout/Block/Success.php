<?php

namespace Knowband\Supercheckout\Block;

use Magento\Customer\Model\Context;
use Magento\Sales\Model\Order;

class Success extends \Magento\Framework\View\Element\Template
{

    public $sc_checkoutSession;
    public $sc_orderConfig;
    public $httpContext;

    public function __construct(\Magento\Framework\View\Element\Template\Context $context, \Magento\Checkout\Model\Session $checkoutSession, \Magento\Sales\Model\Order\Config $orderConfig, \Magento\Framework\App\Http\Context $httpContext, array $data = [])
    {
        parent::__construct($context, $data);
        $this->sc_checkoutSession = $checkoutSession;
        $this->sc_orderConfig = $orderConfig;
        $this->_isScopePrivate = true;
        $this->httpContext = $httpContext;
    }

    public function getAdditionalInfoHtml()
    {
        return $this->_layout->renderElement('order.success.additional.info');
    }

    public function _beforeToHtml()
    {
        $this->prepareBlockData();
        return parent::_beforeToHtml();
    }

    public function prepareBlockData()
    {
        $order = $this->sc_checkoutSession->getLastRealOrder();

        $this->addData(
            [
                'is_order_visible' => $this->isVisible($order),
                'view_order_url' => $this->getUrl(
                    'sales/order/view/',
                    ['order_id' => $order->getEntityId()]
                ),
                'print_url' => $this->getUrl(
                    'sales/order/print',
                    ['order_id' => $order->getEntityId()]
                ),
                'can_print_order' => $this->isVisible($order),
                'can_view_order' => $this->canViewOrder($order),
                'order_id' => $order->getIncrementId()
            ]
        );
    }

    public function isVisible(Order $order)
    {
        return !in_array($order->getStatus(), $this->sc_orderConfig->getInvisibleOnFrontStatuses());
    }

    public function canViewOrder(Order $order)
    {
        return $this->httpContext->getValue(Context::CONTEXT_AUTH) && $this->isVisible($order);
    }
}
