<?php
namespace Knowband\Supercheckout\Helper;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class Attributes extends \Magento\Framework\App\Helper\AbstractHelper
{

    public $scopeConfig;
    private $request;
    private $eavAttribute;

    /*
     * Function Modified by RS on 08-Sept-2017 for fixing the issue "whitescreened on visiting Knowband settings via admin" - reported by Magento Team 
     */
    public function __construct(\Magento\Framework\App\Helper\Context $context, \Magento\Eav\Model\Entity\Attribute $eavAttribute)
    {
        $this->scopeConfig = $context->getScopeConfig();
        $this->request = $context->getRequest();
        $this->eavAttribute = $eavAttribute;
        parent::__construct($context);
    }

    /*
     * Function Modified by RS on 08-Sept-2017 for fixing the issue "whitescreened on visiting Knowband settings via admin" - reported by Magento Team 
     */
    public function unrequireAttribute($attribute_code = "lastname")
    {
        $this->eavAttribute->loadByCode('customer_address', $attribute_code);
        $this->eavAttribute->setIsRequired(false);
        $this->eavAttribute->save();
    }
}
