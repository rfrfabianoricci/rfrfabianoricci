<?php

namespace Knowband\Supercheckout\Helper;

use Magento\Framework\Exception\InputException;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    public $store_manager;
    public $scope_config;
    public $request_obj;
    public $state_obj;
    public $inlineTranslation;
    public $transport_builder;
    public $settings_obj;
    public $customer_session;
   
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\State $state,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
    ) {
    
        $this->store_manager                = $storeManager;
        $this->scope_config                 = $context->getScopeConfig();
        $this->request_obj                     = $context->getRequest();
        $this->state_obj                       = $state;
        $this->inlineTranslation            = $inlineTranslation;
        $this->customer_session             = $customerSession;
        $this->transport_builder            = $transportBuilder;
        parent::__construct($context);
    }
    
    public function getSavedSettings($scope = "default", $scope_id = 0)
    {
        if ($this->request_obj->getParam('store')) {
            $scope_id = $this->store_manager->getStore($this->request_obj->getParam('store'))->getId();
            $scope = "stores";
        } elseif ($this->request_obj->getParam('website')) {
            $scope_id = $this->store_manager->getWebsite($this->request_obj->getParam('website'))->getId();
            $scope = "websites";
        } elseif ($this->request_obj->getParam('group')) {
            $scope_id = $this->store_manager->getGroup($this->request_obj->getParam('group'))->getWebsite()->getId();
            $scope = "groups";
        } else {
            $scope = "default";
            $scope_id = 0;
        }
        $settings_json = $this->scope_config->getValue('knowband/supercheckout/settings', $scope, $scope_id);
        $settings_array = json_decode($settings_json, true);
        if ($settings_array === false) {
            return $this->getDefaultSettings();
        } else {
            return $settings_array;
        }
    }

    public function getDefaultSettings()
    {
        return [
            "supercheckout" => $this->generalSettings()
        ];
    }

    public function generalSettings()
    {
        return [
            "enable" => 1
        ];
    }

    public function getSettings()
    {
        $settings_json = $this->scope_config->getValue(
            'knowband/supercheckout/settings',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        $settings_array = json_decode($settings_json, true);
        if ($settings_array === false) {
            return $this->getDefaultSettings();
        } else {
            return $settings_array;
        }
    }

    public function getSortOrder($type = "")
    {
        $this->settings_obj = $this->getSettings();
        if ($type) {
            if (!empty($this->settings_obj)) {
                if ($this->settings_obj["supercheckout"]["layout-type"] == "two") {
                    return [
                        'col' => $this->settings_obj["supercheckout"]["design"]["twocolumn"]["portlet"][$type]["col-2"],
                        'row' => $this->settings_obj["supercheckout"]["design"]["twocolumn"]["portlet"][$type]["row-2"],
                        'inside' => $this->settings_obj["supercheckout"]["design"]["twocolumn"]["portlet"][$type]["inside-2"]
                    ];
                } else {
                    return [
                        'col' => $this->settings_obj["supercheckout"]["design"]["threecolumn"]["portlet"][$type]["column"],
                        'row' => $this->settings_obj["supercheckout"]["design"]["threecolumn"]["portlet"][$type]["row"],
                        'inside' => 0,
                    ];
                }
            } else {
                $this->settings_obj = $this->getSettings();
                $this->getSortOrder();
            }
        } else {
            return [
                'col' => 0,
                'row' => 0,
                'inside' => 0,
            ];
        }
    }

    public function createCustomerAccount($data)
    {
        $email = isset($data["email"])?$data["email"]:"";
        $firstname = isset($data["firstname"])?$data["firstname"]:"";
        $lastname = isset($data["lastname"])?$data["lastname"]:"";
        $password = isset($data["password"])?$data["password"]:"";
        $customer = $this->customerFactory->create();
        $websiteId  = $this->storeManager->getWebsite()->getWebsiteId();
        $customer->setWebsiteId($websiteId);
        $customer->loadByEmail($email);
        if ($customer->getId()) {
            $this->_session->setCustomerAsLoggedIn($customer);
        } else {
            if ($email && $firstname) {
                $customer->setEmail($email);
                $customer->setFirstname($firstname);
                $customer->setLastname($lastname);
                $customer->setPassword(mt_rand());
                $customer->setDob($dob);
                $customer->setGender($gender);
                $customer->save();
                $this->_session->setCustomerAsLoggedIn($customer);
            } else {
                $result->setData(['error' => __("Email is private. Please make it public to enable this feature.")]);
                return $result;
            }
        }
    }

    public function isLoggedIn()
    {
        if ($this->customer_session->isLoggedIn()) {
            return true;
        }
        return false;
    }

    public function sendRegistrationEmail($email, $password)
    {
        if ($email && $password) {
            try {
                $subject = __("Congratulations! You are now successfully registered.");
                $password = $password;
                $this->inlineTranslation->suspend();
                $sentToEmail = $this->scope_config->getValue(
                    'trans_email/ident_support/email',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                );
                $sentToname = $this->scope_config->getValue(
                    'trans_email/ident_support/name',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                );
                $sender = [
                    'name' => $sentToname,
                    'email' => $sentToEmail,
                ];
                $senderToInfo = [
                    'email' => $email,
                ];
                $transport = $this->transport_builder
                        ->setTemplateIdentifier('supercheckout_email_template')
                        ->setTemplateOptions([
                            'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                            'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID,
                        ])
                        ->setTemplateVars(["password" => $password, "subject" => $subject])
                        ->setFrom($sender)
                        ->addTo($senderToInfo)
                        ->getTransport();
                $transport->sendMessage();
                $this->inlineTranslation->resume();
            } catch (\Magento\Framework\Exception\InputException $e) {
                /* Catch Statements */
            }
        }
    }
}
