<?php

/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Knowband\Supercheckout\Model;

class Address extends \Magento\Quote\Model\Quote\Address
{

    private $helper;

    public function getUserType()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->helper = $objectManager->create('Knowband\Supercheckout\Helper\Data');
        if ($this->helper->isLoggedIn()) {
            return "login";
        } else {
            return "guest";
        }
    }

    public function validate()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->helper = $objectManager->create('Knowband\Supercheckout\Helper\Data');
        $supercheckout_settings = $this->helper->getSavedSettings();
        if (isset($supercheckout_settings["supercheckout"]["enable"]) && $supercheckout_settings["supercheckout"]["enable"]) {
            if ($this->getAddressType() == "billing") {
                $address_type = "payment_address";
            } else {
                $address_type = "shipping_address";
            }
            $errors = [];
            if ((isset($supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["prefix"]["display"])
                && $supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["prefix"]["display"])
                && (isset($supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["prefix"]["require"])
                && $supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["prefix"]["require"])) {
                if (!\Zend_Validate::is($this->getPrefix(), 'NotEmpty')) {
                    $errors[] = __('Please select the prefix.');
                }
            }
            if ((isset($supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["firstname"]["display"])
                && $supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["firstname"]["display"])
                && (isset($supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["firstname"]["require"])
                && $supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["firstname"]["require"])) {
                if (!\Zend_Validate::is($this->getFirstname(), 'NotEmpty')) {
                    $errors[] = __('Please enter the first name.');
                }
            }
            if ((isset($supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["middlename"]["display"])
                && $supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["middlename"]["display"])
                && (isset($supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["middlename"]["require"])
                && $supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["middlename"]["require"])) {
                if (!\Zend_Validate::is($this->getMiddlename(), 'NotEmpty')) {
                    $errors[] = __('Please enter the middle name.');
                }
            }
            if ((isset($supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["lastname"]["display"])
                && $supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["lastname"]["display"])
                && (isset($supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["lastname"]["require"])
                && $supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["lastname"]["require"])) {
                if (!\Zend_Validate::is($this->getLastname(), 'NotEmpty')) {
                    $errors[] = __('Please enter the last name.');
                }
            }
            if ((isset($supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["suffix"]["display"])
                && $supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["suffix"]["display"])
                && (isset($supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["suffix"]["require"])
                && $supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["suffix"]["require"])) {
                if (!\Zend_Validate::is($this->getSuffix(), 'NotEmpty')) {
                    $errors[] = __('Please select the suffix.');
                }
            }
            if ((isset($supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["company"]["display"])
                && $supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["company"]["display"])
                && (isset($supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["company"]["require"])
                && $supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["company"]["require"])) {
                if (!\Zend_Validate::is($this->getCompany(), 'NotEmpty')) {
                    $errors[] = __('Please enter the company.');
                }
            }
            if ((isset($supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["street1"]["display"])
                && $supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["street1"]["display"])
                && (isset($supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["street1"]["require"])
                && $supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["street1"]["require"])) {
                if (!\Zend_Validate::is($this->getStreetLine(1), 'NotEmpty')) {
                    $errors[] = __('Please enter the street.');
                }
            }
            if ((isset($supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["street2"]["display"])
                && $supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["street2"]["display"])
                && (isset($supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["street2"]["require"])
                && $supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["street2"]["require"])) {
                if (!\Zend_Validate::is($this->getStreetLine(2), 'NotEmpty')) {
                    $errors[] = __('Please enter the street line 2.');
                }
            }
            if ((isset($supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["city"]["display"])
                && $supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["city"]["display"])
                && (isset($supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["city"]["require"])
                && $supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["city"]["require"])) {
                if (!\Zend_Validate::is($this->getCity(), 'NotEmpty')) {
                    $errors[] = __('Please enter the city.');
                }
            }
            if ((isset($supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["country"]["display"])
                && $supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["country"]["display"])
                && (isset($supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["country"]["require"])
                && $supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["country"]["require"])) {
                if (!\Zend_Validate::is($this->getCountryId(), 'NotEmpty')) {
                    $errors[] = __('Please enter the country.');
                }
            }
            if ((isset($supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["region"]["display"])
                && $supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["region"]["display"])
                && (isset($supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["region"]["require"])
                && $supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["region"]["require"])) {
                if ($this->getCountryModel()->getRegionCollection()->getSize() && !\Zend_Validate::is($this->getRegionId(), 'NotEmpty') && $this->_directoryData->isRegionRequired($this->getCountryId())) {
                    $errors[] = __('Please enter the state/province.');
                }
            }
            if ((isset($supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["postcode"]["display"])
                && $supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["postcode"]["display"])
                && (isset($supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["postcode"]["require"])
                && $supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["postcode"]["require"])) {
                $_havingOptionalZip = $this->_directoryData->getCountriesWithOptionalZip();
                if (!in_array($this->getCountryId(), $_havingOptionalZip) && !\Zend_Validate::is($this->getPostcode(), 'NotEmpty')) {
                    $errors[] = __('Please enter the zip/postal code.');
                }
            }
            if ((isset($supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["telephone"]["display"])
                && $supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["telephone"]["display"])
                && (isset($supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["telephone"]["require"])
                && $supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["telephone"]["require"])) {
                if (!\Zend_Validate::is($this->getTelephone(), 'NotEmpty')) {
                    $errors[] = __('Please enter the phone number.');
                }
            }
            if ((isset($supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["fax"]["display"])
                && $supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["fax"]["display"])
                && (isset($supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["fax"]["require"])
                && $supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["fax"]["require"])) {
                if (!\Zend_Validate::is($this->getFax(), 'NotEmpty')) {
                    $errors[] = __('Please enter the fax.');
                }
            }
            if ($address_type == "payment_address") {
                if ((isset($supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["taxvat"]["display"])
                    && $supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["taxvat"]["display"])
                    && (isset($supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["taxvat"]["require"])
                    && $supercheckout_settings["supercheckout"][$address_type][$this->getUserType()]["taxvat"]["require"])) {
                    if (!\Zend_Validate::is($this->getVatId(), 'NotEmpty')) {
                        $errors[] = __('Please enter the tax/vat number....');
                    }
                }
            }
            if (empty($errors)) {
                return true;
            }
            return $errors;
        } else {
            return parent::validate();
        }
    }
}
