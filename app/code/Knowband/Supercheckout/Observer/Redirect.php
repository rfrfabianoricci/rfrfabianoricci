<?php
/*
 * Class (Observer) File Added by RS on 08-Sept-2017 for fixing the automatic redirect to SuperCheckout issue in front end
 */
namespace Knowband\Supercheckout\Observer;
 
class Redirect implements \Magento\Framework\Event\ObserverInterface
{
    public $request;
    public $sc_helper;
    public $sc_settings;
    public $sc_responseFactory;
    public $sc_url;
    public function __construct(\Magento\Framework\App\Request\Http $request, \Magento\Framework\App\ResponseFactory $responseFactory, \Magento\Framework\UrlInterface $url)
    {
        $this->sc_responseFactory = $responseFactory;
        $this->sc_url = $url;
        $this->request = $request;
    }
 
    /**
     * This is the method that fires when the event runs.
     *
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        unset($observer);
        $request_name = $this->request->getFullActionName();
        if ($request_name == 'checkout_index_index') {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $this->sc_helper = $objectManager->create('Knowband\Supercheckout\Helper\Data');
            $this->sc_settings = $this->sc_helper->getSettings();
            if (isset($this->sc_settings["supercheckout"]["enable"]) && $this->sc_settings["supercheckout"]["enable"]) {
                if (isset($this->sc_settings["supercheckout"]["test_mode"]) && $this->sc_settings["supercheckout"]["test_mode"]) {
                    /* Do Nothing */
                } else {
                    $supercheckout_url = $this->sc_url->getUrl('supercheckout');
                    $this->sc_responseFactory->create()->setRedirect($supercheckout_url)->sendResponse();
                }
            }
        }
    }
}
