define(
    [
        'jquery',
        'mage/cookies'
    ], function ($) {
        'use strict';

        $.widget(
            'mage.privacypolicyLink', {
                _create: function () {
                    $(this.options.privacyLink).attr("href",this.options.dataUrl);
                }
            }
        );

        jQuery(".footer .sparsh_consent_checkbox").insertAfter('#newsletter-validate-detail .newsletter');
        jQuery(".footer .sparsh_consent_checkbox div").removeClass('control');
        jQuery(".footer .sparsh_consent_checkbox label").removeClass('label');
        return $.mage.privacypolicyLink;
    }
);