<?php
/**
 * Class ConfigPlugin
 *
 * PHP version 7
 *
 * @category Sparsh
 * @package  Sparsh_Gdpr
 * @author   Sparsh <magento@sparsh-technologies.com>
 * @license  https://www.sparsh-technologies.com  Open Software License (OSL 3.0)
 * @link     https://www.sparsh-technologies.com
 */
namespace Sparsh\Gdpr\Plugin;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Config\Model\Config;

/**
 * Class ConfigPlugin
 *
 * @category Sparsh
 * @package  Sparsh_Gdpr
 * @author   Sparsh <magento@sparsh-technologies.com>
 * @license  https://www.sparsh-technologies.com  Open Software License (OSL 3.0)
 * @link     https://www.sparsh-technologies.com
 */
class ConfigPlugin
{
    /**
     * Request
     *
     * @var RequestInterface
     */
    protected $request;

    /**
     * ConfigWriter
     *
     * @var WriterInterface
     */
    protected $configWriter;

    /**
     * ConfigPlugin constructor.
     *
     * @param RequestInterface $request      request
     * @param WriterInterface  $configWriter configWriter
     */
    public function __construct(
        RequestInterface $request,
        WriterInterface $configWriter
    ) {
        $this->request = $request;
        $this->configWriter = $configWriter;
    }

    /**
     * Before save of config method
     *
     * @param Config $subject subject
     *
     * @return void
     */
    public function beforeSave(
        Config $subject
    ) {
        $cookieParams = $this->request->getParam('groups');
        if (isset($cookieParams['cookie']['fields']['cookie_restriction']['value'])) {
            $value = $cookieParams['cookie']['fields']['cookie_restriction']['value'];
            $this->configWriter->save('web/cookie/cookie_restriction', $value);
            $this->configWriter->save('gdpr/cookie/enabled', $value);
        }
        if (isset($cookieParams['cookie']['fields']['enabled']['value'])) {
            $value = $cookieParams['cookie']['fields']['enabled']['value'];
            $this->configWriter->save('web/cookie/cookie_restriction', $value);
            $this->configWriter->save('gdpr/cookie/enabled', $value);
        }
    }
}
