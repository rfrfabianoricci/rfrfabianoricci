<?php
/**
 * Class InstallSchema
 *
 * PHP version 7
 *
 * @category Sparsh
 * @package  Sparsh_Gdpr
 * @author   Sparsh <magento@sparsh-technologies.com>
 * @license  https://www.sparsh-technologies.com  Open Software License (OSL 3.0)
 * @link     https://www.sparsh-technologies.com
 */
namespace Sparsh\Gdpr\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

/**
 * Class InstallSchema
 *
 * @category Sparsh
 * @package  Sparsh_Gdpr
 * @author   Sparsh <magento@sparsh-technologies.com>
 * @license  https://www.sparsh-technologies.com  Open Software License (OSL 3.0)
 * @link     https://www.sparsh-technologies.com
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * Installs DB schema for a module
     *
     * @param SchemaSetupInterface   $setup   setup
     * @param ModuleContextInterface $context context
     *
     * @return void
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $installer = $setup;

        $installer->startSetup();

        /**
         * Create table 'sparsh_gdpr_consent_history'
         */
        $consentTable = $installer->getConnection()
            ->newTable($installer->getTable('sparsh_gdpr_consent_history'))
            ->addColumn(
                'consent_id',
                Table::TYPE_SMALLINT,
                null,
                ['identity' => true, 'nullable' => false, 'primary' => true],
                'Consent ID'
            )
            ->addColumn(
                'name',
                Table::TYPE_TEXT,
                255,
                [],
                'Customer Name'
            )
            ->addColumn(
                'email',
                Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Customer Email'
            )
            ->addColumn(
                'action',
                Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Action'
            )
            ->addColumn(
                'ip_address',
                Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Ip Address'
            )
            ->addColumn(
                'creation_time',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                [
                    'nullable' => false,
                    'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT
                ],
                'Log time'
            )->setComment('Consent History');

        $installer->getConnection()->createTable($consentTable);
        $installer->getConnection()->addIndex(
            $installer->getTable('sparsh_gdpr_consent_history'),
            $installer->getIdxName(
                $installer->getTable('sparsh_gdpr_consent_history'),
                ['name', 'email', 'ip_address', 'action'],
                \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
            ),
            ['name', 'email', 'ip_address', 'action'],
            \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
        );

        $deleteAccountTable = $installer->getConnection()
            ->newTable($installer->getTable('sparsh_gdpr_delete_account_history'))
            ->addColumn(
                'account_id',
                Table::TYPE_SMALLINT,
                null,
                ['identity' => true, 'nullable' => false, 'primary' => true],
                'Account ID'
            )
            ->addColumn(
                'customer_id',
                Table::TYPE_SMALLINT,
                null,
                ['nullable' => false],
                'Customer ID'
            )
            ->addColumn(
                'name',
                Table::TYPE_TEXT,
                25,
                [],
                'Customer Name'
            )
            ->addColumn(
                'email',
                Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Customer Email'
            )
            ->addColumn(
                'customer_reason',
                Table::TYPE_TEXT,
                '2M',
                [],
                'Reason'
            )
            ->addColumn(
                'status',
                Table::TYPE_TEXT,
                25,
                [],
                'Status'
            )
            ->addColumn(
                'admin_reason',
                Table::TYPE_TEXT,
                '2M',
                [],
                'Reason'
            )->setComment('Delete Account History');
        $installer->getConnection()->createTable($deleteAccountTable);
        $installer->getConnection()->addIndex(
            $installer->getTable('sparsh_gdpr_delete_account_history'),
            $installer->getIdxName(
                $installer->getTable('sparsh_gdpr_delete_account_history'),
                ['name', 'email'],
                \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
            ),
            ['name', 'email'],
            \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
        );

        $anonymousTable = $installer->getConnection()
            ->newTable($installer->getTable('sparsh_gdpr_anonymous_account_history'))
            ->addColumn(
                'anonymous_account_id',
                Table::TYPE_SMALLINT,
                null,
                ['identity' => true, 'nullable' => false, 'primary' => true],
                'Anonymous Account ID'
            )
            ->addColumn(
                'customer_id',
                Table::TYPE_SMALLINT,
                null,
                ['nullable' => false],
                'Customer ID'
            )
            ->addColumn(
                'email',
                Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Customer Email'
            )
            ->addColumn(
                'is_anonymous',
                Table::TYPE_BOOLEAN,
                null,
                [],
                'Is Anonymous'
            )->setComment('Anonymous Account History');
        $installer->getConnection()->createTable($anonymousTable);
        $installer->getConnection()->addIndex(
            $installer->getTable('sparsh_gdpr_anonymous_account_history'),
            $installer->getIdxName(
                $installer->getTable('sparsh_gdpr_anonymous_account_history'),
                ['email'],
                \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
            ),
            ['email'],
            \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
        );

        $installer->endSetup();
    }
}
