<?php
/**
 * Class InstallData
 *
 * PHP version 7
 *
 * @category Sparsh
 * @package  Sparsh_Gdpr
 * @author   Sparsh <magento@sparsh-technologies.com>
 * @license  https://www.sparsh-technologies.com  Open Software License (OSL 3.0)
 * @link     https://www.sparsh-technologies.com
 */
namespace Sparsh\Gdpr\Setup;

use Magento\Cms\Model\BlockFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * Class InstallData
 *
 * @category Sparsh
 * @package  Sparsh_Gdpr
 * @author   Sparsh <magento@sparsh-technologies.com>
 * @license  https://www.sparsh-technologies.com  Open Software License (OSL 3.0)
 * @link     https://www.sparsh-technologies.com
 */
class InstallData implements InstallDataInterface
{
    /**
     * Block factory
     *
     * @var BlockFactory
     */
    protected $blockFactory;

    /**
     * Init
     *
     * @param BlockFactory $blockFactory BlockFactory
     */
    public function __construct(BlockFactory $blockFactory)
    {
        $this->blockFactory = $blockFactory;
    }

    /**
     * Install data
     *
     * @param ModuleDataSetupInterface $setup   setup
     * @param ModuleContextInterface   $context context
     *
     * @return void
     */
    public function install(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $cmsBlock = $this->blockFactory->create()
            ->load('cookie_notice', 'identifier');
        if (!$cmsBlock->getId()) {

            $cmsBlockData = [
                'title' => 'Cookie Notice',
                'identifier' => 'cookie_notice',
                'content' => "<h3 style='text-align: center;'>Cookie</h3>
                            <p>&nbsp;</p>
                            <p style='text-align: left;'>
                            This website requires cookies to provide all of its features. 
                            For more information on what data is contained in the cookies, 
                            please see our <a href=\"{{store url='privacy-policy-cookie-restriction-mode'}}\">
                            Privacy Policy page</a>. To accept cookies from this site, please click the 
                            Accept button below.</p>
                            <p style='text-align: left;'>&nbsp;</p>",
                'is_active' => 1,
                'stores' => [0],
                'sort_order' => 0
            ];

            $this->blockFactory->create()->setData($cmsBlockData)->save();
        }
    }
}
