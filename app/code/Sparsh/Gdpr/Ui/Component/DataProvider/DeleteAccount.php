<?php
/**
 * Class DeleteAccount
 *
 * PHP version 7
 *
 * @category Sparsh
 * @package  Sparsh_Gdpr
 * @author   Sparsh <magento@sparsh-technologies.com>
 * @license  https://www.sparsh-technologies.com  Open Software License (OSL 3.0)
 * @link     https://www.sparsh-technologies.com
 */
namespace Sparsh\Gdpr\Ui\Component\DataProvider;

use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Ui\DataProvider\AbstractDataProvider;
use \Sparsh\Gdpr\Model\ResourceModel\DeleteAccount\CollectionFactory;

/**
 * Class DeleteAccount
 *
 * @category Sparsh
 * @package  Sparsh_Gdpr
 * @author   Sparsh <magento@sparsh-technologies.com>
 * @license  https://www.sparsh-technologies.com  Open Software License (OSL 3.0)
 * @link     https://www.sparsh-technologies.com
 */
class DeleteAccount extends AbstractDataProvider
{
    /**
     * DeleteAccountFactory
     *
     * @var \Sparsh\Gdpr\Model\ResourceModel\DeleteAccount\CollectionFactory
     */
    protected $collection;

    /**
     * DataPersistor
     *
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * LoadedData
     *
     * @var array
     */
    protected $loadedData;

    /**
     * DeleteAccount Constructor
     *
     * @param string                 $name                  name
     * @param string                 $primaryFieldName      primaryFieldName
     * @param string                 $requestFieldName      requestFieldName
     * @param CollectionFactory      $pageCollectionFactory DeleteAccountFactory
     * @param DataPersistorInterface $dataPersistor         dataPersistor
     * @param array                  $meta                  meta
     * @param array                  $data                  data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $pageCollectionFactory,
        DataPersistorInterface $dataPersistor,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $pageCollectionFactory->create();
        $this->dataPersistor = $dataPersistor;
        $this->meta = $this->prepareMeta($this->meta);
        parent::__construct(
            $name,
            $primaryFieldName,
            $requestFieldName,
            $meta,
            $data
        );
    }

    /**
     * Prepares Meta
     *
     * @param array $meta meta
     *
     * @return array
     */
    public function prepareMeta(array $meta)
    {
        return $meta;
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }

        $items = $this->collection->getItems();
        $this->loadedData = [];

        foreach ($items as $contact) {
            $this->loadedData[$contact->getData('account_id')] = $contact->getData();
        }
        $data = $this->dataPersistor->get('gdpr_data');
        if (!empty($data)) {
            $contact = $this->collection->getNewEmptyItem();
            $contact->setData($data);
            $this->loadedData[$contact->getData('account_id')] = $contact->getData();
            $this->dataPersistor->clear('gdpr_data');
        }
        return $this->loadedData;
    }
}
