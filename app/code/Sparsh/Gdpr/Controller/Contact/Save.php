<?php
/**
 * Class ContactSave
 *
 * PHP version 7
 *
 * @category Sparsh
 * @package  Sparsh_Gdpr
 * @author   Sparsh <magento@sparsh-technologies.com>
 * @license  https://www.sparsh-technologies.com  Open Software License (OSL 3.0)
 * @link     https://www.sparsh-technologies.com
 */
namespace Sparsh\Gdpr\Controller\Contact;

use Magento\Framework\App\Action\Context;
use Magento\Contact\Model\ConfigInterface;
use Magento\Contact\Model\MailInterface;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Controller\Result\Redirect;
use Sparsh\Gdpr\Model\ConsentHistoryFactory;
use Magento\Framework\HTTP\PhpEnvironment\RemoteAddress;
use Psr\Log\LoggerInterface;

/**
 * Class Save
 *
 * @category Sparsh
 * @package  Sparsh_Gdpr
 * @author   Sparsh <magento@sparsh-technologies.com>
 * @license  https://www.sparsh-technologies.com  Open Software License (OSL 3.0)
 * @link     https://www.sparsh-technologies.com
 */
class Save extends \Magento\Contact\Controller\Index\Post
{
    /**
     * @var ConsentHistoryFactory
     */
    protected $postFactory;

    /**
     * @var RemoteAddress
     */
    protected $remoteAddress;

    /**
     * Save constructor.
     * @param Context $context
     * @param ConfigInterface $contactsConfig
     * @param MailInterface $mail
     * @param DataPersistorInterface $dataPersistor
     * @param ConsentHistoryFactory $postFactory
     * @param RemoteAddress $remoteAddress
     * @param LoggerInterface|null $logger
     */
    public function __construct(
        Context $context,
        ConfigInterface $contactsConfig,
        MailInterface $mail,
        DataPersistorInterface $dataPersistor,
        ConsentHistoryFactory $postFactory,
        RemoteAddress $remoteAddress,
        LoggerInterface $logger = null
    ) {
        parent::__construct(
            $context,
            $contactsConfig,
            $mail,
            $dataPersistor,
            $logger
        );
        $this->postFactory = $postFactory;
        $this->remoteAddress = $remoteAddress;
    }

    /**
     * @return Redirect
     * @throws \Exception
     */
    public function execute()
    {
        $ipAddress = $this->remoteAddress->getRemoteAddress();
        if (!$this->getRequest()->isPost()) {
            return $this->resultRedirectFactory->create()->setPath('*/*/');
        } else {
            $input = $this->getRequest()->getPostValue();
            $post = $this->postFactory->create();
            if (isset($input['sparsh_consent_checkbox'])==1) {
                $input["ip_address"] = $ipAddress;
                $post->setData($input)->save();
            }
        }

        return parent::execute();
    }
}
