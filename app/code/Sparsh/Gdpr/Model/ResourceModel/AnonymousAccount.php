<?php
/**
 * Class AnonymousAccount
 *
 * PHP version 7
 *
 * @category Sparsh
 * @package  Sparsh_Gdpr
 * @author   Sparsh <magento@sparsh-technologies.com>
 * @license  https://www.sparsh-technologies.com  Open Software License (OSL 3.0)
 * @link     https://www.sparsh-technologies.com
 */
namespace Sparsh\Gdpr\Model\ResourceModel;

/**
 * Class AnonymousAccount
 *
 * @category Sparsh
 * @package  Sparsh_Gdpr
 * @author   Sparsh <magento@sparsh-technologies.com>
 * @license  https://www.sparsh-technologies.com  Open Software License (OSL 3.0)
 * @link     https://www.sparsh-technologies.com
 */
class AnonymousAccount extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * AnonymousAccount constructor.
     *
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context context
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context
    ) {
        parent::__construct($context);
    }

    /**
     * Initialize ResourceModel
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('sparsh_gdpr_anonymous_account_history', 'anonymous_account_id');
    }
}
