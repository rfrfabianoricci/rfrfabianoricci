<?php
/**
 * Class DeleteAccount
 *
 * PHP version 7
 *
 * @category Sparsh
 * @package  Sparsh_Gdpr
 * @author   Sparsh <magento@sparsh-technologies.com>
 * @license  https://www.sparsh-technologies.com  Open Software License (OSL 3.0)
 * @link     https://www.sparsh-technologies.com
 */
namespace Sparsh\Gdpr\Model\ResourceModel;

/**
 * Class DeleteAccount
 *
 * @category Sparsh
 * @package  Sparsh_Gdpr
 * @author   Sparsh <magento@sparsh-technologies.com>
 * @license  https://www.sparsh-technologies.com  Open Software License (OSL 3.0)
 * @link     https://www.sparsh-technologies.com
 */
class DeleteAccount extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * DeleteAccount constructor.
     *
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context context
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context
    ) {
        parent::__construct($context);
    }

    /**
     * Initialize ResourceModel
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('sparsh_gdpr_delete_account_history', 'account_id');
    }
}
