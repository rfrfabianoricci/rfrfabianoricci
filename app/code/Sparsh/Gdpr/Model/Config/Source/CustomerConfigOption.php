<?php
/**
 * Class CustomerConfigOption
 *
 * PHP version 7
 *
 * @category Sparsh
 * @package  Sparsh_Gdpr
 * @author   Sparsh <magento@sparsh-technologies.com>
 * @license  https://www.sparsh-technologies.com  Open Software License (OSL 3.0)
 * @link     https://www.sparsh-technologies.com
 */
namespace Sparsh\Gdpr\Model\Config\Source;

/**
 * Class CustomerConfigOption
 *
 * PHP version 7
 *
 * @category Sparsh
 * @package  Sparsh_Gdpr
 * @author   Sparsh <magento@sparsh-technologies.com>
 * @license  https://www.sparsh-technologies.com  Open Software License (OSL 3.0)
 * @link     https://www.sparsh-technologies.com
 */
class CustomerConfigOption implements \Magento\Framework\Data\OptionSourceInterface
{
    const UNDEFINED_OPTION_LABEL = '-- Please Select --';
    /**
     * Path to configuration, set multiselect options for customer policy
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => '', 'label' => __('-- Please Select --')],
            ['value' => 'order', 'label' => __('Order')],
            ['value' => 'newsletter', 'label' => __('Newsletter')]
        ];
    }
}
