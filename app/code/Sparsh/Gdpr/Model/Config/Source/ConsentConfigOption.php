<?php
/**
 * Class ConsentConfigOption
 *
 * PHP version 7
 *
 * @category Sparsh
 * @package  Sparsh_Gdpr
 * @author   Sparsh <magento@sparsh-technologies.com>
 * @license  https://www.sparsh-technologies.com  Open Software License (OSL 3.0)
 * @link     https://www.sparsh-technologies.com
 */
namespace Sparsh\Gdpr\Model\Config\Source;

/**
 * Class ConsentConfigOption
 *
 * PHP version 7
 *
 * @category Sparsh
 * @package  Sparsh_Gdpr
 * @author   Sparsh <magento@sparsh-technologies.com>
 * @license  https://www.sparsh-technologies.com  Open Software License (OSL 3.0)
 * @link     https://www.sparsh-technologies.comm
 */
class ConsentConfigOption implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * Path to configuration, set multiselect options for consent checkbox
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'registrationpage', 'label' => __('Registration Page')],
            ['value' => 'contactpage', 'label' => __('Contact Page')],
            ['value' => 'newsletterform', 'label' => __('Newsletter Form')]
        ];
    }
}
