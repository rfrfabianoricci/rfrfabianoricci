<?php
/**
 * Class DeleteAccount
 *
 * PHP version 7
 *
 * @category Sparsh
 * @package  Sparsh_Gdpr
 * @author   Sparsh <magento@sparsh-technologies.com>
 * @license  https://www.sparsh-technologies.com  Open Software License (OSL 3.0)
 * @link     https://www.sparsh-technologies.com
 */
namespace Sparsh\Gdpr\Model;

use Magento\Framework\Model\AbstractModel;

/**
 * Class DeleteAccount
 *
 * @category Sparsh
 * @package  Sparsh_Gdpr
 * @author   Sparsh <magento@sparsh-technologies.com>
 * @license  https://www.sparsh-technologies.com  Open Software License (OSL 3.0)
 * @link     https://www.sparsh-technologies.com
 */
class DeleteAccount extends AbstractModel
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Sparsh\Gdpr\Model\ResourceModel\DeleteAccount::class);
    }
}
