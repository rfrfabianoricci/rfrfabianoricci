<?php
/**
 * Class Consent
 *
 * PHP version 7
 *
 * @category Sparsh
 * @package  Sparsh_Gdpr
 * @author   Sparsh <magento@sparsh-technologies.com>
 * @license  https://www.sparsh-technologies.com  Open Software License (OSL 3.0)
 * @link     https://www.sparsh-technologies.com
 */
namespace Sparsh\Gdpr\Block;

use Sparsh\Gdpr\Helper\Data;
use Magento\Framework\View\Element\Template\Context;

/**
 * Class Consent
 *
 * @category Sparsh
 * @package  Sparsh_Gdpr
 * @author   Sparsh <magento@sparsh-technologies.com>
 * @license  https://www.sparsh-technologies.com  Open Software License (OSL 3.0)
 * @link     https://www.sparsh-technologies.com
 */
class Consent extends \Magento\Framework\View\Element\Template
{
    /**
     * GdprHelper
     *
     * @var Data
     */
    protected $helper;
       
    /**
     * Consent constructor.
     *
     * @param Context $context context
     * @param Data    $helper  helper
     * @param array   $data    data
     */
    public function __construct(
        Context $context,
        Data $helper,
        array $data = []
    ) {
        $this->helper = $helper;
        parent::__construct($context, $data);
    }
    
    /**
     * Get system configuration value of Show consent checkbox
     *
     * @return array
     */
    public function getConsentCheckboxData()
    {
        $consentInfo = $this->helper->getConsentConfig('show_consent_checkbox_in');
        $consentInfo = explode(',', $consentInfo);
        return $consentInfo;
    }

    /**
     * Get system configuration value of checkbox content
     *
     * @return string
     */
    public function getConsentContent()
    {
        $consentContentInfo = $this->helper->getConsentConfig('checkbox_content');
        return $consentContentInfo;
    }

    /**
     * Get system configuration value of Message before the checkbox
     *
     * @return string
     */
    public function getMsgContent()
    {
        $msgInfo = $this->helper->getConsentConfig('msg_before_checkbox');
        return $msgInfo;
    }
}
