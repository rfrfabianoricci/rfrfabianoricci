<?php
/**
 * Class ConsentObserver
 *
 * PHP version 7
 *
 * @category Sparsh
 * @package  Sparsh_Gdpr
 * @author   Sparsh <magento@sparsh-technologies.com>
 * @license  https://www.sparsh-technologies.com  Open Software License (OSL 3.0)
 * @link     https://www.sparsh-technologies.com
 */
namespace Sparsh\Gdpr\Observer;

use Magento\Framework\Event\ObserverInterface;

/**
 * Class ConsentObserver
 *
 * @category Sparsh
 * @package  Sparsh_Gdpr
 * @author   Sparsh <magento@sparsh-technologies.com>
 * @license  https://www.sparsh-technologies.com  Open Software License (OSL 3.0)
 * @link     https://www.sparsh-technologies.com
 */
class ConsentObserver implements ObserverInterface
{
    /**
     * @var \Sparsh\Gdpr\Model\ConsentHistoryFactory
     */
    protected $postFactory;

    /**
     * @var \Magento\Framework\HTTP\PhpEnvironment\RemoteAddress
     */
    protected $remoteAddress;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;

    /**
     * ConsentObserver constructor.
     * @param \Sparsh\Gdpr\Model\ConsentHistoryFactory $postFactory
     * @param \Magento\Framework\HTTP\PhpEnvironment\RemoteAddress $remoteAddress
     * @param \Magento\Framework\App\RequestInterface $request
     */
    public function __construct(
        \Sparsh\Gdpr\Model\ConsentHistoryFactory $postFactory,
        \Magento\Framework\HTTP\PhpEnvironment\RemoteAddress $remoteAddress,
        \Magento\Framework\App\RequestInterface $request
    ) {
        $this->postFactory = $postFactory;
        $this->remoteAddress = $remoteAddress;
        $this->request = $request;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @throws \Exception
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $ipAddress = $this->remoteAddress->getRemoteAddress();
        $input = $this->request->getPostValue();
        $post = $this->postFactory->create();
        if (isset($input['sparsh_consent_checkbox'])==1) {
            if (isset($input['firstname'])) {
                $name = $input['firstname']." ".$input['lastname'];
                $input["name"] = trim($name);
            }
            $input["ip_address"] = $ipAddress;
            $post->setData($input)->save();
        }
    }
}
