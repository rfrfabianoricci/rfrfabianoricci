/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    map: {
        '*': {
            webuiPopover:'Knowband_Supercheckout/js/webui-popover'
        }
    }
};
